-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Pi 24.Apr 2015, 13:37
-- Verzia serveru: 5.5.24-log
-- Verzia PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `bakdb`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `logtestov`
--

CREATE TABLE IF NOT EXISTS `logtestov` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_testu` int(11) NOT NULL,
  `id_skoly` int(11) NOT NULL,
  `id_skupiny` int(11) NOT NULL,
  `meno_studenta` varchar(40) NOT NULL,
  `priezvisko_studenta` varchar(40) NOT NULL,
  `ip_studenta` varchar(20) NOT NULL,
  `log_testu` text NOT NULL,
  `datum` datetime NOT NULL,
  `body` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Sťahujem dáta pre tabuľku `logtestov`
--

INSERT INTO `logtestov` (`id`, `id_testu`, `id_skoly`, `id_skupiny`, `meno_studenta`, `priezvisko_studenta`, `ip_studenta`, `log_testu`, `datum`, `body`) VALUES
(2, 3, 1, 1, 'Michal', 'Se', '127.0.0.1', 'a:2:{s:2:"m1";s:3:"100";s:2:"m2";s:10:"Striedavý";}', '2015-03-06 12:31:00', 2),
(3, 4, 1, 1, 'Lukas', 'Mrkvicka', '127.0.0.1', 'a:2:{s:2:"m1";s:9:"Zjedol sa";s:2:"m2";s:2:"aa";}', '2015-03-07 17:57:04', 5),
(6, 3, 1, 1, 'Patrik', 'Vrbovsky', '127.0.0.1', 'a:2:{s:2:"m1";s:4:"1000";s:2:"m2";s:10:"Striedavý";}', '2015-03-07 18:39:09', 2),
(13, 3, 1, 1, 'Juraj', 'Kerekes', '127.0.0.1', 'a:2:{s:2:"m1";s:3:"500";s:2:"m2";s:10:"Striedavý";}', '2015-03-07 18:44:37', 2),
(20, 3, 1, 1, 'Jakub', 'Hr', '127.0.0.1', 'a:2:{s:2:"m1";s:3:"250";s:2:"m2";s:10:"Striedavý";}', '2015-03-07 18:53:49', 12),
(21, 4, 1, 1, 'www', 'Se', '127.0.0.1', 'a:2:{s:2:"m1";s:9:"Zjedol sa";s:2:"m2";s:2:"aa";}', '2015-03-10 09:18:32', 5),
(22, 3, 1, 1, 'hlh', 'gg', '127.0.0.1', 'a:0:{}', '2015-04-05 14:51:19', 0),
(23, 3, 1, 1, 'Michal', 'Ssss', '127.0.0.1', 'a:2:{s:2:"m1";s:3:"500";s:2:"m2";s:10:"Striedavý";}', '2015-04-05 14:55:21', 2),
(24, 4, 1, 1, 'Michal', 'Ssss', '127.0.0.1', 'a:1:{s:2:"m1";s:4:"Hore";}', '2015-04-05 15:01:56', 0),
(25, 6, 1, 1, 'lolec', 'smrdi', '127.0.0.1', 'a:1:{s:2:"m1";s:11:"F-22 Raptor";}', '2015-04-05 15:06:56', 0),
(26, 5, 1, 1, 'frano', 'kral', '127.0.0.1', 'a:0:{}', '2015-04-05 15:09:47', 0),
(27, 3, 2, 4, 'Michal', 'Sejco', '127.0.0.1', 'a:2:{s:2:"m1";s:4:"1000";s:2:"m2";s:10:"Striedavý";}', '2015-04-05 15:13:12', 12);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `skola`
--

CREATE TABLE IF NOT EXISTS `skola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazov` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Sťahujem dáta pre tabuľku `skola`
--

INSERT INTO `skola` (`id`, `nazov`) VALUES
(1, 'SPŠE Zochova 9, BA'),
(2, 'SPŠE Hálová 16, BA'),
(3, 'SPŠE Karola Adlera 5, BA');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `skupiny`
--

CREATE TABLE IF NOT EXISTS `skupiny` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazov` varchar(50) NOT NULL,
  `id_skoly` int(11) NOT NULL,
  `id_ucitela` int(11) NOT NULL,
  `heslo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Sťahujem dáta pre tabuľku `skupiny`
--

INSERT INTO `skupiny` (`id`, `nazov`, `id_skoly`, `id_ucitela`, `heslo`) VALUES
(1, '1.A', 1, 1, 'b59c67bf196a4758191e42f76670ceba'),
(2, '1.B', 1, 1, 'b59c67bf196a4758191e42f76670ceba'),
(3, '1.C', 1, 1, 'b59c67bf196a4758191e42f76670ceba'),
(4, 'Prvaci', 2, 2, 'b59c67bf196a4758191e42f76670ceba');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `testy`
--

CREATE TABLE IF NOT EXISTS `testy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazov` varchar(100) CHARACTER SET utf8 NOT NULL,
  `dataTestu` text CHARACTER SET utf8 NOT NULL,
  `id_skoly` int(11) NOT NULL,
  `exist` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Sťahujem dáta pre tabuľku `testy`
--

INSERT INTO `testy` (`id`, `nazov`, `dataTestu`, `id_skoly`, `exist`) VALUES
(3, 'Transformátorový jav', 'a:6:{s:2:"q1";s:170:"Primárna cievka má 500 závitov. Vstupné napätie je 200V. Zistite počet závitov sekundárnej cievky, tak aby platilo, že výstupné napätie bude mať hodnotu 400V";s:2:"m1";a:5:{i:0;s:4:"1000";i:1;s:3:"250";i:2;s:3:"500";i:3;s:3:"750";i:4;s:3:"100";}s:6:"body_1";s:2:"10";s:2:"q2";s:62:"Aký prúd spôsobuje indukciu napätia v sekundárnej cievke?";s:2:"m2";a:2:{i:0;s:10:"Striedavý";i:1;s:12:"Jednosmerný";}s:6:"body_2";s:1:"2";}', 0, 1),
(4, 'Test testov', 'a:6:{s:2:"q1";s:15:"Kde je pomaranc";s:2:"m1";a:5:{i:0;s:4:"Dole";i:1;s:4:"Hore";i:2;s:7:"Kolotoc";i:3;s:5:"Banan";i:4;s:9:"Zjedol sa";}s:6:"body_1";s:1:"5";s:2:"q2";s:3:"555";s:2:"m2";a:3:{i:0;s:2:"aa";i:1;s:2:"aa";i:2;s:2:"aa";}s:6:"body_2";s:1:"5";}', 0, 1),
(5, 'Teoria strun', 'a:3:{s:2:"q1";s:28:"Kolko D quarkov ma elektron.";s:2:"m1";a:4:{i:0;s:1:"2";i:1;s:2:"11";i:2;s:1:"1";i:3;s:1:"3";}s:6:"body_1";s:3:"500";}', 1, 1),
(6, 'Letectvo USAF', 'a:3:{s:2:"q1";s:69:"Ako sa vola lietadlo s kolmym startom od spolocnosti Lockheed Martin?";s:2:"m1";a:5:{i:0;s:14:"F-35 lightning";i:1;s:11:"F-22 Raptor";i:2;s:15:"F-117 Nighthawk";i:3;s:16:"A-10 Thunderbolt";i:4;s:14:"AC-130 Spectre";}s:6:"body_1";s:1:"8";}', 1, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `testyskupin`
--

CREATE TABLE IF NOT EXISTS `testyskupin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_testu` int(11) NOT NULL,
  `id_skupiny` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `id_skoly` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Sťahujem dáta pre tabuľku `testyskupin`
--

INSERT INTO `testyskupin` (`id`, `id_testu`, `id_skupiny`, `status`, `id_skoly`) VALUES
(26, 3, 1, 2, 1),
(27, 4, 2, 3, 1),
(28, 3, 3, 2, 1),
(29, 4, 1, 2, 1),
(30, 6, 1, 2, 1),
(31, 5, 1, 2, 1),
(32, 3, 4, 2, 2),
(33, 4, 4, 2, 2);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meno` varchar(30) CHARACTER SET utf16 NOT NULL,
  `priezvisko` varchar(50) CHARACTER SET utf16 NOT NULL,
  `heslo` varchar(100) CHARACTER SET utf16 NOT NULL,
  `id_skoly` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`id`, `meno`, `priezvisko`, `heslo`, `id_skoly`) VALUES
(1, 'Michal', 'Sejc', 'b59c67bf196a4758191e42f76670ceba', 1),
(2, 'Fredy', 'Fekete', 'b59c67bf196a4758191e42f76670ceba', 2),
(3, 'Jozef', 'Johanson', 'b59c67bf196a4758191e42f76670ceba', 3),
(4, 'Adam', 'Zeleny', 'b59c67bf196a4758191e42f76670ceba', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
