<?php
	session_start();
	
	include('php/db/databaza.php');
	include('php/db/main.php');
	
	$main = new Main();
	
	//zapnutie tutorialu
	if (!isset($_SESSION['tutorial'])){
		$_SESSION['tutorial'] = 1;
	}else{
		if (isset($_POST['tutorial_off'])){
			$_SESSION['tutorial'] = 0;
		}
		if (isset($_POST['tutorial_on'])){
			$_SESSION['tutorial'] = 1;
		}
	}
	
	//login ucitela
	if (isset($_POST['loginFormSubmitUcitel'])){
		$_SESSION['user'] = $main->overUzivatelaUcitel($_POST['loginFormMeno'],$_POST['loginFormHeslo'],$_POST['id_skoly']);
		if (isset($_SESSION['user'])){
			$_SESSION['user']['ucitel'] = 1;
		}
	}
	
	//login studenta
	if (isset($_POST['loginFormSubmitStudent'])){
		$_SESSION['user'] = $main->overUzivatelaSkupiny($_POST['id_skoly'],$_POST['id_skupiny'],$_POST['loginFormHeslo']);
		if (isset($_SESSION['user'])){
			$_SESSION['user']['ucitel'] 	= 0;
			if (($_POST['loginFormMeno'] == '') && ($_POST['loginFormPriezvisko'] == '')){
				$_SESSION['user']['meno'] 		= 'Žiak';
				$_SESSION['user']['priezvisko'] = $_SERVER['REMOTE_ADDR'];
			}else{
				$_SESSION['user']['priezvisko'] = $_POST['loginFormPriezvisko'];
				$_SESSION['user']['meno'] 		= $_POST['loginFormMeno'];
			}
		}
	}
	
	//logout
	if (isset($_POST['logoutFormSubmit'])){
		session_destroy();
		echo "<script>location.href = 'index.php?experiment=tranformatorovy_jav';</script>";
	}
	
	// echo "<pre>";
	// print_r($_SESSION);
	// echo "</pre>";
	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";

	if (isset($_GET['experiment'])){
		$experiment = $_GET['experiment'];
	}else{
		$experiment = 'tranformatorovy_jav';
	}
	
	$display_op = ['none','none','none','none','none'];
	
	$menu_style = "style='color:#fff; background:#AAAAAA;'";
	$menu_style_uvod = "style='color:#fff; background:#AAAAAA; -webkit-border-radius: 15px 0px 0px 15px; -moz-border-radius: 15px 0px 0px 15px; border-radius: 15px 0px 0px 15px;'";
	$menu_style_zaver = "style='color:#fff; background:#AAAAAA; -webkit-border-radius: 0px 15px 15px 0px; -moz-border-radius: 0px 15px 15px 0px; border-radius: 0px 15px 15px 0px;'";
?>
<html>
    <head>
	    <meta http-equiv="content-type" content="text/html;">
		<meta charset="utf-8">
	    <meta name="author" content="Michal Sejč">
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/menu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="jquery/jq.css">
		
	    <title>Bakalarka</title>
		
		
		<script src="jquery/jquery-1.10.2.js"></script>
		<script src="jquery/jquery-ui.js"></script>
		
		<script src="js/main.js"></script>
		<script src="js/suciastky.js"></script>
		<script src="js/atomy.js"></script>
		<script src="js/transformator.js"></script>
		<script src="js/polovodice.js"></script>
		<script src="js/elclanok.js"></script>
		<script src="js/form.js"></script>
		<script src="js/kirchoff.js"></script>
		<script src="js/vodice.js"></script>
    </head>
    <body>			
		<?php
			if (!isset($_GET['idTestu'])){
				?>
					<div id="main">
						<div id="menu">
							<div id='cssmenu' style='width: 980px;'>
								<ul>
									<li style='width: 25%;'><a href="index.php?experiment=kz" <?=($experiment == 'kz' ? $menu_style_uvod : '')?>>Kirchoffove zákony</a></li>
									<li style='width: 25%;'><a href="index.php?experiment=tranformatorovy_jav" <?=($experiment == 'tranformatorovy_jav' ? $menu_style : '')?>>Transformátorový jav</a></li>
									<li style='width: 25%;'><a href="index.php?experiment=vodice" <?=($experiment == 'vodice' ? $menu_style : '')?>>Vodiče a izolanty</a></li>
									<li style='width: 25%;'><a href="index.php?experiment=elclanok" <?=($experiment == 'elclanok' ? $menu_style_zaver : '')?>>Ekektrický článok</a></li>				
								</ul>
							</div>	
						</div>	
						<div id="content">
							<?php
								switch($experiment){
									case 'kz':
										$includnuty_subor = 'php/experimenty/kz.php';
										if (isset($_GET['op'])){
											$display_op[0] = 'block';
										}
									break;
									case 'tranformatorovy_jav':
										$includnuty_subor = 'php/experimenty/transformatorovy_jav.php';
										if (isset($_GET['op'])){
											$display_op[1] = 'block';
										}
									break;
									case 'pn_prechod':
										$includnuty_subor = 'php/experimenty/polovodice.php';
										if (isset($_GET['op'])){
											$display_op[2] = 'block';
										}
									break;
									case 'elclanok':
										$includnuty_subor = 'php/experimenty/elclanok.php';
										if (isset($_GET['op'])){
											$display_op[3] = 'block';
										}
									break;
									case 'vodice':
										$includnuty_subor = 'php/experimenty/vodice_a_izolanty.php';
										if (isset($_GET['op'])){
											$display_op[4] = 'block';
										}
									break;
									case 'login':
										$includnuty_subor = 'php/ui/login.php';
									break;
									case 'administracia':
										$includnuty_subor = 'php/ui/administracia.php';
									break;
									case 'prehladSkupin':
										$includnuty_subor = 'php/ui/prehladSkupin.php';
									break;
									case 'editorTestov':
										$includnuty_subor = 'php/ui/editorTestov.php';
									break;
									case 'testPreStudenta':
										$includnuty_subor = 'php/ui/testPreStudenta.php';
									break;
									default:
										$includnuty_subor = 'php/experimenty/polovodice.php';	
								};
								include($includnuty_subor);	
							?>	
						</div>	
					</div>	
					<footer>
						<endora>
					</footer>
				<?php
			}else{
				include('php/ui/testPreStudenta.php');			
			}
		?>				
	</body>
</html>	
     
     
