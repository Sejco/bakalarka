function kresli_jadro(x,y,pocet_protonov){
	if (pocet_protonov == 4){ 
		proton(x     , y - 16);
		proton(x - 16, y     );
		proton(x + 16, y     );
		proton(x     , y + 16);
	}else if(pocet_protonov == 5){
		proton(x     , y - 17);
		proton(x - 16, y - 5 );
		proton(x + 16, y - 5 );
		proton(x - 10 , y + 14);
		proton(x + 10 , y + 14);
	}else if(pocet_protonov == 3){
		proton(x     , y - 14);
		proton(x - 12, y + 7 );
		proton(x + 12, y + 7 );
	}
	ctx.beginPath();
	ctx.arc(x,y,30,0,2*Math.PI);
	ctx.stroke();
}

function kresli_elektrony(x,y,atom){	//pn - prechod
	if(atom.hore == 1)	{elektron(x, y - 43);}else{diera(x, y - 43);}
	if(atom.pravy == 1)	{elektron(x + 43, y);}else{diera(x + 43, y);}	
	if(atom.lavy == 1)	{elektron(x - 43, y);}else{diera(x - 43, y);}	
	if(atom.dole == 1)	{elektron(x, y + 43);}else{diera(x, y + 43);}	
}

function atom_vodice(x,y,farba,polomer){
	ctx.beginPath();
	ctx.arc(x,y,polomer,0,2*Math.PI);
	ctx.fillStyle = farba;
    ctx.fill();
	
	ctx.drawImage(svetlo,x - polomer,y - polomer,polomer * 2,polomer * 2);
}

function ciara(x,y,xx,yy){
	ctx.lineWidth = 2;
	ctx.beginPath();
	ctx.moveTo(x,y);
	ctx.lineTo(x + xx,y + yy);
	ctx.stroke();
}

function elektron(x,y, a){
	ctx.lineWidth = 1;
	ctx.strokeStyle = '#000000';
	ctx.beginPath();
	ctx.arc(x,y,10,0,2*Math.PI);
	if (typeof(a)==='undefined'){
		ctx.fillStyle = '#3377DD';
	}else{
		ctx.fillStyle = '#3333DD';
	}	
    ctx.fill();
	ctx.fillStyle = '#FFFFFF';
	ctx.font="30px Georgia";
	ctx.fillText("-",x - 6,y + 8);
	ctx.stroke();
}

function proton(x,y){
	ctx.lineWidth = 1;
	ctx.beginPath();
	ctx.arc(x,y,10,0,2*Math.PI);
	ctx.fillStyle = '#FF2211';
    ctx.fill();
	ctx.fillStyle = '#FFFFFF';
	ctx.font="25px Georgia";
	ctx.fillText("+",x - 8,y + 8);
	ctx.stroke();
}

function diera(x,y){
	ctx.lineWidth = 1;
	ctx.beginPath();
	ctx.arc(x,y,10,0,2*Math.PI);
	ctx.fillStyle = '#FFFFFF';
    ctx.fill();
	ctx.fillStyle = '#000000';
	ctx.font="25px Georgia";
	ctx.fillText("+",x - 8,y + 8);
	ctx.stroke();
}

function sipka(x,y,smer,farba,hrubkaCiary){
	if (hrubkaCiary === undefined) {
        hrubkaCiary = 1;
    } 
	ctx.strokeStyle = farba;	
	ctx.setLineDash([0]);
	ctx.lineWidth = hrubkaCiary;
	ctx.beginPath();
	ctx.moveTo(x,y);
	if (smer == 1){			// hore
		ctx.lineTo(x + 3,y + 10);
		ctx.moveTo(x,y);
		ctx.lineTo(x - 3,y + 10);
	}else if (smer == 2){	// do prava
		ctx.lineTo(x - 10,y + 3);
		ctx.moveTo(x,y);
		ctx.lineTo(x - 10,y - 3);
	}else if (smer == 3){	// dole
		ctx.lineTo(x + 3,y - 10);
		ctx.moveTo(x,y);
		ctx.lineTo(x - 3,y - 10);
	}else{					// do lava
		ctx.lineTo(x + 10,y + 3);
		ctx.moveTo(x,y);
		ctx.lineTo(x + 10,y - 3);
	}	
    ctx.stroke();
}

function no3minus(x,y){
	dusik(x + 4, y - 3);
	kyslik(x,y);
	dusik(x - 7, y - 3);
	dusik(x + 2, y + 6);
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="12px Arial, Helvetica, sans-serif";
	ctx.fillText('NO',x - 12,y - 10);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="8px Arial, Helvetica, sans-serif";
	ctx.fillText('3',x + 6,y - 8);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('-',x + 7,y - 17);
	ctx.fill();
}

function cu2plus(x,y){
	med(x,y);
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="12px Arial, Helvetica, sans-serif";
	ctx.fillText('Cu',x - 8,y - 10);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('2+',x + 8,y - 14);
	ctx.fill();
}

function zn2plus(x,y){
	zinok(x,y);
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="12px Arial, Helvetica, sans-serif";
	ctx.fillText('Zn',x - 7,y - 10);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('2+',x + 7,y - 14);
	ctx.fill();
}

function so42minus(x,y){
	kyslik(x - 8,y + 8);
	kyslik(x + 8,y - 8);
	sira(x,y);	
	kyslik(x + 8,y + 8);
	kyslik(x - 8,y - 8);
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="12px Arial, Helvetica, sans-serif";
	ctx.fillText('SO',x - 10,y - 16);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('4',x + 7,y - 14);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('2-',x + 8,y - 22);
	ctx.fill();
}

function kplus(x,y){
	draslik(x,y);
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="12px Arial, Helvetica, sans-serif";
	ctx.fillText('K',x - 4,y - 8);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText('+',x + 4,y - 14);
	ctx.fill();
}

function sira(x,y){
	ctx.beginPath();
	ctx.arc(x,y,12,0,2*Math.PI);
	ctx.fillStyle = 'rgba(204,204,17,0.9)';
	ctx.fill();	
}

function kyslik(x,y){
	ctx.beginPath();
	ctx.arc(x,y,6,0,2*Math.PI);
	ctx.fillStyle = 'rgba(255,85,34,0.9)';
	ctx.fill();	
}

function med(x,y){
	ctx.beginPath();
	ctx.arc(x,y,9,0,2*Math.PI);
	ctx.fillStyle = 'rgba(204,102,0,0.9)';
	ctx.fill();	
}

function zinok(x,y){
	ctx.beginPath();
	ctx.arc(x,y,9,0,2*Math.PI);
	ctx.fillStyle = 'rgba(153,153,153,0.9)';
	ctx.fill();	
}

function draslik(x,y){
	ctx.beginPath();
	ctx.arc(x,y,6,0,2*Math.PI);
	ctx.fillStyle = 'rgba(204,17,221,0.9)';
	ctx.fill();	
}

function dusik(x,y){
	ctx.beginPath();
	ctx.arc(x,y,5,0,2*Math.PI);
	ctx.fillStyle = 'rgba(0,85,238,0.9)';
	ctx.fill();	
}

function atom_kovu(x,y,znacka,farba,naboj){
	ctx.beginPath();
	ctx.arc(x,y,9,0,2*Math.PI);
	ctx.fillStyle = farba;
	ctx.fill();	
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="16px Arial, Helvetica, sans-serif";
	ctx.fillText(znacka,x - 10,y - 12);
	ctx.fill();
	
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.font="10px Arial, Helvetica, sans-serif";
	ctx.fillText(naboj,x + 10,y - 20);
	ctx.fill();
}

function sinus(x,y,vyska,farba,pole_surr,posun){
	var ii = posun;
	ctx.lineWidth = 2;
	ctx.strokeStyle = farba;
	ctx.beginPath();	
	if(typeof(pole_surr)==='undefined'){
		ctx.moveTo(x,y);
		for(i = -Math.PI;i <= Math.PI;i += 0.05){
			x += 1;
			yy = y + Math.sin(i) * vyska;
			ctx.lineTo(x,yy);
		}	
	}else{
		ctx.moveTo(x,pole_surr[vyska][posun]);
		for(i = 1;i <= 125; i++){
			if (ii > 125){
				ii = 0;
			}
			x += 1;
			yy = pole_surr[vyska][ii];
			ii++;
			ctx.lineTo(x,yy);
		}		
	} 
	ctx.stroke();
}

