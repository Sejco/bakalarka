﻿function Elektricky_clanok() {
	var KONST_R 		= 8.3144621; 				// univerzalana konstanta plynov
	var KONST_log 		= 2.303;	 				// prevod z ln na log10
	var KONST_F			= 9.684 * Math.pow(10,4);	// faradayova konstanta
	var T				= 298;						// teplota
	var z				= 1;						// pocet valencnych elektronov nosica
	var n				= 2;						// pocet prenesenych elektronov sucastne
	var roztok_1		= 1;						// koncentracia rozpostnych latok v roztoku
	var roztok_2		= 1;						// koncentracia rozpostnych latok v roztoku
	var sirky_elektrod 	= [30,30];
	var intenzita_farby = [0.2,0.7];
	var farba_vody		= 'rgba(51, 136, 197, 0.5)';
	var spustene		= false;
	var	text_nadoby_1	= 'Roztok ZnSO4';
	var	text_nadoby_2	= 'Roztok CuSO4';
	var naboj_nadoby_1	= 0;
	var naboj_nadoby_2	= 0;
	var glob_cas		= 0;
	var krok_reakcie	= 0;
	var elektrony		= [false,[0,0],[0,0],false];	// pohyb , x, y, zobrazenie
	var rychlost_pohybu	= 2;
	var atom_v_pohybe	= [[false,'prvok',0,0,0,0,0],[false,'prvok',0,0,0,0,0],[false,'prvok',0,0,0,0,0],[false,'prvok',0,0,0,0,0]]; // x1,y1,krok_po_x,krok_po_y,pocet_krokov
	var casovacudalosti	= 200;
	var tutorial_cast	= 1;
	var mat				= {			//material
		litium 	:  {znacka : 'Li', farba : 'rgba(187,187,187,0.9)'	, E : -3.01, img : 'li.jpg'},			
		zinok	:  {znacka : 'Zn', farba : 'rgba(153,153,153,0.9)'	, E : -0.71, img : 'zn.jpg'},	
		med 	:  {znacka : 'Cu', farba : 'rgba(204,102,0,0.9)'	, E : 0.34 , img : 'cu.jpg'}	
	}
	var nadoba_1		= {
		so42minus	:	[[180,370,0,0,0,0,0],[300,323,0,0,0,0,0],[311,433,0,0,0,0,0],[184,284,0,0,0,0,0]],	//x, y, cielove x, cielove y, krok x, krok y, pocet krokov
		zn2plus		:	[[225,306,0,0,0,0,0],[191,418,0,0,0,0,0],[250,370,0,0,0,0,0],[270,428,0,0,0,0,0]],
		no3minus	:	[]
	}					
	var nadoba_2		= {
		so42minus	:	[[480,370,0,0,0,0,0],[590,323,0,0,0,0,0],[585,403,0,0,0,0,0],[536,402,0,0,0,0,0]],
		cu2plus		:	[[539,356,0,0,0,0,0],[506,315,0,0,0,0,0],[500,437,0,0,0,0,0],[615,360,0,0,0,0,0]],
		kplus		:	[]
	}	
	var most			= {
		kplus		:	[[320,170,0,0,0,0,0],[261,250,0,0,0,0,0],[533,182,0,0,0,0,0],[469,172,0,0,0,0,0],
						 [288,205,0,0,0,0,0],[514,239,0,0,0,0,0],[393,172,0,0,0,0,0],[342,174,0,0,0,0,0]],
		no3minus	:	[[300,170,0,0,0,0,0],[276,234,0,0,0,0,0],[515,173,0,0,0,0,0],[449,173,0,0,0,0,0],
						 [266,194,0,0,0,0,0],[524,217,0,0,0,0,0],[416,169,0,0,0,0,0],[370,168,0,0,0,0,0]]
	}
	var interval_animacie;
	var el_na_elektode;
	
	
	
	
	this.Create = function(){
		this.Kresli();	
	}
	
	this.Run = function(){
		spustene = !spustene;
		this.tutorial();
		this.zmen_cudlik_on_off(spustene);
		vypinac(220,80,spustene);
		if (spustene){
			this.animacka_main();
			this.zmen_hodnotu_voltmetra(this.vypocitaj_napatie());			
		}else{
			this.kresli_elektrony();
			this.zmen_hodnotu_voltmetra(0);
			clearInterval(interval_animacie);
		}
	}

	this.Kresli= function(){
		cista_plocha();
		this.kresli_nadobu(100,200,'rgba(170, 170, 170, '+intenzita_farby[0]+')',text_nadoby_1,naboj_nadoby_1);
		this.kresli_nadobu(450,200,'rgba(51, 136, 197, '+intenzita_farby[1]+')',text_nadoby_2,naboj_nadoby_2);
		this.kresli_elektrodu(120,150,mat.zinok.farba,sirky_elektrod[0],mat.zinok.znacka);
		this.kresli_elektrodu(650,150,mat.med.farba,sirky_elektrod[1],mat.med.znacka);
		this.kresli_obvod();
		this.kresli_most(250,200,farba_vody);
		this.kresli_molekuly();
	}
	
	this.vypocitaj_el_potencial_mostu = function(T,z){
		return KONST_log * ((KONST_R * T) / (z * KONST_F));
	}
	
	this.vypocitaj_el_potencial_elektrod = function(EL1,EL2){
		return Math.abs(EL1 - EL2);
	}
	
	this.vypocitaj_pomer_roztokov = function(R1,R2){
		return (R1 / R2);
	}
	
	this.zmen_hodnotu_voltmetra = function(u){
		pom = u.toFixed(4);
		if (spustene && pom < 2 && pom > 0){ 
			document.getElementById("voltmeter_e_cell").value = pom;	
		}else{
			document.getElementById("voltmeter_e_cell").value = 0;
		}
	}
	
	this.zmen_teplotu = function(teplota){
		T = teplota;
	}
	
	this.prepocitaj_napatie_pri_zmene = function(){
		this.zmen_hodnotu_voltmetra(this.vypocitaj_napatie());
	}
	
	this.zmen_zobrazovanu_teplotu = function(teplota){
		document.getElementById("aktualna_teplota").innerHTML = teplota+" K / "+kelvin_to_celsius(teplota)+"°C";	
	}
	
	this.vypocitaj_napatie = function(){
		var E0 		= this.vypocitaj_el_potencial_elektrod(mat.med.E,mat.zinok.E);
		var pomer	= this.vypocitaj_pomer_roztokov(roztok_1,roztok_2);
		var Em		= this.vypocitaj_el_potencial_mostu(T,z);
		
		return E0 - (Em * Math.log(pomer));
	}
	
	this.vypocitaj_vydialenost = function(x1,y1,x2,y2){
		dx = x2 - x1;
		dy = y2 - y1;
		return Math.sqrt(dx*dx + dy*dy);
	}
	
	this.vypocitaj_pocet_krokov = function(d){
		return (d / rychlost_pohybu);
	}
	
	this.vypocitaj_velkost_kroku = function(x1,x2,pocet_krokov){
		return ((x2 - x1) / pocet_krokov);
	}

	this.kresli_nadobu = function(x,y,farba,text,naboj){
		var farba_textu	= this.zisti_farbu_naboja(naboj);
		
		this.vykresli_tvar_nadoby(x,y,farba);
		this.ukaz_text_nadoby(x,y,text,farba_textu);		
		this.ukaz_naboj_nadoby(x,y,naboj,farba_textu);
	}
	
	this.vykresli_tvar_nadoby = function(x,y,farba){
		ctx.fillStyle 	= farba;
		ctx.strokeStyle = '#000000';
		ctx.lineWidth 	= 4;
		ctx.setLineDash([0]);
		ctx.beginPath();	
		ctx.moveTo(x,y);
		ctx.lineTo(x,y + 250);
		ctx.arc(x + 20,y + 250,20,1*Math.PI,0.5*Math.PI,true);
		ctx.lineTo(x + 230, y + 270);
		ctx.arc(x + 230,y + 250,20,0.5*Math.PI,0*Math.PI,true);
		ctx.lineTo(x + 250, y);
		ctx.stroke();
		ctx.fill();		
		ctx.clearRect(x + 2,y,246,50);
	}
	
	this.ukaz_text_nadoby = function(x,y,text,farba){
		ctx.fillStyle = farba;
		ctx.font="18px Tahoma, Geneva, sans-serif";
		ctx.fillText(text,x + 50,y + 290);
	}
	
	this.zisti_text_naboja = function(naboj){
		var naboje = ['--','-','','+','++'];
		return naboje[naboj + 2];
	}
	
	this.ukaz_naboj_nadoby = function(x,y,naboj,farba){	
		var text_naboja = this.zisti_text_naboja(naboj);
		ctx.fillStyle = farba;
		ctx.font="22px Tahoma, Geneva, sans-serif";
		ctx.fillText(text_naboja,x + 170,y + 290);	
	}
	
	this.zisti_farbu_naboja = function(naboj){
		c_kladny	= '#FF2211';
		c_zaporny	= '#3377DD';
		c_neutral	= '#000000';
		if (naboj > 0){
			return c_kladny;
		}
		if (naboj < 0){
			return c_zaporny;
		}
		return c_neutral;
	}
	
	this.kresli_elektrodu = function(x,y,farba,sirka,znacka){
		ctx.fillStyle 	= farba;
		ctx.beginPath();
		ctx.rect(x,y,30,100);
		ctx.fill();
		
		if (sirka >= 1){
			ctx.beginPath();
			ctx.rect(x + ((30 - sirka) / 2),y + 100,sirka,200);
			ctx.fill();
		}
		
		ctx.beginPath();
		ctx.fillStyle = '#000000';
		ctx.font="14px Arial, Helvetica, sans-serif";
		ctx.fillText(znacka,x + 6,y + 20);
		ctx.fill();
	}
	
	this.kresli_obvod = function(){	
		ctx.lineWidth 	= 2;	
		ctx.beginPath();
		ctx.moveTo(135,150);
		ctx.lineTo(135,80);
		ctx.lineTo(360,80);
		ctx.moveTo(440,80);
		ctx.lineTo(665,80);
		ctx.lineTo(665,150);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.moveTo(320,80);
		ctx.lineTo(320,30);
		ctx.lineTo(480,30);
		ctx.lineTo(480,80);
		ctx.stroke();
		
		uzol(320,80);
		uzol(480,80);
		rezistor(400,80,0,'   R');
		voltmeter(400,30);
		vypinac(220,80,spustene);
	}
	
	this.kresli_most = function(x,y,farba){
		var klesajuca_alfa_farby = 0.5;
		ctx.fillStyle 	= farba;
		ctx.strokeStyle = '#000000';
		ctx.lineWidth 	= 4;
		ctx.setLineDash([0]);
		ctx.beginPath();	
		ctx.moveTo(x,y + 70);
		ctx.lineTo(x,y - 5);
		ctx.arc(x + 50,y - 5,50,1*Math.PI,1.5*Math.PI);
		ctx.lineTo(x + 250,y - 55);
		ctx.arc(x + 250,y - 5,50,1.5*Math.PI,0);
		ctx.lineTo(x + 300,y + 70);
		ctx.moveTo(x + 250,y + 70);
		ctx.lineTo(x + 250,y);
		ctx.arc(x + 245,y - 5,5,0,1.5*Math.PI,true);
		ctx.lineTo(x + 60,y - 10);
		ctx.arc(x + 55,y - 5,5,1.5*Math.PI,Math.PI,true);
		ctx.lineTo(x + 50,y + 70);
		ctx.fill();
		ctx.stroke();
		ctx.clearRect(x + 2,y + 50,46,20);
		ctx.clearRect(x + 252,y + 50,46,20);
		
		ctx.beginPath(); 
		ctx.fillStyle = 'rgba(170, 170, 170, '+intenzita_farby[0]+')';
		ctx.rect(x + 2,y + 50,46,20);
		ctx.fill();
		
		ctx.beginPath(); 
		ctx.fillStyle = 'rgba(51, 136, 197, '+intenzita_farby[1]+')';
		ctx.rect(x + 252,y + 50,46,20);
		ctx.fill();
		
		for (i = 0;i < 10;i++){
			ctx.beginPath(); 
			ctx.fillStyle = 'rgba(51, 136, 197, '+klesajuca_alfa_farby+')';
			ctx.rect(x + 2,y + 50 + (i * 2),46,2);
			ctx.fill();
			
			ctx.beginPath(); 
			ctx.fillStyle = 'rgba(51, 136, 197, '+klesajuca_alfa_farby+')';
			ctx.rect(x + 252,y + 50 + (i * 2),46,2);
			ctx.fill();
			klesajuca_alfa_farby -= 0.05;
		}
		
		ctx.fillStyle = '#000000';
		ctx.font="18px Tahoma, Geneva, sans-serif";
		ctx.fillText('Roztok KNO3',x + 99,y - 62);
	}

	this.kresli_molekuly = function(){			
		this.kresli_molekuly_nadoba_1();
		this.kresli_molekuly_nadoba_2();
		this.kresli_molekuly_most();
		this.kresli_atomy_v_pohybe();		
	}	
	
	this.kresli_atomy_v_pohybe = function(){
		for (i = 0;i < atom_v_pohybe.length; i++){
			if (atom_v_pohybe[i][0]){
				if (atom_v_pohybe[i][1] == 'cu'){
					cu2plus(atom_v_pohybe[i][2],atom_v_pohybe[i][3]);	
				}
				if (atom_v_pohybe[i][1] == 'k'){
					kplus(atom_v_pohybe[i][2],atom_v_pohybe[i][3]);	
				}
				if (atom_v_pohybe[i][1] == 'no3'){
					no3minus(atom_v_pohybe[i][2],atom_v_pohybe[i][3]);	
				}
			}
		}
	}
	
	this.kresli_molekuly_nadoba_1 = function(){
		for (i = 0; i < nadoba_1.so42minus.length; i++){
			so42minus(nadoba_1.so42minus[i][0],nadoba_1.so42minus[i][1]);	
		}
		for (i = 0; i < nadoba_1.zn2plus.length; i++){
			zn2plus(nadoba_1.zn2plus[i][0],nadoba_1.zn2plus[i][1]);	
		}
		for (i = 0; i < nadoba_1.no3minus.length; i++){
			no3minus(nadoba_1.no3minus[i][0],nadoba_1.no3minus[i][1]);	
		}
	}
		
	this.kresli_molekuly_nadoba_2 = function(){
		for (i = 0; i < nadoba_2.so42minus.length; i++){
			so42minus(nadoba_2.so42minus[i][0],nadoba_2.so42minus[i][1]);	
		}
		for (i = 0; i < nadoba_2.cu2plus.length; i++){
			cu2plus(nadoba_2.cu2plus[i][0],nadoba_2.cu2plus[i][1]);	
		}
		for (i = 0; i < nadoba_2.kplus.length; i++){
			kplus(nadoba_2.kplus[i][0],nadoba_2.kplus[i][1]);	
		}
	}
	
	this.kresli_molekuly_most = function(){
		for (i = 0; i < most.kplus.length; i++){
			kplus(most.kplus[i][0],most.kplus[i][1]);	
		}
		for (i = 0; i < most.no3minus.length; i++){
			no3minus(most.no3minus[i][0],most.no3minus[i][1]);	
		}		
	}
	
	this.zmen_cudlik_on_off = function(io){
		if (io){
			document.getElementById('button_img_on_off').src = 'images/on_button.png';
		}else{
			document.getElementById('button_img_on_off').src = 'images/off_button.png';
		}
	}
	
	this.animacka_main = function(){	
		var pom_obj = this;
		interval_animacie = setInterval(function(){
			pom_obj.Kresli();
			
			if (spustene){
				if ((glob_cas % 20) == 0){	//nahodny pohyb castic
					pom_obj.generuj_cielove_suradnice_castic_nadoba_1();
					pom_obj.generuj_cielove_suradnice_castic_nadoba_2();
				}
				
				if (krok_reakcie == 0){
					if (nadoba_2.cu2plus.length > 0){
						pom_obj.zinok_odovzdava_elekrony();
						krok_reakcie = 1;
						pom_obj.prepocitaj_napatie_pri_zmene();
					}else{
						krok_reakcie = 99;	// koniec
					}
				}else if(krok_reakcie == 2){
					pom_obj.med_prijima_elektrony();
					krok_reakcie = 3;
					roztok_2 	 -= 0.25; 
					pom_obj.prepocitaj_napatie_pri_zmene();
				}else if (krok_reakcie == 3){
					pom_obj.pohyb_atomov();
				}	
								
				if (elektrony[0]){	pom_obj.pohyb_elektronov();	} 
				if (elektrony[3]){	pom_obj.kresli_elektrony(); } 
				
				pom_obj.pohyb_castic_v_nadobach();
				
				glob_cas++;
			}
		},20);
	}
	
	this.kresli_elektrony = function(){
		elektron(elektrony[1][0],elektrony[1][1]); 
		elektron(elektrony[2][0],elektrony[2][1]);
	}
	
	this.generuj_cielove_suradnice_castic_nadoba_1 = function(){
		var pocet_krokov  = 100;	
		var index_nitratu = [0,2,4,6];
		for (i = 0; i < nadoba_1.zn2plus.length; i++){
			if (nadoba_1.zn2plus[i][6] <= 0){
				pocet_krokov += (getRandomInt(0,5) * 20); 
				
				nadoba_1.zn2plus[i][2] = getRandomInt(170,320);
				nadoba_1.zn2plus[i][3] = getRandomInt(280,450);							
				nadoba_1.zn2plus[i][4] = this.vypocitaj_velkost_kroku(nadoba_1.zn2plus[i][0],nadoba_1.zn2plus[i][2],pocet_krokov);
				nadoba_1.zn2plus[i][5] = this.vypocitaj_velkost_kroku(nadoba_1.zn2plus[i][1],nadoba_1.zn2plus[i][3],pocet_krokov);
				nadoba_1.zn2plus[i][6] = pocet_krokov;
				
				if (typeof nadoba_1.so42minus[i] != 'undefined'){
					nadoba_1.so42minus[i][2] = nadoba_1.zn2plus[i][2] + getRandomInt(-20,20);
					nadoba_1.so42minus[i][3] = nadoba_1.zn2plus[i][3] + getRandomInt(-20,20);
					nadoba_1.so42minus[i][4] = this.vypocitaj_velkost_kroku(nadoba_1.so42minus[i][0],nadoba_1.so42minus[i][2],pocet_krokov);
					nadoba_1.so42minus[i][5] = this.vypocitaj_velkost_kroku(nadoba_1.so42minus[i][1],nadoba_1.so42minus[i][3],pocet_krokov);
					nadoba_1.so42minus[i][6] = pocet_krokov;					
				}else{		
					i_n = index_nitratu[i - 4];
					for (k = 0; k < 2; k++){
						if (typeof nadoba_1.no3minus[i_n + k] != 'undefined'){
							nadoba_1.no3minus[i_n + k][2] = nadoba_1.zn2plus[i][2] + getRandomInt(-20,20);
							nadoba_1.no3minus[i_n + k][3] = nadoba_1.zn2plus[i][3] + getRandomInt(-20,20);
							nadoba_1.no3minus[i_n + k][4] = this.vypocitaj_velkost_kroku(nadoba_1.no3minus[i_n + k][0],nadoba_1.no3minus[i_n + k][2],pocet_krokov);
							nadoba_1.no3minus[i_n + k][5] = this.vypocitaj_velkost_kroku(nadoba_1.no3minus[i_n + k][1],nadoba_1.no3minus[i_n + k][3],pocet_krokov);
							nadoba_1.no3minus[i_n + k][6] = pocet_krokov;	
						}
					}
				}		
				pocet_krokov = 100;	
			}	
		}
	}	
	
	this.generuj_cielove_suradnice_castic_nadoba_2 = function(){	
		var pocet_krokov  = 100;	
		var index_drasliku = [6,4,2,0];
		for (i = 0; i < nadoba_2.so42minus.length; i++){
			if (nadoba_2.so42minus[i][6] <= 0){
				pocet_krokov += (getRandomInt(0,5) * 20); 
				
				nadoba_2.so42minus[i][2] = getRandomInt(470,620);
				nadoba_2.so42minus[i][3] = getRandomInt(280,450);							
				nadoba_2.so42minus[i][4] = this.vypocitaj_velkost_kroku(nadoba_2.so42minus[i][0],nadoba_2.so42minus[i][2],pocet_krokov);
				nadoba_2.so42minus[i][5] = this.vypocitaj_velkost_kroku(nadoba_2.so42minus[i][1],nadoba_2.so42minus[i][3],pocet_krokov);
				nadoba_2.so42minus[i][6] = pocet_krokov;
				
				if (typeof nadoba_2.cu2plus[i] != 'undefined'){
					nadoba_2.cu2plus[i][2] = nadoba_2.so42minus[i][2] + getRandomInt(-20,20);
					nadoba_2.cu2plus[i][3] = nadoba_2.so42minus[i][3] + getRandomInt(-20,20);
					nadoba_2.cu2plus[i][4] = this.vypocitaj_velkost_kroku(nadoba_2.cu2plus[i][0],nadoba_2.cu2plus[i][2],pocet_krokov);
					nadoba_2.cu2plus[i][5] = this.vypocitaj_velkost_kroku(nadoba_2.cu2plus[i][1],nadoba_2.cu2plus[i][3],pocet_krokov);
					nadoba_2.cu2plus[i][6] = pocet_krokov;					
				}else{		
					i_n = index_drasliku[i];
					for (k = 0; k < 2; k++){
						if (typeof nadoba_2.kplus[i_n + k] != 'undefined'){
							nadoba_2.kplus[i_n + k][2] = nadoba_2.so42minus[i][2] + getRandomInt(-20,20);
							nadoba_2.kplus[i_n + k][3] = nadoba_2.so42minus[i][3] + getRandomInt(-20,20);
							nadoba_2.kplus[i_n + k][4] = this.vypocitaj_velkost_kroku(nadoba_2.kplus[i_n + k][0],nadoba_2.kplus[i_n + k][2],pocet_krokov);
							nadoba_2.kplus[i_n + k][5] = this.vypocitaj_velkost_kroku(nadoba_2.kplus[i_n + k][1],nadoba_2.kplus[i_n + k][3],pocet_krokov);
							nadoba_2.kplus[i_n + k][6] = pocet_krokov;	
						}
					}
				}			
				pocet_krokov = 100;	
			}	
		}
	}
	
	this.pohyb_castic_v_nadobach = function(){
		for (var p in nadoba_1) {
			if (nadoba_1.hasOwnProperty(p)) {
				for (i = 0; i < nadoba_1[p].length; i++){
					if (nadoba_1[p][i][6] > 0){
						nadoba_1[p][i][0] += nadoba_1[p][i][4];
						nadoba_1[p][i][1] += nadoba_1[p][i][5];	
						nadoba_1[p][i][6]--;
					}
				}
			}
		}
		for (var p in nadoba_2) {
			if (nadoba_2.hasOwnProperty(p)) {
				for (i = 0; i < nadoba_2[p].length; i++){
					if (nadoba_2[p][i][6] > 0){
						nadoba_2[p][i][0] += nadoba_2[p][i][4];
						nadoba_2[p][i][1] += nadoba_2[p][i][5];	
						nadoba_2[p][i][6]--;
					}	
				}
			}
		}
	}
	
	this.pohyb_atomov = function(){
		var reset_reakcie = true;
		for (i = 0;i < atom_v_pohybe.length; i++){
			if (atom_v_pohybe[i][0]){
				reset_reakcie = false;
				if (atom_v_pohybe[i][1] == 'cu'){	
					this.pohyb_medi_k_elektrode();
				}
				if (atom_v_pohybe[i][1] == 'k'){	
					this.pohyb_drasliku_z_mosta_so_nadoby_2(i);
				}
				if (atom_v_pohybe[i][1] == 'no3'){	
					this.pohyb_nitratu_z_mosta_so_nadoby_2(i);
				}
			}
		}
		if (reset_reakcie){ krok_reakcie = 0; }
	}
	
	this.pohyb_drasliku_z_mosta_so_nadoby_2 = function(i){
		if (atom_v_pohybe[i][6] > 0){
			atom_v_pohybe[i][2] += atom_v_pohybe[i][4];
			atom_v_pohybe[i][3] += atom_v_pohybe[i][5];	
			atom_v_pohybe[i][6]--;
		}else{					
			if (atom_v_pohybe[i][2] > 530){							
				nadoba_2.kplus.push([atom_v_pohybe[i][2],atom_v_pohybe[i][3],atom_v_pohybe[i][4],atom_v_pohybe[i][5],0,0,0]);
				atom_v_pohybe[i][0]= false;
				naboj_nadoby_2++;
				if (i == 0){
					atom_v_pohybe[1] = this.vypocet_trasy_drasliku(most.kplus[most.kplus.length - 1][0],most.kplus[most.kplus.length - 1][1]);
					most.kplus.pop();
				}	
			}else{
				atom_v_pohybe[i]   = this.vypocet_trasy_drasliku(atom_v_pohybe[i][2],atom_v_pohybe[i][3]);
			}
		}	
	}
	
	this.pohyb_nitratu_z_mosta_so_nadoby_2 = function(i){
		if (atom_v_pohybe[i][6] > 0){
			atom_v_pohybe[i][2] += atom_v_pohybe[i][4];
			atom_v_pohybe[i][3] += atom_v_pohybe[i][5];	
			atom_v_pohybe[i][6]--;
		}else{			
			if (atom_v_pohybe[i][2] < 280){		
				nadoba_1.no3minus.push([atom_v_pohybe[i][2],atom_v_pohybe[i][3],atom_v_pohybe[i][4],atom_v_pohybe[i][5],0,0,0]);
				atom_v_pohybe[i][0]	= false;
				naboj_nadoby_1--;
				if (i == 2){
					atom_v_pohybe[3] = this.vypocet_trasy_nitratu(most.no3minus[most.no3minus.length - 1][0],most.no3minus[most.no3minus.length - 1][1]);
					most.no3minus.pop();
				}
			}else{
				atom_v_pohybe[i]   = this.vypocet_trasy_nitratu(atom_v_pohybe[i][2],atom_v_pohybe[i][3]);
			}
		}	
	}
	
	this.pohyb_medi_k_elektrode = function(){
		if (atom_v_pohybe[0][6] > 0){
			atom_v_pohybe[0][2] += atom_v_pohybe[i][4];
			atom_v_pohybe[0][3] += atom_v_pohybe[i][5];	
			atom_v_pohybe[0][6]--;
		}else{		
			elektrony[3] 	   	 = false;							
			sirky_elektrod[1] 	+= 5;
			intenzita_farby[1]	-= 0.15;	
			atom_v_pohybe[0]   	 = this.vypocet_trasy_drasliku(most.kplus[most.kplus.length - 1][0],most.kplus[most.kplus.length - 1][1]);
			atom_v_pohybe[2] 	 = this.vypocet_trasy_nitratu(most.no3minus[most.no3minus.length - 1][0],most.no3minus[most.no3minus.length - 1][1]);
			naboj_nadoby_2		 = -2;
			
			most.kplus.pop();
			most.no3minus.pop();			
		}	
	}
	
	this.pohyb_elektronov = function(){		
		if ((elektrony[1][1] > 80) && (elektrony[2][0] != 665)){
			elektrony[1][1] -= 2;
			if (elektrony[1][1] < 80){elektrony[1][1] = 80;}
		}else if((elektrony[1][1] >= 80) && (elektrony[1][0] < 665)){
			elektrony[1][0] += 2;
			if (elektrony[1][0] > 665){elektrony[1][0] = 665;}
		}else{
			elektrony[1][1] += 2;
			if (elektrony[1][1] >= el_na_elektode){ //koniec trasy v medennej elektrode
				elektrony[0] = false;
				krok_reakcie = 2;
			}	
		}
		if ((elektrony[2][1] > 80) && (elektrony[2][0] != 665)){
			elektrony[2][1] -= 2;
			if (elektrony[2][1] < 80){elektrony[2][1] = 80;}
		}else if((elektrony[2][1] >= 80) && (elektrony[2][0] < 665)){
			elektrony[2][0] += 2;
			if (elektrony[2][0] > 665){elektrony[2][0] = 665;}
		}else{
			elektrony[2][1] += 2;
		}
	}
	
	this.zinok_odovzdava_elekrony = function(){
		intenzita_farby[0]	+= 0.15;	
		roztok_1 			+= 0.25; 
		nova_x 				= 135;
		nova_y 				= getRandomInt(270,400);
		el_na_elektode 		= nova_y;
		
		nadoba_1.zn2plus.push([nova_x,nova_y,nova_x + 160,nova_y,1,0,100]);
		
		elektrony[0] 	= true;
		elektrony[3] 	= true;		
		elektrony[1][0] = nova_x;
		elektrony[1][1] = nova_y + 15;		
		elektrony[2][0] = nova_x;
		elektrony[2][1] = nova_y - 15;	
		
		sirky_elektrod[0]  -= 5;		
		naboj_nadoby_1 		= 2;
	}
	
	this.med_prijima_elektrony = function(){
		var last = nadoba_2.cu2plus.length - 1;
		var x1 	 = nadoba_2.cu2plus[last][0];
		var y1 	 = nadoba_2.cu2plus[last][1];
		var x2 	 = 665;
		var y2 	 = el_na_elektode + 15;
		var pocet_krokov = this.vypocitaj_pocet_krokov(this.vypocitaj_vydialenost(x1,y1,x1,x2));
		var krok_po_x 	 = this.vypocitaj_velkost_kroku(x1,x2,pocet_krokov);
		var krok_po_y 	 = this.vypocitaj_velkost_kroku(y1,y2,pocet_krokov);
		
		atom_v_pohybe[0] = [true,'cu',x1,y1,krok_po_x,krok_po_y,pocet_krokov];
		
		nadoba_2.cu2plus.pop();
	}
	
	this.vypocet_trasy_drasliku = function(x1,y1){
		var x2,y2;
		
		if (x1 < 290){
			x2 = 295;
			y2 = 175;
		}else if ((x1 >= 290) && (x1 < 500)){
			x2 = 510;
			y2 = 165;
		}else if (x1 >= 500){
			x2 = 540 + getRandomInt(-8,10);
			y2 = 320 + getRandomInt(-40,40);
		}
		
		var krok_po_x 	 = this.vypocitaj_velkost_kroku(x1,x2,casovacudalosti);
		var krok_po_y 	 = this.vypocitaj_velkost_kroku(y1,y2,casovacudalosti);
	
		return [true,'k',x1,y1,krok_po_x,krok_po_y,casovacudalosti];
	}
	
	this.vypocet_trasy_nitratu = function(x1,y1){
		var x2,y2;
		
		 if (x1 >= 505){
			x2 = 500;
			y2 = 170;
		}else if ((x1 >= 290) && (x1 < 505)){
			x2 = 285;
			y2 = 165;
		}else if (x1 < 290){
			x2 = 275 + getRandomInt(-10,3);
			y2 = 350 + getRandomInt(-40,40);
		}
		
		var krok_po_x 	 = this.vypocitaj_velkost_kroku(x1,x2,casovacudalosti);
		var krok_po_y 	 = this.vypocitaj_velkost_kroku(y1,y2,casovacudalosti);
	
		return [true,'no3',x1,y1,krok_po_x,krok_po_y,casovacudalosti];
	}
	
	this.dalsia_uloha = function(cast){
		tutorial_cast = cast; 
		this.tutorial();
	}
	
	this.tutorial = function(){
		var pom_ob = this;
		var texty  = [	
						'Zapnite experiment.',
						'Nastavte teplotu na 500 K.'
					];	
		if(document.getElementById('tutorial_text_' + tutorial_cast) != null){
			document.getElementById('tutorial_text_1').innerHTML = texty[0];
			document.getElementById('tutorial_text_2').innerHTML = texty[1];
			document.getElementById('tutorial_text_1').style.color = '#dddddd';
			document.getElementById('tutorial_text_2').style.color = '#dddddd';
			switch(tutorial_cast){
				case 1:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					if (spustene){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_1').innerHTML = texty[0] + '   <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_1').addEventListener("click",function() {pom_ob.dalsia_uloha(2)});					
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 2:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					if (T == 500){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_2').innerHTML = texty[1] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_2').addEventListener("click",function() {pom_ob.dalsia_uloha(3)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;	
				case 3:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_' + tutorial_cast).innerHTML = 'Hotovo.';
					break;
			}
		}
	}
}