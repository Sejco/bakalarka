var pocetOtazok = 1;

function defaultHodnotaIntupu(elemId,pozadovanaHodnota){
	if (skontrolujPozadovanuHodnotu(pozadovanaHodnota,document.getElementById(elemId).value)){
		document.getElementById(elemId).value = '';
	}
}

function emptyHodnotaIntupu(elemId,pozadovanaHodnota){
	if (document.getElementById(elemId).value == ""){
		document.getElementById(elemId).value = pozadovanaHodnota;
		zmenStylInputTextu(elemId,'sedy');
	}
}

function skontrolujPozadovanuHodnotu(pozadovanaHodnota,hodnota){
	if (pozadovanaHodnota == hodnota){
		return true;
	}
	return false;
}

function zmenStylInputTextu(elemId,styl){
	switch (styl){
		case 'cierny':
			document.getElementById(elemId).style.color = "#000000"; 
			document.getElementById(elemId).style.fontStyle = "normal"; 
		break;
		
		case 'sedy':
			document.getElementById(elemId).style.color = "#777777"; 
			document.getElementById(elemId).style.fontStyle = "italic"; 
		break;	
	}
}

function zmenTypInputu(elemId,typ){
	document.getElementById(elemId).type = typ;
}

function zistiZmenuTypuHesla(elemId){
	if (document.getElementById(elemId).value == ''){
		zmenTypInputu(elemId,'text');
	}
}

function odoberOtazku(cisloOtazky){
	pocetOtazok--;
	var row0 = document.getElementById('otazka_' + cisloOtazky);
	row0.parentNode.removeChild(row0);
	
	var row1 = document.getElementById('moznosti_' + cisloOtazky);
	row1.parentNode.removeChild(row1);
	
	var row2 = document.getElementById('bodyTr_' + cisloOtazky);
	row2.parentNode.removeChild(row2);	
}

function pridajOtazku(tableId) {
	pocetOtazok++;
	var table = document.getElementById(tableId);
	
	var row1 = table.insertRow(-1);
	var row2 = table.insertRow(-1);
	var row3 = table.insertRow(-1);
	row1.className = 'editorTestovMedzeraMedziRiadkami';
	row1.id = 'otazka_' + pocetOtazok;
	row2.id = 'moznosti_' + pocetOtazok;
	row3.id = 'bodyTr_' + pocetOtazok;
	
	var cell11 = row1.insertCell(0);
	var cell12 = row1.insertCell(1);
	cell11.innerHTML = 'Otázka ' + pocetOtazok;	
	cell12.colSpan = 8;
	
	var cell21 = row2.insertCell(0);
	var cell22 = row2.insertCell(1);
	var cell13 = row2.insertCell(2);
	cell21.innerHTML = 'Možnosti';
	
	var cell31 = row3.insertCell(0);
	var cell32 = row3.insertCell(1);
	var cell33 = row3.insertCell(2);
	cell31.innerHTML = 'Počet bodov';
		
    var input1 = vytvorInputTextarea(pocetOtazok);	
	var input2 = vytvorInputPreMoznosti(row2.id, true);	
	var input3 = vytvorInputPreButton(row2.id);
	var input4 = vytvorInputPreBody(pocetOtazok);
	var input5 = vytvorInputPreButtonDeleteOtazky(pocetOtazok);
	
	cell12.appendChild(input1);
	cell22.appendChild(input2);	
	cell13.appendChild(input3);	
	cell32.appendChild(input4);
	cell33.appendChild(input5);
}

function pridajMoznost(rowId){
	var row = document.getElementById(rowId);
	if (document.getElementById(rowId).cells.length < 7){
		var cell = row.insertCell(document.getElementById(rowId).cells.length - 1);
		cell.className = "inputEditorTestov";
		
		var input = vytvorInputPreMoznosti(rowId, false);
		
		cell.appendChild(input);	
	}
	odstranCudlikPridaniaBunky(rowId);
}

function odstranCudlikPridaniaBunky(rowId){
	var row = document.getElementById(rowId);
	if (document.getElementById(rowId).cells.length == 7){
		row.deleteCell(-1);
	}
}

function vytvorInputPreMoznosti(rowId, spravna){
	cisloOtazky 	= ziskajCisloRiadkuZoStringu(rowId);
	cisloMoznosti	= document.getElementById(rowId).cells.length - 3;

	var input = document.createElement('input');
	
	input.type = "text";
	input.name = 'm' + cisloOtazky + '[]';
	input.id   = 'm' + cisloOtazky + '' + cisloMoznosti;

	if (spravna){
		input.className = 'inputEditorTestov spravnaMoznost';
	}else{
		input.className = 'inputEditorTestov';
	}
	
	return input;
}

function vytvorInputPreButton(rowId){
	var input = document.createElement( 'input' );
	input.type = "button";
	input.className = 'mybutton';
	input.value = 'Pridaj možnosť';
	input.onclick = function () {pridajMoznost(rowId);};
	
	return input;
}

function vytvorInputPreButtonDeleteOtazky(cisloOtazky){
	var input = document.createElement( 'input' );
	input.type = "button";
	input.className = 'mybutton';
	input.value = 'Odstrániť otázku';
	input.onclick = function () {odoberOtazku(cisloOtazky);};
	
	return input;
}

function vytvorInputPreBody(cisloOtazky){
	var input = document.createElement( 'input' );
	input.type 		= "number";
	input.value 	= 0;
	input.min 		= 0;
	input.max 		= 999999;
	input.className = 'editorTestovPrvyStlpec';
	input.name 		= 'body_' + cisloOtazky;
	input.id		= 'body_' + cisloOtazky;
	input.oninput   = function () {kontrola_vstupu_bez_blank('body_' + cisloOtazky, 0, 999999);};
	
	return input;
}

function vytvorInputTextarea(cisloOtazky){
	var input = document.createElement( 'textarea' );
	input.className = 'textAreaEditorTestov';
	input.name 		= 'q' + cisloOtazky;
	input.id		= 'q' + cisloOtazky;
	
	return input;
}

function ziskajCisloRiadkuZoStringu(retazec){
	return retazec.substring(9);
}
