function Kirchoff(){
	var zlozitostObvodu 	= 0;
	var ukazSipky			= 0;
	var ukazSlucky			= 0;
	var ukazPrudy			= 1;
	var spustene			= false;
	var posunAnimPrudu  	= 0;
	var aktualnySmerPruduO0 = [0,0,0];
	var smeryPrudovO0		= [[[1,2,4],[3,4,2]],[[1,4,2],[3,2,4]]];
	var poleSipokI1O0   	= [[400,370,340,310,280,250,220,190,160,130],[210,240,270,300,330,360],[380,350,320,290,260,230]];
	var poleSipokI2O0   	= [[400,370,340,310,280,250,220,190,160,130],[420,450,480,510,540,570],[550,520,490,460,430,400]];
	var poleSipokI3O0   	= [380,350,320,290,260,230,200,170,140,110];
	var farbyprudovO0		= ['#33DD00','#CC55EE','#FFAA00'];
	var tutorial_cast		= 1;
	var tutorial_uloha_3 	= false;
	var tutorial_uloha_4 	= false;
	var intervalPrudu;
	
	this.Create = function(){
		this.Kresli();
		this.zobrazInputy(zlozitostObvodu);
	}
	
	this.Run = function(){
		spustene = !spustene;	
		this.tutorial();	
		if (spustene){
			this.animackaMain();
		}else{
			clearInterval(intervalPrudu);
		}	
		
		this.ukazPruduObvodLvl0();
		this.Kresli();
		this.zmenOnOffCudlik();
	}
	
	this.Kresli = function(){
		ctx.clearRect ( 0 , 0 , c.width , c.height );
		this.KresliObvod(zlozitostObvodu);
	}
	
	this.zobrazInputy = function(n){
		if (n == 0){
			document.getElementById('obvod2').style.display = 'block';
		}
	}
	
	this.zmenHodnotuSuciastky = function(elemId,hodnota){
		document.getElementById(elemId).value = hodnota;
	}
	
	this.zmenHodnotuNadSliderom = function(elemId,hodnota,jednotka){
		document.getElementById(elemId).innerHTML = hodnota+' '+jednotka;	
	}
	
	this.zmenOnOffCudlik = function(){
		if (spustene){
			document.getElementById('button_img_on_off').src = 'images/on_button.png';
		}else{
			document.getElementById('button_img_on_off').src = 'images/off_button.png';
		}
	}
	
	this.ukazPruduObvodLvl0 = function(){		
		if (spustene){
			var prudy = this.opravSmeryPrudovO0(this.vypocitajPrudyObvodLvl0());
			
			if ((prudy[1] >  prudy[0]) && (prudy[1] > prudy[2])){
				tutorial_uloha_3 = true;
			}else{
				tutorial_uloha_3 = false;
			}
			if ((prudy[0] == 0.05) && (prudy[1] == 0.03) && (prudy[2] == 0.08)){
				tutorial_uloha_4 = true;
			}else{
				tutorial_uloha_4 = false;
			} 
			document.getElementById('A1O0').value = prudy[0].toFixed(4);
			document.getElementById('A2O0').value = prudy[1].toFixed(4);
			document.getElementById('A3O0').value = prudy[2].toFixed(4);	
		}else{
			document.getElementById('A1O0').value = '0';
			document.getElementById('A2O0').value = '0';
			document.getElementById('A3O0').value = '0';
		}	
	}
	
	this.opravSmeryPrudovO0 = function(prudy){
		for (i = 0; i < prudy.length; i++){
			if (prudy[i] < 0){
				prudy[i] *= -1;
				aktualnySmerPruduO0[i] = 1;
			}else{
				aktualnySmerPruduO0[i] = 0;
			}		
		}
		return prudy;
	}	
	
	this.vypocitajPrudyObvodLvl0 = function(){
		var pomMatica = [[],[]];
		var U1 = parseFloat(document.getElementById('U1O0').value);
		var U2 = parseFloat(document.getElementById('U2O0').value);
		var R1 = parseFloat(document.getElementById('R1O0').value);
		var R2 = parseFloat(document.getElementById('R2O0').value);
		var R3 = parseFloat(document.getElementById('R3O0').value);
		
		var mat1  = [ R1 + R3,  R3     ,  U1];
		var mat2  = [-R3     , -R3 - R2, -U2];
		var matica = [mat1,mat2];

		
		pomMatica[0] = vynasobRiadokMatice(matica[0].slice(0),matica[1][0]); 
		pomMatica[1] = vynasobRiadokMatice(matica[1].slice(0),matica[0][0]); 
		
		matica = pomMatica.slice(0);
		
		if (matica[0][0] == matica[1][0]){ 
			pomMatica[0] = vynasobRiadokMatice(matica[0].slice(0),-1); 
			matica = pomMatica.slice(0);
		}
		
		matica[0].splice(0,1);
		matica[1].splice(0,1);
		
		var I2 = (matica[0][1] + matica[1][1]) / (matica[0][0] + matica[1][0]);
		var I1 = (U1 - I2*R3) / (R1 + R3); 
		var I3 = I1 + I2;
		
		return [I1,I2,I3];	
	}
	
	this.KresliObvod = function(zlozitostObvodu){
		switch(zlozitostObvodu){
			case 0:
				this.KresliObvodLvl0();
			break;
			case 1:
				this.KresliObvodLvl1();
			break;			
		}
	}
	
	this.KresliObvodLvl0 = function(){
		this.KresliVodiceObvodLvl0();
		this.KresliUzlyObvodLvl0();
		if (ukazSlucky){
			slucka(295,270,'1.',1);
			slucka(485,270,'2.',1);
		}
		if (spustene){
			this.kresliPrudyO1();
		}
		striedavyZdroj(200,320,3,'U1',ukazSipky);
		striedavyZdroj(580,320,3,'U2',ukazSipky);
		
		kzRezistor(200,190,1,'R1',ukazSipky);
		kzRezistor(390,190,3,'R3',ukazSipky);
		kzRezistor(580,190,1,'R2',ukazSipky);
		
		ampermeter(295,410,farbyprudovO0[0]);
		ampermeter(390,320,farbyprudovO0[2]);
		ampermeter(485,410,farbyprudovO0[1]);
	}
	
	this.KresliObvodLvl1 = function(){
		this.KresliVodiceObvodLvl1();
		this.KresliUzlyObvodLvl1();
		if (ukazSlucky){
			this.kresliSluckyLvl1();
		}
		striedavyZdroj(100,250,3,'U1',ukazSipky);
		striedavyZdroj(465,100,4,'U2',ukazSipky);
		striedavyZdroj(530,320,3,'U3',ukazSipky);
		striedavyZdroj(680,250,1,'U4',ukazSipky);
		
		kzRezistor(250,250,3,'R4',ukazSipky);
		kzRezistor(530,200,1,'R3',ukazSipky);
		kzRezistor(340,100,2,'R1',ukazSipky);
		kzRezistor(390,400,4,'R2',ukazSipky);
	}
	
	this.KresliVodiceObvodLvl0 = function(){
		ctx.lineWidth = 2;
		ctx.strokeStyle = '#000000';
		ctx.beginPath();
		ctx.moveTo(200,100);
		ctx.lineTo(580,100);
		ctx.lineTo(580,410);
		ctx.lineTo(200,410);
		ctx.lineTo(200,100);		
		ctx.moveTo(390,100);
		ctx.lineTo(390,410);			
		ctx.stroke();
	}
	
	this.KresliVodiceObvodLvl1 = function(){
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.moveTo(100,100);
		ctx.lineTo(680,100);
		ctx.lineTo(680,400);
		ctx.lineTo(100,400);
		ctx.lineTo(100,100);		
		ctx.moveTo(250,100);
		ctx.lineTo(250,400);		
		ctx.moveTo(530,100);
		ctx.lineTo(530,400);	
		ctx.stroke();
	}
	
	this.KresliUzlyObvodLvl0 = function(){		
		uzol(390,100);
		uzol(390,410);
		
		ctx.fillStyle = '#000000';
		ctx.font="15px Helvetica";
		ctx.fillText("A",385, 90);
		ctx.fillText("B",385, 430);
		ctx.stroke();
	}
	
	this.KresliUzlyObvodLvl1 = function(){		
		uzol(250,100);
		uzol(250,400);
		uzol(530,100);
		uzol(530,400);	
		
		ctx.fillStyle = '#000000';
		ctx.font="15px Helvetica";
		ctx.fillText("A",245, 90);
		ctx.fillText("C",245, 420);
		ctx.fillText("B",525, 90);
		ctx.fillText("D",525, 420);
		ctx.stroke();
	}
	
	this.kresliSluckyLvl1 = function(){
		slucka(175,200,'1.',1);
		slucka(390,200,'2.',1);
		slucka(605,200,'3.',1);
	}
	
	this.kresliPrudyO1 = function(){
		this.kresliPrudI1ObvodLvl0();
		this.kresliPrudI2ObvodLvl0();
		this.kresliPrudI3ObvodLvl0();
	}
	
	this.kresliPrudI1ObvodLvl0 = function(){
		var farba 				= farbyprudovO0[0];
		var pomPosunAnimPrudu 	= posunAnimPrudu;
		var korekciaPosunuSipok = 0;
		if (aktualnySmerPruduO0[0] == 1){
			pomPosunAnimPrudu 	= 30 - pomPosunAnimPrudu;
			korekciaPosunuSipok = 10;
		}
		for (i = 0; i < poleSipokI1O0[0].length; i++){
			sipka(200,poleSipokI1O0[0][i] + korekciaPosunuSipok - pomPosunAnimPrudu, smeryPrudovO0[0][aktualnySmerPruduO0[0]][0], farba,2);	
		}
		for (i = 0; i < poleSipokI1O0[1].length; i++){
			sipka(poleSipokI1O0[1][i] - korekciaPosunuSipok + pomPosunAnimPrudu,100, smeryPrudovO0[0][aktualnySmerPruduO0[0]][1], farba,2);	
		}
		for (i = 0; i < poleSipokI1O0[2].length; i++){
			sipka(poleSipokI1O0[2][i] + korekciaPosunuSipok - pomPosunAnimPrudu,410, smeryPrudovO0[0][aktualnySmerPruduO0[0]][2], farba,2);	
		}
		ctx.beginPath();
		ctx.fillStyle = farbyprudovO0[0];
		ctx.font="20px Arial";
		ctx.fillText("I1",285,80);
		ctx.stroke();
	}
	
	this.kresliPrudI2ObvodLvl0 = function(){
		var farba = farbyprudovO0[1];
		var pomPosunAnimPrudu 	= posunAnimPrudu;
		var korekciaPosunuSipok = 0;
		if (aktualnySmerPruduO0[1] == 1){
			pomPosunAnimPrudu 	= 30 - pomPosunAnimPrudu;
			korekciaPosunuSipok = 10;
		}
		for (i = 0; i < poleSipokI2O0[0].length; i++){
			sipka(580,poleSipokI2O0[0][i] + korekciaPosunuSipok - pomPosunAnimPrudu, smeryPrudovO0[1][aktualnySmerPruduO0[1]][0], farba,2);	
		}
		for (i = 0; i < poleSipokI2O0[1].length; i++){
			sipka(poleSipokI2O0[1][i] + korekciaPosunuSipok - pomPosunAnimPrudu,100, smeryPrudovO0[1][aktualnySmerPruduO0[1]][1], farba,2);	
		}
		for (i = 0; i < poleSipokI2O0[2].length; i++){
			sipka(poleSipokI2O0[2][i] - korekciaPosunuSipok + pomPosunAnimPrudu,410, smeryPrudovO0[1][aktualnySmerPruduO0[1]][2], farba,2);	
		}
		ctx.beginPath();
		ctx.fillStyle = farbyprudovO0[1];
		ctx.font="20px Arial";
		ctx.fillText("I2",475,80);
		ctx.stroke();
	}
	
	this.kresliPrudI3ObvodLvl0 = function(){
		var farba = farbyprudovO0[2];
		for (i = 0; i < poleSipokI3O0.length; i++){
			sipka(390,poleSipokI3O0[i] + posunAnimPrudu,3,farba,2);	
		}
		ctx.beginPath();
		ctx.fillStyle = farbyprudovO0[2];
		ctx.font="20px Arial";
		ctx.fillText("I3",400,270);
		ctx.stroke();
	}
	
	this.animackaMain = function(){
		var pomObj	= this;
		intervalPrudu = setInterval(function(){
			pomObj.Kresli();
			posunAnimPrudu++;
			if (posunAnimPrudu == 30){
				posunAnimPrudu = 0;
			}
		}, 40);	
	}
	
	this.dalsia_uloha = function(cast){
		tutorial_cast = cast; 
		this.tutorial();
	}
	
	this.tutorial = function(){
		var pom_ob = this;
		var texty  = [	
						'Zapnite experiment.',						
						'Nastavte oba napäťové zdroje na hodnotu 2 V.',
						'Nastavte vstupné napätia tak, aby prúd I2 (fialový) sa rovnal súčtu I1 a I3.',
						'Nastavte vstupné parametre tak, aby I1 = 0.05 A, I2 = 0.03 A a I3 = 0.08 A.'
					];	
		if(document.getElementById('tutorial_text_' + tutorial_cast) != null){
			document.getElementById('tutorial_text_1').innerHTML = texty[0];
			document.getElementById('tutorial_text_2').innerHTML = texty[1];
			document.getElementById('tutorial_text_3').innerHTML = texty[2];
			document.getElementById('tutorial_text_4').innerHTML = texty[3];
			document.getElementById('tutorial_text_1').style.color = '#dddddd';
			document.getElementById('tutorial_text_2').style.color = '#dddddd';
			document.getElementById('tutorial_text_3').style.color = '#dddddd';
			document.getElementById('tutorial_text_4').style.color = '#dddddd';
			switch(tutorial_cast){
				case 1:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					if (spustene){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_1').innerHTML = texty[0] + '   <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_1').addEventListener("click",function() {pom_ob.dalsia_uloha(2)});					
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 2:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					if ((parseFloat(document.getElementById('U1O0').value) == 2) && (parseFloat(document.getElementById('U2O0').value) == 2)){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_2').innerHTML = texty[1] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_2').addEventListener("click",function() {pom_ob.dalsia_uloha(3)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;	
				case 3:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					if (tutorial_uloha_3){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_3').innerHTML = texty[2] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_3').addEventListener("click",function() {pom_ob.dalsia_uloha(4)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 4:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					if (tutorial_uloha_4){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_4').innerHTML = texty[3] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_4').addEventListener("click",function() {pom_ob.dalsia_uloha(5)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 5:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_' + tutorial_cast).innerHTML = 'Hotovo.';
					break;
			}
		}
	}
}