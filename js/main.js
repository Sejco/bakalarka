function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function changeBackground(color) {
   document.body.style.background = color;
}


var active_tab = (function(){
    var stateKey, eventKey, keys = {
        hidden: "visibilitychange",
        webkitHidden: "webkitvisibilitychange",
        mozHidden: "mozvisibilitychange",
        msHidden: "msvisibilitychange"
    };
    for (stateKey in keys) {
        if (stateKey in document) {
            eventKey = keys[stateKey];
            break;
        }
    }
    return function(c) {
        if (c) document.addEventListener(eventKey, c);
        return !document[stateKey];
    }
})();

function kelvin_to_celsius(K){
	return (K - 273);
}

function cista_plocha(){
	ctx.fillStyle = "#ffffff";
	ctx.fillRect (0, 0, c.width, c.height);
}

function zmen_value(elem,diff_value){
	hodnota = parseFloat(document.getElementById(elem).value);
	document.getElementById(elem).value =  hodnota + diff_value;
}

function nastav_value(elem,hodnota){
	document.getElementById(elem).value =  hodnota;
}

function francuzska_notacia(yourNumber) {
	var n = yourNumber.toString().split(".");
	n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	return n.join(".");
}

function elipsa(x,y,r,koef){
	ctx.beginPath();
	ctx.moveTo(x + r,y);
	for(var theta=0;  theta < 2*Math.PI;  theta+=0.2){  
		var xx = x +        r * Math.cos(theta) ;
		var yy = y - koef * r * Math.sin(theta) ;  
		ctx.lineTo(xx,yy);
	}
	ctx.stroke();
}

function vrat_pole_sur_elipsy(x,y,r,koef){
	var pole = [];
	for(var theta=-0.5*Math.PI;  theta < 1.5*Math.PI;  theta+=0.2){  
		var xx = x +        r * Math.cos(theta) ;
		var yy = y - koef * r * Math.sin(theta) ;  
		pole.push([xx,yy])
	}
	return pole;
}

function vrat_pole_sur_kruhu(r){
	var pole = [];
	for(var theta=0;  theta < 2*Math.PI;  theta+=0.2){  
		var xx = r * Math.cos(theta) ;
		var yy = r * Math.sin(theta) ;  
		pole.push([xx,yy])
	}
	return pole;
}

function kruznica(x,y,r,dotted,farba){
	ctx.beginPath();
	ctx.strokeStyle = farba;
	ctx.setLineDash([dotted]);
	ctx.arc(x,y,r,0,2*Math.PI);
	ctx.stroke();
}

function polkruh(x,y,r,dotted,farba,start,ciel){
	ctx.beginPath();
	ctx.strokeStyle = farba;
	ctx.setLineDash([dotted]);
	ctx.arc(x,y,r,start,ciel);
	ctx.stroke();
}

function al(text){
	ctx.fillStyle = '#000000';
	ctx.font="20px Tahoma, Geneva, sans-serif";
	ctx.fillText(text,150,100);
}

function getPosition(event)
{
  var x = event.x;
  var y = event.y;

  x -= c.offsetLeft;
  y -= c.offsetTop;

  al("x:" + x + " y:" + y);
}

function vynasobRiadokMatice(pole,nasobitel){
	for (i = 0; i <  pole.length; i++){
		pole[i] = pole[i] * nasobitel;
	}
	return pole;
}

function kontrola_vstupu_bez_blank(element,hranica_od,hranica_do){
	if (document.getElementById(element).value < hranica_od){
		document.getElementById(element).value = hranica_od;	
	}
	if (document.getElementById(element).value > hranica_do){
		document.getElementById(element).value = hranica_do;	
	}
	if (document.getElementById(element).value != "0"){
		document.getElementById(element).value = parseInt(document.getElementById(element).value) || "0";
	}
}

function kontrola_vstupu(element,hranica_od,hranica_do){
	if (document.getElementById(element).value < hranica_od){
		document.getElementById(element).value = hranica_od;	
	}
	if (document.getElementById(element).value > hranica_do){
		document.getElementById(element).value = hranica_do;	
	}
	if (document.getElementById(element).value != "0"){
		document.getElementById(element).value = parseInt(document.getElementById(element).value) || "";
	}
}
