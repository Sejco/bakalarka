function Polovodice() {
	var KONSTANTA_Q 		= 1.60217648740 * Math.pow(10,-19); //elektricky naboj
	var KONSTANTA_K 		= 1.380650388238137546253272195613524  * Math.pow(10,-23); //boltzmannova konstatna
	var moj_interval 		= 0;
	var start_x 			= 100;
	var start_y 			= 100;
	var posun   			= 110;
	var odskok 				= 0;
	var koniec_cyklu 		= 0; 
	var pokracuj_v_cykle 	= 0; 
	var smer_priechodu 		= 1;
	var spustene 			= false;
	var aktual_atom_x 		= 5;
	var aktual_atom_y 		= 2;
	var zaverny_smer_krok 	= 0;
	var aktualna_teplota 	= 300;
	var aktualne_napatie 	= 0.8;
	var aktualna_hustota	= -10;
	var aktualny_prvok		= 1;
	var faktor_idealnosti	= 1.4;
	var rychlost_animacie	= 20;
	var tecuci_prud			= 0;
	var zmena_rychlosti_e	= 1;
	var pauza_animacky		= false;
	var tutorial_cast		= 1;
	
	function novy_atom(jadro,hore,dole,pravy,lavy){
		this.jadro = jadro;
		this.hore  = hore;
		this.dole  = dole;
		this.pravy = pravy;
		this.lavy  = lavy;
	}
	
	this.Vytvor_pole_mriezky = function(){	
		this.stlpec_1 = [new novy_atom(5,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1)];
		this.stlpec_2 = [new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1)];
		this.stlpec_3 = [new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1)];
		this.stlpec_4 = [new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1)];
		this.stlpec_5 = [new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1)];
		this.stlpec_6 = [new novy_atom(4,1,1,1,1),new novy_atom(4,1,1,1,1),new novy_atom(3,0,1,1,1)];	
		
		this.pole_atomov = [this.stlpec_1,this.stlpec_2,this.stlpec_3,this.stlpec_4,this.stlpec_5,this.stlpec_6];
	}
	
	this.Create = function(){			
		moj_interval 	 = 0;
		koniec_cyklu	 = 0; 
		pokracuj_v_cykle = 0; 
		spustene 		 = false;
		aktual_atom_x 	 = 5;
		aktual_atom_y 	 = 2;
		
		this.Vytvor_pole_mriezky();
		this.Kresli_plochu();
	}
	
	this.Run = function(){
		spustene = !spustene;
		this.tutorial();
		if (spustene){
			tecuci_prud = 0;
			document.getElementById('button_img_on_off').src = 'images/on_button.png';
			this.prepoctaj_prud_pri_zmene();
			if (smer_priechodu != 1){
				this.spusti_animacku_toku(2,1000);
			}	
		}else{
			document.getElementById('button_img_on_off').src = 'images/off_button.png';
			this.zastav_animacku_toku();
			this.zmen_hodnotu_ampermetra('0','display_ampermetra');
		}	
		vypinac(495,420,spustene);
	}
	
	this.Stop = function(){
		spustene = false;
		this.zastav_animacku_toku();
		vypinac(495,420,spustene);
		document.getElementById('button_img_on_off').src = 'images/off_button.png';
		this.zmen_hodnotu_ampermetra('0','display_ampermetra');
	}
	
	this.prepoctaj_prud_pri_zmene = function(){
		if (aktualny_prvok == 1){
			aktualna_hustota  = -10;
			faktor_idealnosti = 1.4;
		}else{
			aktualna_hustota  = -4;
			faktor_idealnosti = 1;
		}	
		if (spustene){		
			if (smer_priechodu == 1){
				pom_prud = this.vypocitaj_prud(aktualna_hustota,aktualne_napatie,aktualna_teplota,faktor_idealnosti);
				this.zmen_hodnotu_ampermetra(pom_prud,'display_ampermetra');				
				this.zmen_rychlosti_animacii(pom_prud);
				
				if (pom_prud < 0.00001){
					this.zastav_animacku_toku();
					tecuci_prud = 0;
				}else{
					if (tecuci_prud == 0){
						tecuci_prud = 1;
						this.spusti_animacku_toku(1,rychlost_animacie);
					}else{
						this.zastav_animacku_toku();
						this.spusti_animacku_toku(1,rychlost_animacie);
					}	
				}
			}else{
				pom_prud = this.vypocitaj_prud(aktualna_hustota,-aktualne_napatie,aktualna_teplota,faktor_idealnosti);
				this.zmen_hodnotu_ampermetra(pom_prud,'display_ampermetra');
			}	
		}	
	}
	
	this.vypocitaj_prud = function(mocnina_i0,V,T,n){
		Is = 10 * Math.pow(10,mocnina_i0);
		e  = Math.E;
		Vt = (KONSTANTA_K * T);
		Vd = KONSTANTA_Q * V;

		I = Is * (Math.pow(e,(Vd / (n * Vt))) - 1);
		if (aktualne_napatie == 0){
			return "0";
		}else{
			if (I < 10){
				return I.toFixed(5);
			}
			if (I < 100){
				return I.toFixed(4);
			}
			if (I < 1000){
				return I.toFixed(3);
			}
			if (I < 10000){
				return I.toFixed(2);
			}	
			if (I < 100000){
				return I.toFixed(1);
			}
			if (I < 1000000){
				return I.toFixed(0);
			}
			return "< 1 M";
		}
	}
	
	this.zastav_animacku_toku = function(){
		clearInterval(moj_interval);
	}
	
	this.spusti_animacku_toku = function(smer,rychlost){
		var pom_obj = this;
		if (smer == 1){
			moj_interval = setInterval(function(){pom_obj.priepustny_smer()}, rychlost);
		}else{
			moj_interval = setInterval(function(){pom_obj.zaverny_smer()}, rychlost);
		}
	}
	
	this.zmen_rychlosti_animacii = function(I){
		if ((I > 0.000009) && (I < 0.01)){
			zmena_rychlosti_e = 3;
		}else if ((I >= 0.01) && (I < 0.1)){
			zmena_rychlosti_e = 2.5;
		}else if ((I >= 0.1) && (I < 0.5)){
			zmena_rychlosti_e = 2;
		}else if ((I >= 0.5) && (I < 1)){
			zmena_rychlosti_e = 1.5;
		}else if ((I >= 1) && (I < 1.8)){
			zmena_rychlosti_e = 1;
		}else if ((I >= 1.8) && (I < 3)){
			zmena_rychlosti_e = 0.8;		
		}else if ((I >= 3) && (I < 10)){
			zmena_rychlosti_e = 0.5;
		}else if ((I >= 10) && (I < 30)){
			zmena_rychlosti_e = 0.3;
		}else if (I >= 30){
			zmena_rychlosti_e = 0.1;
		}		
	}
	
	this.Zmen_smer = function(){
		smer_priechodu *= -1;
		this.Stop();
		var pom_ob = this;
		setTimeout(function(){pom_ob.Create()}, 1000);
	}
	
	this.zaverny_smer = function(){	
		if (zaverny_smer_krok == 0){
			this.animacka_indium_incoming(5,2);
		}else if(zaverny_smer_krok == 1){
			this.animacka_arzen_leti_domu(0,0);
		}else{
			clearInterval(moj_interval);
			zaverny_smer_krok = -1;
		}
		zaverny_smer_krok++;
	}
		
	this.priepustny_smer = function() {	
		if (!pauza_animacky){
			pauza_animacky = true;
			if (koniec_cyklu == 1){	
				if (pokracuj_v_cykle == 1){
					koniec_cyklu = 0;
					pokracuj_v_cykle = 0;
					this.animacka_arzen_incoming(0,0);
				}else{
					this.animacka_indium_leti_domu(5,2);	
					pokracuj_v_cykle = 1;
				}	
			}else{
				if (((aktual_atom_x == 1) && (aktual_atom_y == 0)) || ((aktual_atom_x == 0) && (aktual_atom_y == 1))){
					if ((this.pole_atomov[1][0].lavy == 0) || (this.pole_atomov[0][1].hore == 0)){
						if (aktual_atom_x == 1){
							this.animacka_volny_do_prava(1,0);
							
							this.pole_atomov[1][0].hore = 1;
							this.pole_atomov[1][0].dole = 1;
							this.pole_atomov[1][0].lavy = 1;
							this.pole_atomov[1][0].pravy = 1;				
						}else{
							this.animacka_volny_dole(0,1);
							
							this.pole_atomov[0][1].hore = 1;
							this.pole_atomov[0][1].dole = 1;
							this.pole_atomov[0][1].lavy = 1;
							this.pole_atomov[0][1].pravy = 1;
						}
						
						this.pole_atomov[5][2].hore = 0;
						this.pole_atomov[5][2].dole = 1;
						this.pole_atomov[5][2].lavy = 1;
						this.pole_atomov[5][2].pravy = 1;
						
						aktual_atom_x = 5;
						aktual_atom_y = 2;
						
						koniec_cyklu = 1;
					}else{				
						ctx.clearRect (122, 122, 27, 32);
						ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
						ctx.fillRect (122, 122, 27, 32);
						elektron(135 + getRandomInt(-2,2),135 + getRandomInt(-2,2));
						this.swap_rovnaky_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);
					}
				}else{	
					ctx.clearRect (122, 122, 27, 32);
					ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
					ctx.fillRect (122, 122, 27, 32);
					elektron(135 + getRandomInt(-2,2),135 + getRandomInt(-2,2));
					
					if ((this.pole_atomov[aktual_atom_x][aktual_atom_y].dole == 0) || (this.pole_atomov[aktual_atom_x][aktual_atom_y].pravy == 0)){
						this.swap_rovnaky_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);
					}else{
						if ((this.pole_atomov[aktual_atom_x][aktual_atom_y].lavy == 0) && (aktual_atom_y == 0)){
							this.swap_susedny_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],this.pole_atomov[aktual_atom_x][aktual_atom_y],this.pole_atomov[aktual_atom_x - 1][aktual_atom_y],aktual_atom_x,aktual_atom_y);
						}else{
							var randomswap = getRandomInt(0,2);
							if (randomswap == 0){
								this.swap_rovnaky_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);
							}else{
								if ((aktual_atom_y == 0) && (this.pole_atomov[aktual_atom_x][aktual_atom_y].hore == 0)){
									this.swap_rovnaky_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);
								}else{
									if (aktual_atom_x == 0){
										if(this.pole_atomov[aktual_atom_x][aktual_atom_y].hore == 0){
											this.swap_susedny_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],this.pole_atomov[aktual_atom_x][aktual_atom_y - 1],this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);			
										}else{
											this.swap_rovnaky_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],aktual_atom_x,aktual_atom_y);
										}
									}else{
										this.swap_susedny_atom(this.pole_atomov[aktual_atom_x][aktual_atom_y],this.pole_atomov[aktual_atom_x][aktual_atom_y - 1],this.pole_atomov[aktual_atom_x - 1][aktual_atom_y],aktual_atom_x,aktual_atom_y);			
									}
								}
							}
						}	
					}
				}	
			}
		}
	}
	
	this.swap_rovnaky_atom = function(atom,x,y){
		if (atom.hore == 0){			
			atom.hore = 1;
			atom.lavy = 0;
			this.animacka_swapu_vlastneho_elektronu_z_lava_hore(x,y);
			return
		}
		if (atom.lavy == 0){
			atom.hore = 0;
			atom.lavy = 1;
			this.animacka_swapu_vlastneho_elektronu_z_hora_do_lava(x,y);
			return
		}
		if (atom.dole == 0){
			atom.lavy = 0;
			atom.dole = 1;
			this.animacka_swapu_vlastneho_elektronu_z_lava_dole(x,y);
			return
		}
		if (atom.pravy == 0){
			var random_vyber = getRandomInt(0,1);	
			if (random_vyber == 1){
				atom.pravy = 1;
				atom.hore = 0;
				this.animacka_swapu_vlastneho_elektronu_z_hora_do_prava(x,y);
			}else{
				atom.pravy = 1;
				atom.dole = 0;
				this.animacka_swapu_vlastneho_elektronu_z_dola_do_prava(x,y);
			}		
		}
	}
	
	this.swap_susedny_atom = function(atom1,atom2,atom3,x,y){
		if (typeof atom2 != "undefined") {
			if (atom1.hore == 0){
				this.animacka_swapu_suseda_dole(x,y);	
				atom1.hore = 1;
				atom2.dole = 0;
				aktual_atom_y--;									
				return
			}
		}
		if (typeof atom3 != "undefined") {
			if (atom1.lavy == 0){ 
				this.animacka_swapu_suseda_dolava(x,y);
				atom1.lavy = 1;
				atom3.pravy = 0;
				aktual_atom_x--;
				return
			}
		}	
	}
	
	this.Kresli_plochu = function(){	
		ctx.lineWidth = 2;
		ctx.clearRect ( 0 , 0 , c.width , c.height );
		
		ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
		ctx.fillRect(40,40,340,340);		
		
		ctx.fillStyle = "rgba(255, 34, 17, 0.3)";
		ctx.fillRect(380,40,340,340);
		
		if (smer_priechodu == 1){
			ctx.fillStyle = "rgba(51, 119, 221, 1)";
			ctx.fillRect(13,40,24,340);
								
			ctx.fillStyle = "rgba(255, 34, 17, 1)";
			ctx.fillRect(723,40,24,340);
		}else{
			ctx.fillStyle = "rgba(255, 34, 17, 1)";
			ctx.fillRect(13,40,24,340);
								
			ctx.fillStyle = "rgba(51, 119, 221, 1)";
			ctx.fillRect(723,40,24,340);
		}	
		
		
		ctx.beginPath();
		ctx.rect(380,40,340,340);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.rect(40,40,340,340);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.moveTo(25,380);
		ctx.lineTo(25,420);
		ctx.lineTo(735,420);
		ctx.lineTo(735,380);
		ctx.stroke();
		
		ampermeter(380,420);
		
		zdroj_jednosmerny(265,420,smer_priechodu);
		
		vypinac(495,420,spustene);
		
		elektron(135,143);
		
		for (i = 0;i < 6;i++){
			for (j = 0;j < 3;j++){			
				if (i < 3){odskok = 0;}else{odskok = 10;}				
				var x_atomu = (i * posun) + start_x + odskok;
				var y_atomu = (j * posun) + start_y;
				kresli_jadro(x_atomu, y_atomu, this.pole_atomov[i][j].jadro);
				kresli_elektrony(x_atomu, y_atomu, this.pole_atomov[i][j]);				
			}	
		}		
	}
	
	this.zmen_hodnotu_ampermetra = function(hodnota,elem){
		document.getElementById(elem).value = hodnota;
	}
	
	this.zmen_teplotu = function(t){
		aktualna_teplota = t;
	}
	
	this.zmen_napatie = function(u){
		aktualne_napatie = u;
	}
	
	this.zmen_prvok = function(e){
		aktualny_prvok = e;
	}

	this.zmen_zobrazovanu_teplotu = function(teplota){
		document.getElementById("aktualna_teplota").innerHTML = teplota+" K / "+kelvin_to_celsius(teplota)+"°C";	
	}

	this.zmen_zobrazovane_napatie = function(u){
		pom = u.toFixed(2);
		document.getElementById("aktualne_napatie").innerHTML = pom+" V";	
	}

	this.zmen_zobrazovane_prvku = function(prvok){
		if (prvok == 2){
			document.getElementById("obrazok_prvku").src = "images/germanium.png";
			document.getElementById("aktualny_prvok").innerHTML = "Germánium";				
		}else{
			document.getElementById("obrazok_prvku").src = "images/kremik.png";	
			document.getElementById("aktualny_prvok").innerHTML = "Kremík";	
		}	
	}
	
	////----------ANIMACIE---------------------------------------------------------------
	
	this.animacka_arzen_leti_domu = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x + 10;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect((sur_elektronu_x) - 97, sur_elektronu_y + 32, 135, 23);	
			ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
			ctx.fillRect((sur_elektronu_x) - 69, sur_elektronu_y + 32, 107, 23);	
			
			ctx.lineWidth = 2;
			ctx.beginPath();
			ctx.moveTo(40,132);
			ctx.lineTo(40,155);
			ctx.stroke();
			
			elektron((sur_elektronu_x + 25) - iii,sur_elektronu_y + 43);
			
			elektron(sur_elektronu_x - 10,sur_elektronu_y + 43);
			
			ctx.fillStyle = "rgba(255, 34, 17, 1)";
			ctx.fillRect(sur_elektronu_x - 97, sur_elektronu_y + 32, 24, 23);	
			iii += 2;					
			if (iii > 110){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 12);	
	}
	
	this.animacka_arzen_incoming = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x + 10;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect((sur_elektronu_x) - 97, sur_elektronu_y + 32, 135, 23);	
			ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
			ctx.fillRect((sur_elektronu_x) - 69, sur_elektronu_y + 32, 107, 23);	
			
			ctx.lineWidth = 2;
			ctx.beginPath();
			ctx.moveTo(40,132);
			ctx.lineTo(40,155);
			ctx.stroke();
			
			elektron((sur_elektronu_x - 84) + iii,sur_elektronu_y + 43);
			
			elektron(sur_elektronu_x - 10,sur_elektronu_y + 43);
			
			ctx.fillStyle = "rgba(51, 119, 221, 1)";
			ctx.fillRect(sur_elektronu_x - 97, sur_elektronu_y + 32, 24, 23);	
			iii += 2;					
			if (iii > 110){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 12 * zmena_rychlosti_e);	
	}
	
	this.animacka_indium_incoming = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x + 10;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect((sur_elektronu_x) - 20, sur_elektronu_y - 55, 107, 23);	
			ctx.fillStyle = "rgba(255, 34, 17, 0.3)";
			ctx.fillRect((sur_elektronu_x) - 20, sur_elektronu_y - 55, 79, 23);	
			
			ctx.lineWidth = 2;			
			ctx.beginPath();
			ctx.moveTo(720,265);
			ctx.lineTo(720,288);
			ctx.stroke();
			
			diera(sur_elektronu_x,sur_elektronu_y - 43);
			elektron((sur_elektronu_x + 76) - iii,sur_elektronu_y - 43);
			
			ctx.fillStyle = "rgba(51, 119, 221, 1)";
			ctx.fillRect(sur_elektronu_x + 63, sur_elektronu_y - 55, 24, 23);	
			iii += 2;					
			if (iii > 76){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 15);	
	}	
	
	this.animacka_indium_leti_domu = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x + 10;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect((sur_elektronu_x) - 20, sur_elektronu_y - 55, 107, 23);	
			ctx.fillStyle = "rgba(255, 34, 17, 0.3)";
			ctx.fillRect((sur_elektronu_x) - 20, sur_elektronu_y - 55, 79, 23);	
			
			ctx.lineWidth = 2;
			ctx.beginPath();
			ctx.moveTo(720,265);
			ctx.lineTo(720,288);
			ctx.stroke();
			
			diera(sur_elektronu_x,sur_elektronu_y - 43);
			elektron(sur_elektronu_x + iii,sur_elektronu_y - 43);
			
			ctx.fillStyle = "rgba(255, 34, 17, 1)";
			ctx.fillRect(sur_elektronu_x + 63, sur_elektronu_y - 55, 24, 23);	
			iii += 2;					
			if (iii > 76){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 15 * zmena_rychlosti_e);	
	}	
	
	
	this.animacka_volny_do_prava = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect((sur_elektronu_x - 110) - 55, sur_elektronu_y - 55, 130, 110);	
			ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
			ctx.fillRect((sur_elektronu_x - 110) - 55, sur_elektronu_y - 55, 130, 110);	
			
			kresli_jadro(sur_elektronu_x - 110, sur_elektronu_y, pom_objekt.pole_atomov[x - 1][y].jadro);
			kresli_elektrony(sur_elektronu_x - 110, sur_elektronu_y, pom_objekt.pole_atomov[x - 1][y]);	
			
			diera(sur_elektronu_x - 43,sur_elektronu_y);
			elektron(132 + iii,135 - iii);
			iii++;					
			if (iii > 35){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 15 * zmena_rychlosti_e);	
	}
	
	this.animacka_volny_dole = function(x,y){		
		var iii = 0;
		var sur_elektronu_x = (x * posun) + start_x;
		var sur_elektronu_y = (y * posun) + start_y;
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect(sur_elektronu_x - 55, (sur_elektronu_y - 110) - 55, 110, 130);	
			ctx.fillStyle = "rgba(51, 119, 221, 0.3)";
			ctx.fillRect(sur_elektronu_x - 55, (sur_elektronu_y - 110) - 55, 110, 130);	
			
			kresli_jadro(sur_elektronu_x, sur_elektronu_y - 110, pom_objekt.pole_atomov[x][y - 1].jadro);
			kresli_elektrony(sur_elektronu_x, sur_elektronu_y - 110, pom_objekt.pole_atomov[x][y - 1]);	
			
			diera(sur_elektronu_x,sur_elektronu_y - 43);
			elektron(132 - iii,135 + iii);
			iii++;					
			if (iii > 32){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 15 * zmena_rychlosti_e);	
	}
	
	this.animacka_swapu_suseda_dole = function(x,y){		
		var iii = 0;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_elektronu_x = (x * posun) + start_x + odskok;
		var sur_elektronu_y = ((y - 1) * posun) + start_y;
			
		moj_interval1 = setInterval(function(){
			ctx.clearRect ( sur_elektronu_x - 15, (sur_elektronu_y + 31) + iii , 30 , 12 );
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
			ctx.fillRect ( sur_elektronu_x - 15, (sur_elektronu_y + 31) + iii , 30 , 12 );
			diera(sur_elektronu_x,sur_elektronu_y + 43);
			elektron(sur_elektronu_x,(sur_elektronu_y + 43) + iii);
			iii++;					
			if (iii > 24){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);	
	}
	
	this.animacka_swapu_suseda_dolava = function(x,y){	
		var iii = 0;
		if (x == 3){
			var sur_elektronu_x = ((x - 1) * posun) + start_x + 10;
			var sur_elektronu_y = (y * posun) + start_y;
			
			moj_interval1 = setInterval(function(){
				ctx.lineWidth = 2;
				ctx.clearRect (sur_elektronu_x + 31, sur_elektronu_y - 15, 40 , 30 );
				ctx.fillStyle = "rgba(51, 119, 221, 0.3)"
				ctx.fillRect (sur_elektronu_x + 31, sur_elektronu_y - 15, 18 , 30 );
				ctx.fillStyle = "rgba(255, 34, 17, 0.3)";
				ctx.fillRect (sur_elektronu_x + 51, sur_elektronu_y - 15, 20 , 30 );
				ctx.beginPath();
				ctx.moveTo(380,sur_elektronu_y - 15);
				ctx.lineTo(380,sur_elektronu_y + 15);
				ctx.lineTo(380,sur_elektronu_y - 15);
				ctx.stroke();
				diera(sur_elektronu_x + 33,sur_elektronu_y);
				diera(sur_elektronu_x + 67,sur_elektronu_y);
				elektron((sur_elektronu_x + 33) + iii,sur_elektronu_y);
				iii++;					
				if (iii > 34){
					clearInterval(moj_interval1);
					pauza_animacky = false;
				}
			}, 17 * zmena_rychlosti_e);	
		
		}else{
			if (x < 3){odskok = 0;}else{odskok = 10;}	
			var sur_elektronu_x = ((x - 1) * posun) + start_x + odskok;
			var sur_elektronu_y = (y * posun) + start_y;
				
			moj_interval1 = setInterval(function(){
				ctx.lineWidth = 2;
				ctx.clearRect ((sur_elektronu_x + 31) + iii, sur_elektronu_y - 15, 12 , 30 );
				if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
				ctx.fillRect ((sur_elektronu_x + 31) + iii, sur_elektronu_y - 15, 12 , 30 );
				diera(sur_elektronu_x + 43,sur_elektronu_y);
				elektron((sur_elektronu_x + 43) + iii,sur_elektronu_y);
				iii++;					
				if (iii > 24){
					clearInterval(moj_interval1);
					pauza_animacky = false;
				}
			}, 20 * zmena_rychlosti_e);	
		}
	}
	
	this.animacka_swapu_vlastneho_elektronu_z_lava_hore = function(x,y){
		var uhol = 182;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_atomu_x = (x * posun) + start_x + odskok;
		var sur_atomu_y = (y * posun) + start_y;
		
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			xx = 43 * Math.cos(uhol * Math.PI / 180);
			yy = 43 * Math.sin(uhol * Math.PI / 180);
			ctx.clearRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);			
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}				
			ctx.fillRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			
			kresli_jadro(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y].jadro);
			kresli_elektrony(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y]);	
				
			diera(sur_atomu_x - 43,sur_atomu_y);
			diera(sur_atomu_x,sur_atomu_y - 43);
			elektron(sur_atomu_x + xx,sur_atomu_y + yy);
			uhol += 4;					
			if (uhol == 274){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);		
	}
	
	this.animacka_swapu_vlastneho_elektronu_z_lava_dole = function(x,y){
		var uhol = 182;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_atomu_x = (x * posun) + start_x + odskok;
		var sur_atomu_y = (y * posun) + start_y;
		
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			xx = 43 * Math.cos(uhol * Math.PI / 180);
			yy = 43 * Math.sin(uhol * Math.PI / 180);
			ctx.clearRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);			
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
			ctx.fillRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			
			kresli_jadro(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y].jadro);
			kresli_elektrony(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y]);	
				
			diera(sur_atomu_x - 43,sur_atomu_y);
			diera(sur_atomu_x,sur_atomu_y + 43);
			elektron(sur_atomu_x + xx,sur_atomu_y + yy);
			uhol -= 4;					
			if (uhol == 86){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);		
	}
	
	this.animacka_swapu_vlastneho_elektronu_z_hora_do_prava = function(x,y){
		var uhol = 272;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_atomu_x = (x * posun) + start_x + odskok;
		var sur_atomu_y = (y * posun) + start_y;
		
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			xx = 43 * Math.cos(uhol * Math.PI / 180);
			yy = 43 * Math.sin(uhol * Math.PI / 180);
			ctx.clearRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
			ctx.fillRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			
			kresli_jadro(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y].jadro);
			kresli_elektrony(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y]);	
				
			diera(sur_atomu_x,sur_atomu_y - 43);
			diera(sur_atomu_x + 43,sur_atomu_y);
			elektron(sur_atomu_x + xx,sur_atomu_y + yy);
			uhol += 4;					
			if (uhol == 364){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);		
	}
	
	this.animacka_swapu_vlastneho_elektronu_z_dola_do_prava = function(x,y){
		var uhol = 92;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_atomu_x = (x * posun) + start_x + odskok;
		var sur_atomu_y = (y * posun) + start_y;
		
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			xx = 43 * Math.cos(uhol * Math.PI / 180);
			yy = 43 * Math.sin(uhol * Math.PI / 180);
			ctx.clearRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
			ctx.fillRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			
			kresli_jadro(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y].jadro);
			kresli_elektrony(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y]);	
				
			diera(sur_atomu_x,sur_atomu_y + 43);
			diera(sur_atomu_x + 43,sur_atomu_y);
			elektron(sur_atomu_x + xx,sur_atomu_y + yy);
			uhol -= 4;					
			if (uhol == -4){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);		
	}
	
	this.animacka_swapu_vlastneho_elektronu_z_hora_do_lava = function(x,y){
		var uhol = 272;
		if (x < 3){odskok = 0;}else{odskok = 10;}	
		var sur_atomu_x = (x * posun) + start_x + odskok;
		var sur_atomu_y = (y * posun) + start_y;
		
		var pom_objekt = this;
			
		moj_interval1 = setInterval(function(){
			xx = 43 * Math.cos(uhol * Math.PI / 180);
			yy = 43 * Math.sin(uhol * Math.PI / 180);
			ctx.clearRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			if (x < 3){ctx.fillStyle = "rgba(51, 119, 221, 0.3)";}else{ctx.fillStyle = "rgba(255, 34, 17, 0.3)";}
			ctx.fillRect(sur_atomu_x - 55, sur_atomu_y - 55, 110, 110);
			
			kresli_jadro(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y].jadro);
			kresli_elektrony(sur_atomu_x, sur_atomu_y, pom_objekt.pole_atomov[x][y]);	
				
			diera(sur_atomu_x,sur_atomu_y - 43);
			diera(sur_atomu_x - 43,sur_atomu_y);
			elektron(sur_atomu_x + xx,sur_atomu_y + yy);
			uhol -= 4;					
			if (uhol == 176){
				clearInterval(moj_interval1);
				pauza_animacky = false;
			}
		}, 20 * zmena_rychlosti_e);		
	}
	
	this.dalsia_uloha = function(cast){
		tutorial_cast = cast; 
		this.tutorial();
	}
	
	this.tutorial = function(){
		var pom_ob = this;
		var texty  = [	
						'Zapnite experiment.',
						'Nastavte vstupné napätie na 0.10 V.',
						'Zmeňte typ materiálu na Germánium (Ge).',
						'Pomocou zmeny teploty docieľte veľkosť prúdu väčšiu ako 1 A.',
						'Obráťte polaritu zdroja a znova zapnite experiment.'
					];	
		if(document.getElementById('tutorial_text_' + tutorial_cast) != null){
			document.getElementById('tutorial_text_1').innerHTML = texty[0];
			document.getElementById('tutorial_text_2').innerHTML = texty[1];
			document.getElementById('tutorial_text_3').innerHTML = texty[2];
			document.getElementById('tutorial_text_4').innerHTML = texty[3];
			document.getElementById('tutorial_text_5').innerHTML = texty[4];
			document.getElementById('tutorial_text_1').style.color = '#dddddd';
			document.getElementById('tutorial_text_2').style.color = '#dddddd';
			document.getElementById('tutorial_text_3').style.color = '#dddddd';
			document.getElementById('tutorial_text_4').style.color = '#dddddd';
			document.getElementById('tutorial_text_5').style.color = '#dddddd';
			switch(tutorial_cast){
				case 1:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					if (spustene){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_1').innerHTML = texty[0] + '   <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_1').addEventListener("click",function() {pom_ob.dalsia_uloha(2)});					
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 2:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					if (aktualne_napatie == 0.1){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_2').innerHTML = texty[1] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_2').addEventListener("click",function() {pom_ob.dalsia_uloha(3)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;	
				case 3:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					if (aktualny_prvok == 2){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_3').innerHTML = texty[2] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_3').addEventListener("click",function() {pom_ob.dalsia_uloha(4)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 4:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					if ((aktualna_teplota >= 155) && (aktualna_teplota <= 165) && (aktualny_prvok == 2) && (aktualne_napatie == 0.1)){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_4').innerHTML = texty[3] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_4').addEventListener("click",function() {pom_ob.dalsia_uloha(5)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 5:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					document.getElementById('tutorial_text_5').style.color = '#000000';
					if ((smer_priechodu != 1) && (spustene)){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_5').innerHTML = texty[4] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_5').addEventListener("click",function() {pom_ob.dalsia_uloha(6)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;	
				case 6:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					document.getElementById('tutorial_text_5').style.color = '#000000';
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_' + tutorial_cast).innerHTML = 'Hotovo.';
					break;
			}
		}
	}
};

