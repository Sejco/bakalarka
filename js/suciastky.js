function zdroj_jednosmerny(x,y,orientacia){
	ctx.strokeStyle = '#000000';
	ctx.lineWidth 	= 2;
	ctx.clearRect(x - 20,y - 15,40,30);
	
	ctx.beginPath();
	ctx.moveTo(x + 20,y);
	ctx.lineTo(x + 5, y);
	ctx.stroke();
	
	ctx.beginPath();
	ctx.moveTo(x - 20,y);
	ctx.lineTo(x - 5, y);
	ctx.stroke();
	
	if (orientacia == 1){
		ctx.beginPath();
		ctx.moveTo(x + 5,y - 15);
		ctx.lineTo(x + 5,y + 15);
		ctx.stroke();
			
		ctx.beginPath();
		ctx.moveTo(x - 5,y - 7);
		ctx.lineTo(x - 5,y + 7);
		ctx.stroke();
	
		ctx.fillStyle = '#000000';
		ctx.font="23px Georgia";
		ctx.fillText("-",x - 16,y - 8);
		ctx.fillText("+",x + 6,y - 8);
	}else{
		ctx.beginPath();
		ctx.moveTo(x + 5,y - 7);
		ctx.lineTo(x + 5,y + 7);
		ctx.stroke();
			
		ctx.beginPath();
		ctx.moveTo(x - 5,y - 15);
		ctx.lineTo(x - 5,y + 15);
		ctx.stroke();
	
		ctx.fillStyle = '#000000';
		ctx.font="23px Georgia";
		ctx.fillText("+",x - 21,y - 8);
		ctx.fillText("-",x + 6,y - 8);	
	}	
}

function vypinac(x,y,stav){
	ctx.lineWidth = 2;
	ctx.clearRect(x - 35,y - 15,70,30);
	
	ctx.beginPath();
	ctx.arc(x + 15,y,4,0,2*Math.PI);
	ctx.moveTo(x + 19,y);
	ctx.lineTo(x + 35,y);
	ctx.stroke();	
	
	ctx.beginPath();
	ctx.arc(x - 15,y,4,0,2*Math.PI);
	ctx.moveTo(x - 19,y);
	ctx.lineTo(x - 35,y);
	ctx.stroke();
	
	if (stav){
		ctx.beginPath();
		ctx.moveTo(x + 11,y);
		ctx.lineTo(x - 11,y);
		ctx.stroke();
	}else{
		ctx.beginPath();
		ctx.moveTo(x + 11,y);
		ctx.lineTo(x - 11,y - 15);
		ctx.stroke();
	}
}

function ampermeter(x,y,farba){
	if (farba === undefined){
		farba = '#ffffff';
	}
	ctx.lineWidth = 2;
	ctx.clearRect(x - 40,y - 25,80,50);
	ctx.beginPath();
	ctx.fillStyle = farba;
	ctx.rect(x - 40,y - 25,80,50);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.lineWidth = 1;
	ctx.arc(x, y + 12, 11, 0, 2 * Math.PI);
	ctx.stroke();
	
	ctx.beginPath();
	ctx.lineWidth = 1;
	ctx.moveTo(x + 12, y + 12);
	ctx.lineTo(x + 21, y + 12);
	ctx.moveTo(x - 12, y + 12);
	ctx.lineTo(x - 21, y + 12);
	ctx.stroke();
	
	ctx.fillStyle = '#000000';
	ctx.font="20px Tahoma, Geneva, sans-serif";
	ctx.fillText("A",x - 6,y + 19);
}

function ohmmeter(x,y,farba){
	if (farba === undefined){
		farba = '#ffffff';
	}
	ctx.lineWidth = 2;
	ctx.clearRect(x - 40,y - 25,80,50);
	ctx.beginPath();
	ctx.fillStyle = farba;
	ctx.rect(x - 40,y - 25,80,50);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.lineWidth = 1;
	ctx.arc(x, y + 12, 11, 0, 2 * Math.PI);
	ctx.stroke();
	
	ctx.beginPath();
	ctx.lineWidth = 1;
	ctx.moveTo(x + 12, y + 12);
	ctx.lineTo(x + 21, y + 12);
	ctx.moveTo(x - 12, y + 12);
	ctx.lineTo(x - 21, y + 12);
	ctx.stroke();
	
	ctx.fillStyle = '#000000';
	ctx.font="20px Tahoma, Geneva, sans-serif";
	ctx.fillText("Ω",x - 7,y + 19);
}

function voltmeter(x,y){
	ctx.lineWidth = 2;
	ctx.clearRect(x - 40,y - 25,80,50);
	ctx.beginPath();
	ctx.rect(x - 40,y - 25,80,50);
	ctx.stroke();
	
	ctx.fillStyle = '#000000';
	ctx.font="20px Tahoma, Geneva, sans-serif";
	ctx.fillText("V",x - 6,y + 19);
}

function rezistor(x,y,o,text){
	if (o == 1){
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.rect(x - 10,y - 35,20,75);
		ctx.stroke();
		
		ctx.fillStyle = '#000000';
		ctx.font="20px Tahoma, Geneva, sans-serif";
		ctx.fillText(text,x - 6,y + 8);
	}else{
		ctx.clearRect(x - 40, y - 15, 80, 30);
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.rect(x - 40,y - 15,80,30);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.fillStyle = "rgba(153, 68, 34, 0.6)";
		ctx.rect(x - 39,y - 14,78,28);
		ctx.fill();
		
		ctx.fillStyle = '#000000';
		ctx.font="20px Tahoma, Geneva, sans-serif";
		ctx.fillText(text,x - 25,y + 7);
	}
}

function uzol(x,y){
	ctx.beginPath();
	ctx.fillStyle = '#000000';
	ctx.arc(x, y, 5, 0, 2 * Math.PI, true);
	ctx.fill();
}

function striedavyZdroj(x,y,orientacia,text,s){
    ctx.clearRect(x - 20,y - 20,40,40);
	ctx.lineWidth = 2;
	ctx.strokeStyle = '#000000';
	ctx.beginPath();
	ctx.arc(x,y - 15,5,0,2*Math.PI);
	ctx.moveTo(x + 5,y + 15);
	ctx.arc(x,y + 15,5,0,2*Math.PI);
	ctx.fillStyle = '#000000';
	ctx.font="20px Helvetica";	
	ctx.fillText(text,x + 10,y + 7);
	ctx.stroke();
	
	if (orientacia == 1){
		ctx.fillText('-',x - 22,y - 10);
		ctx.fillText('+',x - 22,y + 22);
		if (s){
			sipka(x + 28,y - 20,orientacia,'#000000');
			ciara(x + 28,y + 20, 0, -40);
		}	
	}else if(orientacia == 2){
	
	}else if(orientacia == 3){
		ctx.fillText('+',x - 22,y - 8);
		ctx.fillText('-',x - 20,y + 20);
		if (s){
			sipka(x + 28,y + 20,orientacia,'#000000');
			ciara(x + 28,y + 20, 0, -40);
		}
	}else{
		ctx.fillText('-',x - 22,y - 18);
		ctx.fillText('+',x + 14,y - 18);
		if (s){
			sipka(x - 22,y + 26,orientacia,'#000000');
			ciara(x - 22,y + 26, 40, 0);
		}
	}
}

function kzRezistor(x,y,orientacia,text,s){
	if ((orientacia == 1) || (orientacia == 3)){
		ctx.clearRect(x - 15,y - 40,30,80);
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.rect(x - 15,y - 40, 30, 80);
		ctx.fillStyle = '#000000';
		ctx.font="20px Helvetica";	
		ctx.fillText(text,x - 12,y + 7);
		ctx.stroke();
		if (s){
			if (orientacia == 1){
				sipka(x + 28,y - 20,orientacia,'#000000');
				ciara(x + 28,y - 20, 0, 40);
			}else if(orientacia == 3){
				sipka(x - 25,y + 20,orientacia,'#000000');
				ciara(x - 25,y - 20, 0, 40);
			}
		}
	}else{
		ctx.clearRect(x - 40,y - 15,80,30);
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.rect(x - 40,y - 15, 80, 30);
		ctx.fillStyle = '#000000';
		ctx.font="20px Helvetica";	
		ctx.fillText(text,x - 12,y + 7);
		ctx.stroke();
		if (s){
			if (orientacia == 2){
				sipka(x + 20,y + 25,orientacia,'#000000');
				ciara(x - 20,y + 25, 40, 0);
			}else if(orientacia == 4){
				sipka(x - 20,y + 25,orientacia,'#000000');
				ciara(x - 20,y + 25, 40, 0);
			}
		}
	}	
}

function slucka(x,y,text,smer){
	ctx.lineWidth = 2;
	ctx.strokeStyle = '#000000';	
	ctx.beginPath();
	ctx.arc(x,y,25,0,0.5*Math.PI,true);
	ctx.fillStyle = '#FFFFFF';
    ctx.fill();
	ctx.fillStyle = '#000000';
	ctx.font="25px Helvetica";
	ctx.fillText(text,x - 8,y + 8);
	ctx.stroke();
	sipka(x + 24,y + 3,3,'#000000');
}




