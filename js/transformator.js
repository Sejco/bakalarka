﻿function Trafo() {
	var druh_prudu					= 1;
	var spustene					= false;
	var vstupne_napatie				= 5000;	
	var stare_vstupne_napatie		= 0;
	var pocet_zavitov_1 			= 800;
	var pocet_zavitov_2 			= 200;
	var smer_mag_toku 				= -1;
	var zmena_dc					= 0;
	var pom_time					= 0;
	var tutorial_cast				= 1;
	var vystupne_napatie;
	var koeficient_zavitov_1;
	var koeficient_zavitov_2;
	var pocet_ilusracnych_zavitov_1;
	var pocet_ilusracnych_zavitov_2;
	var posun_od_hora_1;
	var posun_od_hora_2;
	var pocet_z_kreslenie_1;
	var pocet_z_kreslenie_2;	
	var moj_interval1;
	var moj_interval2;
	var moj_interval3;
	var moj_interval4;
	var vyska_apmlitudy_1;
	var vyska_apmlitudy_2;

	
	//suradnice na kreslenie
	var suradnice_elipsy			= vrat_pole_sur_elipsy(150,230,15,2);
	var suradnice_kruhov			= [];
	var suradnice_amplitud			= []; 
	var rychlosti_elektronov		= []; 
	var pohyb_elektornov 			= [[100,290],[260,380],[610,280],[520,380]];	
	var pohyb_elektornov_dc 		= [[100,290,true],[150,230,true],[250,230,true],[150,380,false],[250,380,false]]; //true -> je hore, false -> dole	
	var suradnice_dc				= [];
	var suradnice_dc2				= [];


	this.Create = function(){
		this.prepocitaj_konstanty_pre_zavity(pocet_zavitov_1,pocet_zavitov_2);	
		this.generuj_suradnice_kruhov();
		this.generuj_suradnice_aplitud();
		rychlosti_elektronov = this.generuj_rychlosti_elektronov();
		console.log(rychlosti_elektronov);
	}
	
	this.Run = function(){
		spustene = !spustene;	
		this.tutorial();
		this.zmen_cudlik_on_off(spustene);
		if (spustene){
			pohyb_elektornov = [[100,290],[260,380],[610,280],[520,380]];
			smer_mag_toku = -1;
			this.ukaz_vystupne_napatie();	
			this.vypocitaj_vysku_amplitud(vstupne_napatie,vystupne_napatie);			
			this.animacka_main();
			this.vypocitaj_suradnice_dc_grafu(0);
			this.vypocitaj_suradnice_dc_grafu2(0);
		}else{
			clearInterval(moj_interval1);
			clearInterval(moj_interval2);
			clearInterval(moj_interval3);
			clearInterval(moj_interval4);
			this.zmen_hodnotu_voltmetra(0);
		}	
		this.prepocitaj_konstanty_pre_zavity(pocet_zavitov_1,pocet_zavitov_2);	
	}
	
	this.generuj_suradnice_kruhov = function(){
		for (i = 5; i < 15;i++){
			suradnice_kruhov.push(vrat_pole_sur_kruhu(i));
		}	
	}
	
	this.generuj_suradnice_aplitud = function(){
		pom_pole = [];
		for(g = 0; g <= 2000; g++){		// vysky amplitud od 0 po 2000
			for (h = -Math.PI; h <= Math.PI; h += 0.05){	// priebeh sinusovky
				yamp = 100 + Math.sin(h) * g;
				pom_pole.push(yamp);
			}			
			suradnice_amplitud.push(pom_pole);
			pom_pole = [];		
		}
	}
	
	this.generuj_rychlosti_elektronov = function(){
		pom_pole = [];
		for (h = -Math.PI; h <= Math.PI; h += 0.05){	// priebeh sinusovky
			yamp = 61 - (Math.abs(Math.sin(h) * 60));
			pom_pole.push(Math.floor(yamp) + 3);
		}				
		return pom_pole;
	}
	
	this.ukaz_vystupne_napatie = function(){
		if(druh_prudu == 1){
			vystupne_napatie = this.vypocitaj_vystupne_napatie(this.vypocitaj_pomer(pocet_zavitov_1,pocet_zavitov_2));
			this.vypocitaj_vysku_amplitud(vstupne_napatie,vystupne_napatie);
			this.zmen_hodnotu_voltmetra(vystupne_napatie);
		}else{
			this.vypocitajZmenuNapatia();
			this.zmen_hodnotu_voltmetra(0);
		}	
	}
	
	this.zmen_hodnotu_voltmetra = function(hodnota){
		if (spustene){
			if (hodnota == 0){
				document.getElementById('display_voltmeter').value = 0;
			}else if ((hodnota < 11) && (hodnota != 0)){
				document.getElementById('display_voltmeter').value = hodnota.toFixed(1);
			}else{
				document.getElementById('display_voltmeter').value = francuzska_notacia(hodnota.toFixed());
			}	
		}else{
			document.getElementById('display_voltmeter').value = 0;
		}	
	}
	
	this.vypocitajZmenuNapatia = function(){
		var pom = Math.abs(stare_vstupne_napatie - vstupne_napatie);
		stare_vstupne_napatie = vstupne_napatie;
		pom_time = 0;
		if (pom != 0){
			zmena_dc = pom;
		}
	}
	
	this.vypocitaj_suradnice_dc_grafu = function(u1){
		for (i = 0; i < 125; i++){
			suradnice_dc[i]  = 100 - (u1 / 100).toFixed(0) * 0.6;
		}
	}
	
	this.vypocitaj_suradnice_dc_grafu2 = function(u){
		for (i = 0; i < 125; i++){
			suradnice_dc2[i]  = 100 - (u / 100).toFixed(0) * 0.6;
		}
	}
	
	this.vypocitaj_novu_surr_dc = function(u1){
		return 100 - (u1 / 100).toFixed(0) * 0.6;
	}
	
	this.kresli_graf_dc = function(){
		x = 35;
		ctx.beginPath();
		ctx.strokeStyle = "#FF0000";
		for (i = 123; i--;){
			ctx.moveTo(x,suradnice_dc[i]);
			ctx.lineTo(x - 1,suradnice_dc[i + 1]);
			x++;
		}
		ctx.stroke();
	}
	
	this.kresli_graf_dc2 = function(){
		x = 627;
		ctx.beginPath();
		ctx.strokeStyle = "#0055FF";
		for (i = 123; i--;){
			ctx.moveTo(x,suradnice_dc2[i]);
			ctx.lineTo(x - 1,suradnice_dc2[i + 1]);
			x++;
		}
		ctx.stroke();
	}
	
	this.vypocitaj_vysku_amplitud = function(u1,u2){
		vyska_apmlitudy_1 = (u1 / 100).toFixed(0);
		vyska_apmlitudy_2 = (u2 / 100).toFixed(0);
	}
	
	this.vypocitaj_pomer = function(z1,z2){
		return (z2 / z1);
	}
	
	this.vypocitaj_vystupne_napatie = function(pomer){
		return pomer * vstupne_napatie;
	}
	
	this.zmen_vstupne_napatie = function(u){
		vstupne_napatie = u;
	}
	
	this.zmen_zobrazovane_napatie = function(){		
		document.getElementById("aktualne_napatie").innerHTML = francuzska_notacia(vstupne_napatie)+" V";	
	}
	
	this.zmen_cudlik_on_off = function(io){
		if (io){
			document.getElementById('button_img_on_off').src = 'images/on_button.png';
		}else{
			document.getElementById('button_img_on_off').src = 'images/off_button.png';
		}
	}
	
	this.zmen_img_prudu = function(acdc){
		zmena = false;
		if (acdc == 1){
			if (druh_prudu == 2){
				zmena = true;
			}
			druh_prudu = 1;
			document.getElementById('img_druh_prudu').src = 'images/striedavy_prud.png'
			document.getElementById('druh_prudu').innerHTML = 'Striedavý prúd';
		}else{
			if (druh_prudu == 1){
				zmena = true;
			}
			druh_prudu = 2;
			document.getElementById('img_druh_prudu').src = 'images/jednosmerny_prud.png'
			document.getElementById('druh_prudu').innerHTML = 'Jednosmerný prúd';
		}
		if (zmena){
			this.Kresli(pocet_z_kreslenie_1,pocet_z_kreslenie_2,smer_mag_toku);
			this.Run();
			this.Run();
		}		
	}
	
	this.kresli_eplipsu_podla_surr = function(x,y){
		ctx.beginPath();
		ctx.moveTo(suradnice_elipsy[0][0] + x,suradnice_elipsy[0][1] + y);
		ctx.setLineDash([2]);
		for (i = 0;i < suradnice_elipsy.length / 2;i++){
			ctx.lineTo(suradnice_elipsy[i][0] + x,suradnice_elipsy[i][1] + y)
		}
		ctx.stroke();
		ctx.setLineDash([0]);
		ctx.beginPath();
		ctx.moveTo(suradnice_elipsy[suradnice_elipsy.length / 2][0] + x,suradnice_elipsy[suradnice_elipsy.length / 2][1] + y);
		for (i = suradnice_elipsy.length / 2;i < suradnice_elipsy.length;i++){
			ctx.lineTo(suradnice_elipsy[i][0] + x,suradnice_elipsy[i][1] + y)
		}
		ctx.stroke();
	}
	
	this.kresli_telo_trafaku = function(){
		ctx.setLineDash([0]);
		ctx.lineWidth = 2;
		ctx.strokeStyle = '#777777';
		ctx.fillStyle = '#dddddd';
		ctx.beginPath();
		ctx.rect(260,160,260,300);
		ctx.stroke();		
		ctx.fill();
		ctx.beginPath();
		ctx.rect(340,220,100,180);
		ctx.fillStyle = '#ffffff';
		ctx.stroke();		
		ctx.fill();
		ctx.beginPath();
		ctx.stroke();		
	}
	
	this.kresli_obvod_1 = function(){
		ctx.beginPath();	
		ctx.strokeStyle = '#000000';
		ctx.arc(100,290,5,0,2*Math.PI);
		ctx.moveTo(100,285);
		ctx.lineTo(100,230);
		ctx.lineTo(340,230);
		ctx.stroke();		
		ctx.beginPath();
		ctx.arc(100,320,5,0,2*Math.PI);
		ctx.moveTo(100,325);
		ctx.lineTo(100,380);
		ctx.lineTo(260,380);
		ctx.fillStyle = '#000000';
		ctx.font="20px Arial";
		if (spustene){
			if (druh_prudu == 1){
				if (smer_mag_toku == 1){			
					ctx.fillText("+",110,296);
					ctx.fillText("-",112,324);
				}else{			
					ctx.fillText("-",112,293);
					ctx.fillText("+",110,326);
				}
			}else{
				ctx.fillText("-",112,293);
				ctx.fillText("+",110,326);
			}
		}
		ctx.stroke();
	}
	
	this.kresli_obvod_2 = function(){
		ctx.strokeStyle = '#000000';
		ctx.setLineDash([0]);
		ctx.beginPath();
		ctx.moveTo(690,285);
		ctx.lineTo(690,230);
		ctx.lineTo(440,230);
		ctx.stroke();			
		ctx.beginPath();
		ctx.moveTo(690,325);
		ctx.lineTo(690,380);
		ctx.lineTo(520,380);
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(610,230);
		ctx.lineTo(610,380);
		ctx.stroke();			
		
		uzol(610,230);
		uzol(610,380);
		voltmeter(690,300);		
	}
	
	this.kresli_cievku_1 = function(pz1){
		ctx.beginPath();	
		ctx.strokeStyle = '#BBBBBB';
		ctx.setLineDash([2]);
		ctx.moveTo(340,230);		
		for (i = 0;i < pz1; i++){
			pom = koeficient_zavitov_1 * i;
			ctx.lineTo(260,posun_od_hora_1 + 240 + pom);
			ctx.moveTo(340,posun_od_hora_1 + 245 + pom);			
		}
		ctx.lineTo(260,380);
		ctx.stroke();
		
		ctx.strokeStyle = '#000000';
		ctx.setLineDash([0]);
		ctx.beginPath();
		ctx.moveTo(340,230);	
		for (i = 0;i < pz1; i++){
			pom = koeficient_zavitov_1 * i;
			ctx.moveTo(260,posun_od_hora_1 + 240 + pom);
			ctx.lineTo(340,posun_od_hora_1 + 245 + pom);	
		}
		ctx.stroke();	
	}
	
	this.kresli_cievku_2 = function(pz2){
		ctx.beginPath();	
		ctx.strokeStyle = '#BBBBBB';
		ctx.setLineDash([2]);
		ctx.moveTo(440,230);		
		for (i = 0;i < pz2; i++){
			ctx.lineTo(520,posun_od_hora_2 + 240 + koeficient_zavitov_2 * i);
			ctx.moveTo(440,posun_od_hora_2 + 245 + koeficient_zavitov_2 * i);			
		}
		ctx.lineTo(520,380);
		ctx.stroke();
	
		ctx.beginPath();	
		ctx.strokeStyle = '#000000';
		ctx.setLineDash([0]);
		for (i = 0;i < pz2; i++){
			ctx.moveTo(440,posun_od_hora_2 + 245 + koeficient_zavitov_2 * i);
			ctx.lineTo(520,posun_od_hora_2 + 240 + koeficient_zavitov_2 * i);			
		}
		ctx.stroke();
	}
	
	this.kresli_magneticke_silociary_cievky_1 = function(pz1,s){
		for (i = 0;i < pz1; i++){
			pom = koeficient_zavitov_1 * i;				
			if (s == 1){	
				polkruh(260,posun_od_hora_1 + 245 + pom,15 - pz1,2,'#00AACC',0.5 * Math.PI,2 * Math.PI);
				polkruh(340,posun_od_hora_1 + 240 + pom,15 - pz1,2,'#00AACC',1 * Math.PI,0.5 * Math.PI);
				sipka(275 - pz1,posun_od_hora_1 + 256 + pom - pz1,3,'#00AACC');
				sipka(325 + pz1,posun_od_hora_1 + 255 + pom - pz1,3,'#00AACC');
			}else{
				polkruh(260,posun_od_hora_1 + 245 + pom,15 - pz1,2,'#00AACC',0 * Math.PI,1.5 * Math.PI);
				polkruh(340,posun_od_hora_1 + 240 + pom,15 - pz1,2,'#00AACC',1.5 * Math.PI,3 * Math.PI);
				sipka(275 - pz1,posun_od_hora_1 + 242 + pom - pz1,1,'#00AACC');
				sipka(325 + pz1,posun_od_hora_1 + 239 + pom - pz1,1,'#00AACC');
			}
		}	
	}	
	
	this.kresli_magneticke_silociary_cievky_2 = function(pz2,s){
		for (i = 0;i < pz2; i++){
			pom = koeficient_zavitov_2 * i;		
			if (s != 1){	
				polkruh(440,posun_od_hora_2 + 245 + pom,15 - pz2,2,'#00AACC',0.5 * Math.PI,2 * Math.PI);
				polkruh(520,posun_od_hora_2 + 240 + pom,15 - pz2,2,'#00AACC',1 * Math.PI,0.5 * Math.PI);
				sipka(455 - pz2,posun_od_hora_2 + 259 + pom - pz2,3,'#00AACC');
				sipka(505 + pz2,posun_od_hora_2 + 255 + pom - pz2,3,'#00AACC');
			}else{
				polkruh(440,posun_od_hora_2 + 245 + pom,15 - pz2,2,'#00AACC',0 * Math.PI,1.5 * Math.PI);
				polkruh(520,posun_od_hora_2 + 240 + pom,15 - pz2,2,'#00AACC',1.5 * Math.PI,3 * Math.PI);
				sipka(455 - pz2,posun_od_hora_2 + 242 + pom - pz2,1,'#00AACC');
				sipka(505 + pz2,posun_od_hora_2 + 239 + pom - pz2,1,'#00AACC');
			}
		}	
	}
	
	
	this.Kresli = function(pz1,pz2,s){		
		cista_plocha();		
		this.kresli_grafy();
		this.kresli_telo_trafaku();
				
		if (spustene){
			if (druh_prudu == 1){
				this.kresli_magneticke_silociary_cievky_2(pz2,s);
				this.kresli_magneticke_silociary_cievky_1(pz1,s);
				this.kresli_magneticke_silociary_core();
			}else{
				
				if (zmena_dc != 0){
					this.kresli_magneticke_silociary_cievky_2(pz2,s);
					this.kresli_magneticke_silociary_cievky_1(pz1,s);
				}
				this.kresli_magneticke_silociary_core();
			}		
		}	
		
		this.kresli_cievku_1(pz1);
		this.kresli_cievku_2(pz2);	
		this.kresli_obvod_1();
		this.kresli_obvod_2();		
	}
	
	this.zmen_ukazovatel_zavitov_control_panel = function(elem,pz){
		if (pz == 1){
			document.getElementById(elem).innerHTML = pz + " závit";
		}else if ((pz > 1) && (pz < 5)){
			document.getElementById(elem).innerHTML = pz + " závity";
		}else{
			document.getElementById(elem).innerHTML = pz + " závitov";
		}	
	}
	
	this.zmen_pocet_zavitov = function(n1,n2){
		if (!n2){
			pocet_zavitov_1 = n1;		
		}	
		if (!n1){
			pocet_zavitov_2 = n2;
		}
		
		this.prepocitaj_konstanty_pre_zavity(pocet_zavitov_1,pocet_zavitov_2);
		
		this.zmen_ukazovatel_zavitov_control_panel('aktualne_zavity_1',pocet_zavitov_1);
		this.zmen_ukazovatel_zavitov_control_panel('aktualne_zavity_2',pocet_zavitov_2);
	}
	
	this.prepocitaj_konstanty_pre_zavity = function(pz1,pz2){
		pom_1 = pz1 / 100;
		pom_2 = pz2 / 100;
		koeficient_zavitov_1 = 150 / (parseFloat(pom_1.toFixed(0)) + 1);
		koeficient_zavitov_2 = 150 / (parseFloat(pom_2.toFixed(0)) + 1);
		posun_od_hora_1 = (150 / (parseFloat(pom_1.toFixed(0)) + 1) - 15);
		posun_od_hora_2 = 150 / (parseFloat(pom_2.toFixed(0)) + 1) - 15;
		pocet_z_kreslenie_1	= parseFloat(pom_1.toFixed(0));
		pocet_z_kreslenie_2	= parseFloat(pom_2.toFixed(0));
		
		if (!spustene){
			this.Kresli(pocet_z_kreslenie_1,pocet_z_kreslenie_2);	
		}
	}
	
	this.kresli_grafy = function(){
		ctx.setLineDash([0]);
		ctx.strokeStyle = '#000000';
		ctx.lineWidth = 2;
		ctx.beginPath();	
		ctx.moveTo(35,40);
		ctx.lineTo(35,170);	
		ctx.moveTo(35,100);
		ctx.lineTo(160,100);
		ctx.fillStyle = '#000000';
		ctx.font="15px Arial";
		ctx.fillText("+U1",1,50);
		ctx.fillText("-U1",1,170);
		ctx.fillText("t",170,110);
		ctx.stroke();
				
		ctx.beginPath();	
		ctx.moveTo(625,40);
		ctx.lineTo(625,170);	
		ctx.moveTo(625,100);
		ctx.lineTo(750,100);	
		ctx.fillText("+U2",590,50);
		ctx.fillText("-U2",590,170);
		ctx.fillText("t",760,110);
		ctx.stroke();	
		
		sipka(35,172,3,'#000000');		
		sipka(35,38,1,'#000000');		
		sipka(162,100,2,'#000000');	

		sipka(625,172,3,'#000000');		
		sipka(625,38,1,'#000000');		
		sipka(750,100,2,'#000000');		
	}
	
	this.kresli_magneticke_silociary_core = function(){
		ctx.strokeStyle = '#00AACC';
		ctx.setLineDash([2]);
		ctx.lineWidth = 2;
		ctx.beginPath();	
		ctx.arc(305,205,20,1*Math.PI,1.5*Math.PI);
		ctx.arc(475,205,20,1.5*Math.PI,2*Math.PI);
		ctx.arc(475,415,20,2*Math.PI,0.5*Math.PI);
		ctx.arc(305,415,20,0.5*Math.PI,1*Math.PI);
		ctx.lineTo(285,205);		
		ctx.stroke();	
		
		ctx.beginPath();	
		ctx.arc(310,210,15,1*Math.PI,1.5*Math.PI);
		ctx.arc(470,210,15,1.5*Math.PI,2*Math.PI);
		ctx.arc(470,410,15,2*Math.PI,0.5*Math.PI);
		ctx.arc(310,410,15,0.5*Math.PI,1*Math.PI);
		ctx.lineTo(295,210);		
		ctx.stroke();	
		
		ctx.beginPath();	
		ctx.arc(315,215,10,1*Math.PI,1.5*Math.PI);
		ctx.arc(465,215,10,1.5*Math.PI,2*Math.PI);
		ctx.arc(465,405,10,2*Math.PI,0.5*Math.PI);
		ctx.arc(315,405,10,0.5*Math.PI,1*Math.PI);
		ctx.lineTo(305,215);	
		ctx.stroke();
		
		ctx.beginPath();	
		ctx.arc(320,220,5,1*Math.PI,1.5*Math.PI);
		ctx.arc(460,220,5,1.5*Math.PI,2*Math.PI);
		ctx.arc(460,400,5,2*Math.PI,0.5*Math.PI);
		ctx.arc(320,400,5,0.5*Math.PI,1*Math.PI);
		ctx.lineTo(315,220);
		ctx.stroke();
	}
	
	this.animacka_main = function(){
		var surr_x_pola 			= 149;
		var surr_y_pola 			= 149;
		var ii 						= 0;
		var pom_ob 					= this
		var posun 					= 126;
		var posun2					= 63;
		var slow 					= 2;
		var surr_sipok_x			= [	465,475,485,495,
										465,475,485,495,
										465,475,485,495,
										465,475,485,495,									
										
										330,350,330,350,
										370,390,370,390,
										410,430,410,430,
										
										285,295,305,315,
										285,295,305,315,
										285,295,305,315,
										285,295,305,315,
										
										430,450,430,450,
										390,410,390,410,
										350,370,350,370];	
										
		var surr_sipok_y			= [	380,360,380,360,
										340,320,340,320,
										300,280,300,280,
										260,240,260,240,
										
										405,415,425,435,
										405,415,425,435,
										405,415,425,435,
										
										370,350,370,350,
										330,310,330,310,
										290,270,290,270,
										250,230,250,230,
										
										215,205,195,185,
										215,205,195,185,
										215,205,195,185];
			
		moj_interval3 = setTimeout(pohyb_elektronov_animacka,  rychlosti_elektronov[posun - 1] * 2);
		
		function pohyb_elektronov_animacka(){
			if (druh_prudu == 1){ // AC
				if (smer_mag_toku == 1){ //elektrony 1 - 4
					if (pohyb_elektornov[0][0] > 100){pohyb_elektornov[0][0] -= 1;  }else{pohyb_elektornov[0][1] += 1;	}				
					if (pohyb_elektornov[1][1] < 380){pohyb_elektornov[1][1] += 1;  }else{pohyb_elektornov[1][0] += 1;	}				
					if (pohyb_elektornov[2][0] < 610){pohyb_elektornov[2][0] += 1;  }else{pohyb_elektornov[2][1] += 1;	}				
					if (pohyb_elektornov[3][1] < 380){pohyb_elektornov[3][1] += 1;  }else{pohyb_elektornov[3][0] -= 1;	}
				}else{ 
					if (pohyb_elektornov[0][1] > 230){pohyb_elektornov[0][1] -= 1;  }else{pohyb_elektornov[0][0] += 1;	}
					if (pohyb_elektornov[1][0] > 100){pohyb_elektornov[1][0] -= 1;  }else{pohyb_elektornov[1][1] -= 1;	}					
					if (pohyb_elektornov[2][1] > 230){pohyb_elektornov[2][1] -= 1;  }else{pohyb_elektornov[2][0] -= 1;	}
					if (pohyb_elektornov[3][0] < 610){pohyb_elektornov[3][0] += 1;  }else{pohyb_elektornov[3][1] -= 1;	}				
				}		
				moj_interval3 = setTimeout(pohyb_elektronov_animacka, rychlosti_elektronov[posun - 1] * 2);		
			}else{
				for (f = 0; f < 5; f++){
					if (pohyb_elektornov_dc[f][2]){
						if (pohyb_elektornov_dc[f][1] > 230){pohyb_elektornov_dc[f][1] -= 2;  }else{pohyb_elektornov_dc[f][0] += 2;	}
						if (pohyb_elektornov_dc[f][0] == 340){
							pohyb_elektornov_dc[f][0] = 260;
							pohyb_elektornov_dc[f][1] = 380;
							pohyb_elektornov_dc[f][2] = false;
						
						}	
					}else{
						if (pohyb_elektornov_dc[f][0] > 100){pohyb_elektornov_dc[f][0] -= 2;  }else{pohyb_elektornov_dc[f][1] -= 2;	}	
						if (pohyb_elektornov_dc[f][1] == 320){
							
							
							pohyb_elektornov_dc[f][0] = 100;
							pohyb_elektornov_dc[f][1] = 290;
							pohyb_elektornov_dc[f][2] = true;
					
						}
					}
				}	
					
				moj_interval3 = setTimeout(pohyb_elektronov_animacka, 40);	
			}
		}
		
		moj_interval2 = setInterval(function(){			//priebeh sinusoviek
			if (druh_prudu == 1){		
				posun--;
				posun2--;
			}else{			
				pom_time++;
				suradnice_dc.push(pom_ob.vypocitaj_novu_surr_dc(vstupne_napatie));
				suradnice_dc.shift();
				
				suradnice_dc2.push(pom_ob.vypocitaj_novu_surr_dc(zmena_dc * pom_ob.vypocitaj_pomer(pocet_zavitov_1,pocet_zavitov_2)));
				suradnice_dc2.shift();
				pom_ob.zmen_hodnotu_voltmetra(zmena_dc * pom_ob.vypocitaj_pomer(pocet_zavitov_1,pocet_zavitov_2));
				if (pom_time == 10){
					zmena_dc = 0;
				}
			}
		},36 * slow);
		
		moj_interval1 = setInterval(function(){				//magneticke castice, sinusoidy
			pom_ob.Kresli(pocet_z_kreslenie_1,pocet_z_kreslenie_2,smer_mag_toku);	

			if (druh_prudu == 1){	
				if (posun2 == 0){posun2 = 126;}
				
				sinus(35,100,vyska_apmlitudy_1,'#FF0000',suradnice_amplitud,posun);				
				sinus(625,100,vyska_apmlitudy_2,'#0055FF',suradnice_amplitud,posun2);		

				if (smer_mag_toku == 1){
					for (i = 0;i < 16;i++) {	sipka(surr_sipok_x[i],surr_sipok_y[i],1,'#00AACC');		}			
					for (i = 16;i < 28;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],2,'#00AACC');		}
					for (i = 28;i < 44;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],3,'#00AACC');		}
					for (i = 44;i < 57;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],4,'#00AACC');		}
					
					for (i = 0;i < 16;i++){
						surr_sipok_y[i]--;
						if (surr_sipok_y[i] < 220){
							surr_sipok_y[i] = 380;
						}
					}					
					for (i = 16;i < 28;i++){
						surr_sipok_x[i]++;
						if (surr_sipok_x[i] > 450){
							surr_sipok_x[i] = 330;
						}
					}					
					for (i = 28;i < 44;i++){
						surr_sipok_y[i]++;
						if (surr_sipok_y[i] > 390){
							surr_sipok_y[i] = 230;
						}
					}					
					for (i = 44;i < 57;i++){
						surr_sipok_x[i]--;
						if (surr_sipok_x[i] < 330){
							surr_sipok_x[i] = 450;
						}
					}
					
				}else{
					for (i = 0;i < 16;i++) {	sipka(surr_sipok_x[i],surr_sipok_y[i] + 5,3,'#00AACC');	}			
					for (i = 16;i < 28;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],4,'#00AACC');		}
					for (i = 28;i < 44;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],1,'#00AACC');		}
					for (i = 44;i < 57;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],2,'#00AACC');		}
					
					for (i = 0;i < 16;i++){
						surr_sipok_y[i]++;
						if (surr_sipok_y[i] > 385){
							surr_sipok_y[i] = 225;
						}
					}
					for (i = 16;i < 28;i++){
						surr_sipok_x[i]--;
						if (surr_sipok_x[i] < 330){
							surr_sipok_x[i] = 450;
						}
					}					
					for (i = 28;i < 44;i++){
						surr_sipok_y[i]--;
						if (surr_sipok_y[i] < 230){
							surr_sipok_y[i] = 390;
						}
					}					
					for (i = 44;i < 57;i++){
						surr_sipok_x[i]++;
						if (surr_sipok_x[i] > 450){
							surr_sipok_x[i] = 330;
						}
					}
				}
				
				elektron(pohyb_elektornov[0][0],pohyb_elektornov[0][1]);	
				elektron(pohyb_elektornov[1][0],pohyb_elektornov[1][1]);	
				elektron(pohyb_elektornov[2][0],pohyb_elektornov[2][1]);	
				elektron(pohyb_elektornov[3][0],pohyb_elektornov[3][1]);
				
				if (smer_mag_toku == 1){
					surr_x_pola++;
					surr_y_pola++;
				}else{
					surr_x_pola--;
					surr_y_pola--;
				}	
				
				if(surr_x_pola == 0){
					smer_mag_toku *= -1;
				}
				
				if(surr_x_pola == 149){ //koniec cyklu - reset
					smer_mag_toku 	   *= -1;
					ii 					= 0;
					pohyb_elektornov 	= [[100,290],[260,380],[610,280],[520,380]];
					posun 				= 126;
					posun2 				= 63;
				}
			}else{
				pom_ob.kresli_graf_dc();
				pom_ob.kresli_graf_dc2();
				
				for (f = 0; f < 5; f++){
					elektron(pohyb_elektornov_dc[f][0],pohyb_elektornov_dc[f][1]);
				}
				
				if (zmena_dc > 0){
					for (i = 0;i < 16;i++) {	sipka(surr_sipok_x[i],surr_sipok_y[i] + 5,3,'#00AACC');	}			
					for (i = 16;i < 28;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],4,'#00AACC');		}
					for (i = 28;i < 44;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],1,'#00AACC');		}
					for (i = 44;i < 57;i++){	sipka(surr_sipok_x[i],surr_sipok_y[i],2,'#00AACC');		}
					
					for (i = 0;i < 16;i++){
						surr_sipok_y[i]++;
						if (surr_sipok_y[i] > 385){
							surr_sipok_y[i] = 225;
						}
					}
					for (i = 16;i < 28;i++){
						surr_sipok_x[i]--;
						if (surr_sipok_x[i] < 330){
							surr_sipok_x[i] = 450;
						}
					}					
					for (i = 28;i < 44;i++){
						surr_sipok_y[i]--;
						if (surr_sipok_y[i] < 230){
							surr_sipok_y[i] = 390;
						}
					}					
					for (i = 44;i < 57;i++){
						surr_sipok_x[i]++;
						if (surr_sipok_x[i] > 450){
							surr_sipok_x[i] = 330;
						}
					}
				}
			}
		}, 15 * slow);			
	}
	
	this.dalsia_uloha = function(cast){
		tutorial_cast = cast; 
		this.tutorial();
	}
	
	this.tutorial = function(){
		var pom_ob = this;
		var texty  = [	
						'Zapnite experiment.',
						'Nastavte vstupné napätie na 2 000 V.',
						'Nastavte počty závitov cievok tak, aby výstupné napätie bolo 8 000 V.',
						'Nastavte počty závitov cievok tak, aby výstupné napätie bolo 200 V.'
					];	
		if(document.getElementById('tutorial_text_' + tutorial_cast) != null){
			document.getElementById('tutorial_text_1').innerHTML = texty[0];
			document.getElementById('tutorial_text_2').innerHTML = texty[1];
			document.getElementById('tutorial_text_3').innerHTML = texty[2];
			document.getElementById('tutorial_text_4').innerHTML = texty[3];
			document.getElementById('tutorial_text_1').style.color = '#dddddd';
			document.getElementById('tutorial_text_2').style.color = '#dddddd';
			document.getElementById('tutorial_text_3').style.color = '#dddddd';
			document.getElementById('tutorial_text_4').style.color = '#dddddd';
			switch(tutorial_cast){
				case 1:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					if (spustene){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_1').innerHTML = texty[0] + '   <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_1').addEventListener("click",function() {pom_ob.dalsia_uloha(2)});					
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 2:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					if (vstupne_napatie == 2000){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_2').innerHTML = texty[1] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_2').addEventListener("click",function() {pom_ob.dalsia_uloha(3)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;	
				case 3:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					if (vystupne_napatie == 8000){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_3').innerHTML = texty[2] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_3').addEventListener("click",function() {pom_ob.dalsia_uloha(4)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 4:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					if (vystupne_napatie == 200){
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
						document.getElementById('tutorial_text_4').innerHTML = texty[3] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
						document.getElementById('tutorial_text_4').addEventListener("click",function() {pom_ob.dalsia_uloha(5)});
					}else{
						document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
					}
					break;
				case 5:
					document.getElementById('tutorial_text_1').style.color = '#000000';
					document.getElementById('tutorial_text_2').style.color = '#000000';
					document.getElementById('tutorial_text_3').style.color = '#000000';
					document.getElementById('tutorial_text_4').style.color = '#000000';
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_' + tutorial_cast).innerHTML = 'Hotovo.';
					break;
			}
		}
	}
};