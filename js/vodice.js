function Vodice_a_izolanty(){
	var spustene 				= false; 
	var napatie 				= 500;
	var teplota 				= 293;
	var material				= 'cu';	
	var dlzka_vodica 			= 100;
	var priemer_vodica 			= 0.00001;
	var deafult_resitivity		= 100;
	var rychlost_kmitu 			= 3;
	var tutorial_cast			= 1;
	var aktualny_prud			= 0;
	var elektrony_nevodicov   	= [];
	var rychlost_nevodicov 		= 20;
	var volne_elektrony_metal 	= [];//xs,ys, krokx, kroky, ttl
	var volne_elektrony_grafit  = [];    //x, y, krok_x, krok_y, ttl, poza atom
	var pozicie_x_atomov_grafit	= [[120,330,420,630],[180,270,480,570],[120,330,420,630],[180,270,480,570]];
	var pozicie_y_atomov_grafit	= [90,170,250,330];
	var pozicie_x_atomov_metal	= [140,255,140,375,495,375,615,615];
	var pozicie_y_atomov_metal	= [120,210,300,120,210,300,120,300];
	var odpor;
	var pohyb_atomov_metal;
	var pohyb_elektronov_metal;
	var pohyb_elektronov_grafit;
	var pohyb_elektronov_nevodice;
	var rychlost_animacie;
	var prud_tecie;
	var kmit_atomu;
	var xx_kmitu_atomu = [0,0,0,0,0,0,0,0];
	var yy_kmitu_atomu = [0,0,0,0,0,0,0,0];


	
	var materialy	= {			
		ag 	:  {nazov : 'Striebro', farba : 'rgba(204,204,204,0.9)' , bcg : 'rgba(204,204,204,0.4)', img : 'images/ag.jpg' , ro : 0.0000000159, alpha :  0.0038	 },	
		cu 	:  {nazov : 'Meď', 		farba : 'rgba(204,102,0,0.9)'	, bcg : 'rgba(204,102,0,0.4)'  , img : 'images/cu.png' , ro : 0.0000000168, alpha :  0.00386 },	
		c 	:  {nazov : 'Grafit', 	farba : 'rgba(70,70,70,0.6)'	, bcg : 'rgba(70,70,70,0.4)'   , img : 'images/c.jpg'  , ro : 0.00020, 	    alpha : -0.0005	 },	
		au 	:  {nazov : 'Zlato', 	farba : 'rgba(255,221,34,0.9)'	, bcg : 'rgba(255,221,34,0.4)' , img : 'images/au.jpg' , ro : 0.0000000224, alpha :  0.003715},	
		di  :  {nazov : 'Diamant', 	farba : 'rgba(70,70,70,0.9)'	, bcg : 'rgba(70,70,70,0.4)'   , img : 'images/di.jpg' , ro : 100000000000 , alpha :  0		 },	
		si  :  {nazov : 'Kremík', 	farba : 'rgba(153,153,153,0.9)'	, bcg : 'rgba(153,153,153,0.4)', img : 'images/si.jpg' , ro : 640		  , alpha :  0.00159 },	
	}

	this.Create = function(){
		this.Init_grafit_elektrony();
		this.vypocitaj_kmit_atomu();
		this.Init_elektronov();
		this.Kresli();	
	}
	
	this.Run = function(){
		spustene = !spustene;
		this.zmenOnOffCudlik();
		this.zmen_hodnotu_ohmmetra();
		this.zmen_hodnotu_ampermetra();
		this.Kresli();
		this.animacka_main();
	}

	
	this.Init_grafit_elektrony = function(){
		volne_elektrony_grafit = [];
		for (i = 0;i < 4; i++){
			pom_pole_x = [];
			for (j = 0; j < 4; j++){
				pom_pole_x.push([pozicie_x_atomov_grafit[i][j] + getRandomInt(-10,10), pozicie_y_atomov_grafit[i] + getRandomInt(-10,10), 0, 0, 0, getRandomInt(0,1)]);
			}
			volne_elektrony_grafit.push(pom_pole_x);   
		}
	}
	
	this.Init_elektronov = function(){
		volne_elektrony_metal = [];
		for (i = 0; i < pozicie_x_atomov_metal.length; i++){
			pom = getRandomInt(0,1);
			if (pom == 1){
				volne_elektrony_metal.push([pozicie_x_atomov_metal[i] + getRandomInt(-60,60), pozicie_y_atomov_metal[i] - 50 + getRandomInt(-10,10),0,0,0]);
			}else{
				volne_elektrony_metal.push([pozicie_x_atomov_metal[i] + getRandomInt(-60,60), pozicie_y_atomov_metal[i] + 50 + getRandomInt(-10,10),0,0,0]);
			}
		}
	}
	
	this.Kresli = function(){
		cista_plocha();
		this.Kresli_obvod();
		
		if ((material == 'cu') || (material == 'ag') || (material == 'au')){
			this.Kresli_metalovu_mriezku();
			if (!spustene){
				this.Kresli_vsetky_elektrony_metal();
			}
		}else if(material == 'si'){
			this.Kresli_kremikovu_mriezku();
		}else if(material == 'di'){
			this.Kresli_diamantovu_strukturu();
		}else if(material == 'c'){
			if (!spustene){
				this.Kresli_elektorny_grafit(1);
				this.Kresli_strukturu_grafitu();		
				this.Kresli_elektorny_grafit(0);
			}
		}
	}
	
	this.Kresli_vsetky_elektrony_metal = function(){
		for (i = 0; i < volne_elektrony_metal.length; i++){	
			elektron(volne_elektrony_metal[i][0],volne_elektrony_metal[i][1]);
		}
	}

	this.Kresli_obvod = function(){		
		ctx.beginPath();
		ctx.fillStyle = materialy[material].bcg;
		ctx.rect(65,40,630,340);
		ctx.fill();
	
		ctx.beginPath();
		ctx.lineWidth = 3;
		ctx.rect(65,41,630,338);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.lineWidth = 2;
		ctx.moveTo(65,200);
		ctx.lineTo(25,200);
		ctx.lineTo(25,420);
		ctx.lineTo(735,420);
		ctx.lineTo(735,200);
		ctx.lineTo(695,200);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.fillStyle = '#000000';
		ctx.rect(41,39,25,342);
		ctx.fill();
		
		ctx.beginPath();
		ctx.fillStyle = '#000000';
		ctx.rect(694,39,25,342);
		ctx.fill();
		
		zdroj_jednosmerny(130,420,0);
		
		rezistor(250,420,0,'100 Ω');
			
		vypinac(380,420,spustene);	
		
		ampermeter(560,420);	
	}
	
	this.Kresli_metalovu_mriezku = function(){
		for (i = 0; i < pozicie_x_atomov_metal.length; i++){
			atom_vodice(pozicie_x_atomov_metal[i] + xx_kmitu_atomu[i], pozicie_y_atomov_metal[i] + yy_kmitu_atomu[i], materialy[material].farba,20);
		}
	}
	
	this.Kresli_kremikovu_mriezku = function(){		
		//kreslenie elektronov
		x = 200;
		y = 85;
		for (j = 0; j < 3; j++){
			for (i = 0; i < 4; i++){
				ciara(x - 35,y,70,0);
				ciara(x - 35,y + 30,70,0);
				elektron(x,y);
				elektron(x,y + 30);
				x += 120;	
			}
			y += 110; 
			x = 200;
		}

		x = 125;
		y = 155;
		for (j = 0; j < 2; j++){
			for (i = 0; i < 5; i++){
				ciara(x,y - 30,0,60);
				ciara(x + 30,y - 30,0,60);
				elektron(x,y);
				elektron(x + 30,y);
				x += 120;	
			}
			y += 110; 
			x = 125;
		}

		//krelenie atomov
		x = 140;
		y = 100;
		for (j = 0; j < 3; j++){
			for (i = 0; i < 5; i++){
				atom_vodice(x,y,materialy[material].farba,30);
				x += 120;
			}
			y += 110;
			x = 140;
		}		
	}
	
	this.Kresli_diamantovu_strukturu = function(){
		//kreslenie elektronov
		x = 140;
		y = 130;
		for (j = 0; j < 2; j++){
			for (i = 0; i < 4; i++){
				ciara(x - 11, y - 11,31,31);
				ciara(x + 5, y - 25,32,32);
				elektron(x,y);
				elektron(x + 25,y - 5);
				x += 130;	
			}
			y += 150; 
			x = 140;
		}
		
		x = 205;
		y = 130;
		for (j = 0; j < 2; j++){
			for (i = 0; i < 4; i++){
				ciara(x - 12, y + 7 ,32, - 32);
				ciara(x + 3 , y + 17,31, - 31);
				elektron(x + 10,y - 15);
				elektron(x + 13,y + 8);
				x += 130;	
			}
			y += 150; 
			x = 205;
		}
		
		x = 140;
		y = 205;
		for (i = 0; i < 4; i++){
			ciara(x - 12, y + 7 ,32, - 32);
			ciara(x + 3 , y + 17,31, - 31);
			elektron(x + 10,y - 15);
			elektron(x + 13,y + 8);
			x += 130;	
		}
		
		x = 205;
		y = 205;
		for (i = 0; i < 4; i++){
			ciara(x - 11, y - 11,31,31);
			ciara(x + 5, y - 25,32,32);
			elektron(x,y);
			elektron(x + 25,y - 5);
			x += 130;	
		}
	
		// kreslenie atomov
		x = 120;
		y = 90;		
		for (j = 0; j < 2; j++){
			for (i = 0; i < 5; i++){
				atom_vodice(x,y,materialy[material].farba,30);
				x += 130;
			}
			y += 150;
			x = 120;
		}
		
		x = 185;
		y = 165;		
		for (j = 0; j < 2; j++){
			for (i = 0; i < 4; i++){
				atom_vodice(x,y,materialy[material].farba,30);
				x += 130;
			}
			y += 150;
			x = 185;
		}
	}
	
	this.Kresli_strukturu_grafitu = function(){
		for (i = 0; i < 4; i++){
			for (j = 0; j < 4; j++){
				atom_vodice(pozicie_x_atomov_grafit[i][j], pozicie_y_atomov_grafit[i], materialy[material].farba, 30);
			}
		}	
		ciara(126,221,28,-39);
		ciara(142,231,25,-36);
		elektron(150,220,true);
		elektron(145,195,true);

		ciara(125,118,29,39);
		ciara(143,110,24,34);
		elektron(150,120,true);
		elektron(145,145,true);
		
		ciara(205,155,40,0);
		ciara(205,185,40,0);
		elektron(225,155,true);
		elektron(225,185,true);

		ciara(281,302,25,-36);
		ciara(296,316,26,-37);
		elektron(296,280,true);
		elektron(307,302,true);

		ciara(205,345,40,0);
		ciara(205,315,40,0);
		elektron(225,345,true);
		elektron(225,315,true);

		ciara(280,198,26,36);
		ciara(295,185,26,36);
		elektron(305,195,true);
		elektron(295,220,true);
	
		ciara(355,235,40,0);
		ciara(355,265,40,0);
		elektron(375,235,true);
		elektron(375,265,true);

		ciara(281,142,25,-36);
		ciara(296,156,26,-37);
		elektron(296,120,true);
		elektron(307,142,true);
	
		ciara(505,345,40,0);
		ciara(505,315,40,0);
		elektron(525,345,true);
		elektron(525,315,true);
	
		ciara(430,278,26,36);
		ciara(445,265,26,36);
		elektron(455,275,true);
		elektron(445,300,true);
	
		ciara(505,155,40,0);
		ciara(505,185,40,0);
		elektron(525,155,true);
		elektron(525,185,true);
	
		ciara(430,118,26,36);
		ciara(445,105,26,36);
		elektron(455,115,true);
		elektron(445,140,true);
	
		ciara(580,198,26,36);
		ciara(595,185,26,36);
		elektron(605,195,true);
		elektron(595,220,true);
	
		ciara(355,75,40,0);
		ciara(355,105,40,0);
		elektron(375,75,true);
		elektron(375,105,true);

		ciara(125,278,29,39);
		ciara(143,270,24,34);
		elektron(150,280,true);
		elektron(145,305,true);
		
		ciara(431,222,25,-36);
		ciara(446,236,26,-37);
		elektron(446,200,true);
		elektron(457,222,true);
		
		ciara(581,302,25,-36);
		ciara(596,316,26,-37);
		elektron(596,280,true);
		elektron(607,302,true);
		
		ciara(581,142,25,-36);
		ciara(596,156,26,-37);
		elektron(596,120,true);
		elektron(607,142,true);
	}
	
	this.zmen_teplotu = function(t){
		teplota = t;
		if (material == 'c'){
			rychlost_kmitu = Math.ceil(t / 50);
		}else{
			if ((material != 'di') && (material != 'si')){
				this.vypocitaj_kmit_atomu();
			}
		}
	}
	
	this.zmen_napatie = function(u){
		napatie = u;
	}
	
	this.vypocitaj_deltu_t = function(){
		return kelvin_to_celsius(teplota) - 20;
	}
	
	this.vypocitaj_kmit_atomu = function(){
		kmit_atomu = ((95 + teplota) / 200).toFixed();
	}
	
	this.vypocitaj_ro = function(){
		delta_t = this.vypocitaj_deltu_t();
		alpha	= materialy[material].alpha;
		ro0		= materialy[material].ro;
		
		return ro0 * (1 + (alpha * delta_t));
	}
	
	this.vypocitaj_odpor = function(){
		ro 		 = this.vypocitaj_ro();
		vysledok = (ro * (dlzka_vodica / priemer_vodica));
		
		return (vysledok >= 0 ? (vysledok + deafult_resitivity) : (deafult_resitivity + 0.00000002));
	}
	
	this.vypocitaj_prud = function(){
		aktualny_prud = (napatie / odpor).toFixed(4);
		return aktualny_prud;
	}
	
	this.zmen_material = function(){
		material = document.getElementById('id_materialu').value;
		this.Init_grafit_elektrony(); 
		this.Reset_elektronov();
		this.Kresli();
	}
	
	this.zmen_zobrazovanu_teplotu = function(teplota){
		document.getElementById("aktualna_teplota").innerHTML = teplota+" K / "+kelvin_to_celsius(teplota)+"°C";	
	}

	this.zmen_zobrazovane_napatie = function(u){
		document.getElementById("aktualne_napatie").innerHTML = u + " V";	
	}
	
	this.zmen_zobrazovany_material = function(){
		if (spustene){
			this.Run();
		}
		document.getElementById('obrazok_materialu').src = materialy[document.getElementById('id_materialu').value].img;
	}
	
	this.zmenOnOffCudlik = function(){
		if (spustene){
			document.getElementById('button_img_on_off').src = 'images/on_button.png';
		}else{
			document.getElementById('button_img_on_off').src = 'images/off_button.png';
		}
	}
	
	this.zmen_hodnotu_ohmmetra = function(){
		if (spustene){
			odpor = this.vypocitaj_odpor();
			document.getElementById('display_ohmmetra').value = odpor.toFixed(5) + ' Ω';
		}else{
			document.getElementById('display_ohmmetra').value = 0 + ' Ω';
		}
	}
	
	this.zmen_hodnotu_ampermetra = function(){
		if (spustene){
			var prud = this.vypocitaj_prud();
			document.getElementById('display_ampermetra').value = prud;
			this.vypocitaj_rychlost_animacie(prud);
		}else{
			document.getElementById('display_ampermetra').value = 0;
		}
	}
		
	this.vrat_index_najdenej_hodnoty_z_pola = function(pole1, pole2, hodnota1, hodnota2){
		for (t = 0; t < pole1.length;t++){
			if ((pole1[t] == hodnota1 - 120) && (pole2[t] == hodnota2)){
				return t;
			}
		}
		return false;
	}
	
	this.animacka_vodivosti_metalov = function(index_elektronu,uhol){
			
	}
	
	this.animacka_main = function(){
		var _this = this;
		clearInterval(pohyb_elektronov_metal);
		clearInterval(pohyb_elektronov_grafit);
		clearInterval(pohyb_elektronov_nevodice);
		if (spustene){
			if ((material == 'cu') || (material == 'ag') || (material == 'au')){
				this.generuj_suradnice_kmitu_atomov();
				pohyb_elektronov_metal = setTimeout(function() {_this.animacka_metalov.call(_this);},  rychlost_animacie);
			}else if (material == 'c'){
				pohyb_elektronov_grafit = setTimeout(function() {_this.animacka_grafitu.call(_this);}, 30);
			}else{
				this.init_el_nevodice();
				pohyb_elektronov_nevodice = setTimeout(function() {_this.animacka_nevodicov.call(_this);}, 20);
			}
		}
	}
	
	this.vypocitaj_rychlost_animacie = function(prud){
		if ((material == 'cu') || (material == 'ag') || (material == 'au')){
			rychlost_animacie = 15 - Math.ceil(prud);
		}
		if (material == 'c'){
			rychlost_animacie = 40 - (parseInt(prud * 50));
		}
		prud_tecie 		  = this.vypocitaj_ci_tecie_prud(prud);
	}
	
	this.vypocitaj_ci_tecie_prud = function(prud){
		return prud > 0;
	}
	
	this.zisti_unikatnost_elektronu_k_atomu = function(){ 																					
		var pom_hodnota;
		var pom_pole = [];
		for (j = 0; j < volne_elektrony_metal.length; j++){
			pom_hodnota = volne_elektrony_metal[j][5]; 
			for (t = 0; t < volne_elektrony_metal.length; t++){
				if (j != t){
					if (volne_elektrony_metal[t][5] ==  pom_hodnota){
						pom_pole.push(t);
					}
				}
			}
		}
		if (pom_pole.length > 0){
			return pom_pole;
		}
		return false;
	}
	
	this.init_el_nevodice = function(){
		elektrony_nevodicov = [];
		for (i = 0; i < 3; i++){
			elektrony_nevodicov.push([705, i * 100 + getRandomInt(100,140), true]);
		}
	}
	
	this.animacka_nevodicov = function(){		
		var _this = this;
		this.Kresli();
		
		for (i = 0; i < elektrony_nevodicov.length; i++){		
			if (elektrony_nevodicov[i][2]){
				elektrony_nevodicov[i][0] -= 1;
				if (705 - elektrony_nevodicov[i][0] > ((napatie + getRandomInt(0,100)) / 50)){
					elektrony_nevodicov[i][2] = false;
				}
			}else{
				elektrony_nevodicov[i][0] += 1;
				if (elektrony_nevodicov[i][0] > 705){
					elektrony_nevodicov[i][2] = true;
				}
			}
			elektron(elektrony_nevodicov[i][0], elektrony_nevodicov[i][1]);
		}
	
		pohyb_elektronov_nevodice = setTimeout(function() {_this.animacka_nevodicov.call(_this);},  rychlost_nevodicov);	
	}
	
	this.generuj_suradnice_kmitu_atomov = function(){
		pohyb_atomov_metal = setInterval(function(){  
			for (i = 0; i < pozicie_x_atomov_metal.length; i++){
				xx_kmitu_atomu[i] = getRandomInt(-kmit_atomu,kmit_atomu);
				yy_kmitu_atomu[i] = getRandomInt(-kmit_atomu,kmit_atomu);
			}
		}, 50);	
	}
	
	this.vypocitaj_vzdialenost_elektronu_od_atomu = function(x1,y1,h){
		return Math.sqrt(Math.pow((pozicie_x_atomov_metal[h] - x1), 2) + Math.pow((pozicie_y_atomov_metal[h] - y1), 2));
	}
	
	this.animacka_metalov = function(){				
		var _this = this;
		this.Kresli();
		pom_vzdialenost = (30 + kmit_atomu * 2);
		for (i = 0; i < volne_elektrony_metal.length; i++){
			if (!prud_tecie){
				if (volne_elektrony_metal[i][4] == 0){
					pom_krok = getRandomInt(180,200);
					volne_elektrony_metal[i][2]	= (getRandomInt(100,600) - volne_elektrony_metal[i][0]) / pom_krok;
					volne_elektrony_metal[i][3]	= (getRandomInt(80,300) - volne_elektrony_metal[i][1]) / pom_krok;
					volne_elektrony_metal[i][4]	= pom_krok;
				}
				volne_elektrony_metal[i][4] -= 1;
				volne_elektrony_metal[i][0] += volne_elektrony_metal[i][2];
				volne_elektrony_metal[i][1] += volne_elektrony_metal[i][3];
			}else{
				volne_elektrony_metal[i][0] -= 1;
				for (v = 0; v < pozicie_x_atomov_metal.length; v++){
					if (this.vypocitaj_vzdialenost_elektronu_od_atomu(volne_elektrony_metal[i][0],volne_elektrony_metal[i][1],v) < pom_vzdialenost){
						if (this.vypocitaj_vzdialenost_elektronu_od_atomu(volne_elektrony_metal[i][0],volne_elektrony_metal[i][1] + 1,v) < this.vypocitaj_vzdialenost_elektronu_od_atomu(volne_elektrony_metal[i][0],volne_elektrony_metal[i][1],v)){
							volne_elektrony_metal[i][1] -= 1;
						}else{	
							volne_elektrony_metal[i][1] += 1;
						}
					}
				}
			}
			
			if (volne_elektrony_metal[i][0] < 60){
				volne_elektrony_metal[i][0] = 710;
				volne_elektrony_metal[i][4] = 0;					
				pom = getRandomInt(0,1);
				if (pom == 1){
					volne_elektrony_metal[i][1] = pozicie_y_atomov_metal[i] - 50 + getRandomInt(-15,15);
				}else{
					volne_elektrony_metal[i][1] = pozicie_y_atomov_metal[i] + 50 + getRandomInt(-15,15);
				}
			}	
		}	
		this.Kresli_vsetky_elektrony_metal();	
		pohyb_elektronov_metal = setTimeout(function() {_this.animacka_metalov.call(_this);},  rychlost_animacie);	
	}
	
	this.elektron_kruzi_okolo_jadra = function(xs,ys,polomer,uhol){
		xx = polomer * Math.cos(uhol * Math.PI / 180);
		yy = polomer * Math.sin(uhol * Math.PI / 180);
		elektron(xx + xs,yy + ys);
	}
	
		
	this.Kresli_elektorny_grafit = function(poza_atom){
		for(i = 0; i < volne_elektrony_grafit.length; i++){
			for(j = 0; j < volne_elektrony_grafit[i].length; j++){
				if (poza_atom){
					if(volne_elektrony_grafit[i][j][5]){
						elektron(volne_elektrony_grafit[i][j][0],volne_elektrony_grafit[i][j][1]);
					}	
				}else{
					if(!volne_elektrony_grafit[i][j][5]){
						elektron(volne_elektrony_grafit[i][j][0],volne_elektrony_grafit[i][j][1]);
					}
				}
			}
		}
	}
	
	this.animacka_grafitu = function(){
		var _this = this;
		this.Kresli();
		this.vygeneruj_cielove_suradnice_elektronov_grafit();
		this.pohni_elektronmi_grafitu();		
		this.Kresli_elektorny_grafit(1);
		this.Kresli_strukturu_grafitu();
		this.Kresli_elektorny_grafit(0);
		
		pohyb_elektronov_grafit = setTimeout(function() {_this.animacka_grafitu.call(_this);},  rychlost_animacie);	
	}
	
	this.vygeneruj_cielove_suradnice_elektronov_grafit = function(){
		for (i = 0;i < volne_elektrony_grafit.length; i++){
			for (j = 0;j < volne_elektrony_grafit[i].length; j++){
				if (volne_elektrony_grafit[i][j][4] <= 0){		
					if (!prud_tecie){
						pom_krok =  getRandomInt(21 - rychlost_kmitu,40 - rychlost_kmitu);
						pom_kmit = 5 + rychlost_kmitu;		
						if (Math.abs(pozicie_x_atomov_grafit[i][j] - volne_elektrony_grafit[i][j][0]) > 50){	
							volne_elektrony_grafit[i][j][2] = ((pozicie_x_atomov_grafit[i][j] + getRandomInt(-pom_kmit, pom_kmit)) - volne_elektrony_grafit[i][j][0]) / (Math.abs(pozicie_x_atomov_grafit[i][j] - volne_elektrony_grafit[i][j][0]) - rychlost_kmitu);
						}else{	
							volne_elektrony_grafit[i][j][2] = ((pozicie_x_atomov_grafit[i][j] + getRandomInt(-pom_kmit, pom_kmit)) - volne_elektrony_grafit[i][j][0]) / pom_krok; 
						}						
						volne_elektrony_grafit[i][j][3] = ((pozicie_y_atomov_grafit[i] + getRandomInt(-pom_kmit, pom_kmit)) - volne_elektrony_grafit[i][j][1]) / pom_krok;			
						volne_elektrony_grafit[i][j][4] = pom_krok;
					}else{
						pom_krok = 40;
						pom_kmit = 5 + rychlost_kmitu;
						volne_elektrony_grafit[i][j][2] = ((volne_elektrony_grafit[i][j][0] - getRandomInt(40,60)) - volne_elektrony_grafit[i][j][0]) / pom_krok; 
						volne_elektrony_grafit[i][j][3] = ((pozicie_y_atomov_grafit[i] + getRandomInt(-pom_kmit,pom_kmit)) - volne_elektrony_grafit[i][j][1]) / pom_krok;
						volne_elektrony_grafit[i][j][4] = pom_krok;
					}
				} 
				if (volne_elektrony_grafit[i][j][0] < 60){
					yy = volne_elektrony_grafit[i][j][1];
					volne_elektrony_grafit[i].shift();
					volne_elektrony_grafit[i].push([705, yy, 0, 0, 0, getRandomInt(0,1)]);
				}
			}
		}	
	}
	
	this.pohni_elektronmi_grafitu = function(){
		for (i = 0;i < volne_elektrony_grafit.length; i++){	
			for (j = 0;j < volne_elektrony_grafit[i].length; j++){	
				volne_elektrony_grafit[i][j][0] += volne_elektrony_grafit[i][j][2]; 
				volne_elektrony_grafit[i][j][1] += volne_elektrony_grafit[i][j][3];
				volne_elektrony_grafit[i][j][4] -= 1;
			}
		}
	}
	
	this.dalsia_uloha = function(cast){
		tutorial_cast = cast; 
		this.tutorial();
	}
	
	this.tutorial = function(){
		var pom_ob = this;
		var texty  = [	
						'Zmeňte materiál na zlato.',
						'Zapnite experiment.',				
						'Nastavte teplotu tak, aby celkový odpor obvodu sa rovnal 100.7 ohmu.',
						'Nastavte vstupné parametre tak, aby obvodom tiekol prúd 10 A.',
						'Zmeňte materiál na grafit.',
						'Nastavte vstupné parametre tak, aby obvodom tiekol prúd 0.36 A.',
						'Nastavte vstupné parametre tak, aby obvodom tiekol prúd 0.0 A.'
					];	
	if(document.getElementById('tutorial_text_' + tutorial_cast) != null){
		document.getElementById('tutorial_text_1').innerHTML = texty[0];
		document.getElementById('tutorial_text_2').innerHTML = texty[1];
		document.getElementById('tutorial_text_3').innerHTML = texty[2];
		document.getElementById('tutorial_text_4').innerHTML = texty[3];
		document.getElementById('tutorial_text_5').innerHTML = texty[4];
		document.getElementById('tutorial_text_6').innerHTML = texty[5];
		document.getElementById('tutorial_text_7').innerHTML = texty[6];
		document.getElementById('tutorial_text_1').style.color = '#dddddd';
		document.getElementById('tutorial_text_2').style.color = '#dddddd';
		document.getElementById('tutorial_text_3').style.color = '#dddddd';
		document.getElementById('tutorial_text_4').style.color = '#dddddd';
		document.getElementById('tutorial_text_5').style.color = '#dddddd';
		document.getElementById('tutorial_text_6').style.color = '#dddddd';
		document.getElementById('tutorial_text_7').style.color = '#dddddd';
		switch(tutorial_cast){
			case 1:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				if (material == 'au'){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_1').innerHTML = texty[0] + '   <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_1').addEventListener("click",function() {pom_ob.dalsia_uloha(2)});					
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;
			case 2:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				if (spustene){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_2').innerHTML = texty[1] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_2').addEventListener("click",function() {pom_ob.dalsia_uloha(3)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;	
			case 3:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				if ((material == 'au') && (teplota == 865)){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_3').innerHTML = texty[2] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_3').addEventListener("click",function() {pom_ob.dalsia_uloha(4)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;
			case 4:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				document.getElementById('tutorial_text_4').style.color = '#000000';
				if (aktualny_prud == 10){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_4').innerHTML = texty[3] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_4').addEventListener("click",function() {pom_ob.dalsia_uloha(5)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;
			case 5:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				document.getElementById('tutorial_text_4').style.color = '#000000';
				document.getElementById('tutorial_text_5').style.color = '#000000';
				if (material == 'c'){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_5').innerHTML = texty[4] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_5').addEventListener("click",function() {pom_ob.dalsia_uloha(6)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;	
			case 6:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				document.getElementById('tutorial_text_4').style.color = '#000000';
				document.getElementById('tutorial_text_5').style.color = '#000000';
				document.getElementById('tutorial_text_6').style.color = '#000000';
				if ((material == 'c') && (aktualny_prud == 0.36)){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_6').innerHTML = texty[5] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_6').addEventListener("click",function() {pom_ob.dalsia_uloha(7)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;	
			case 7:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				document.getElementById('tutorial_text_4').style.color = '#000000';
				document.getElementById('tutorial_text_5').style.color = '#000000';
				document.getElementById('tutorial_text_6').style.color = '#000000';
				document.getElementById('tutorial_text_7').style.color = '#000000';
				if ((spustene) && (material == 'c') && (napatie == 0)){
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
					document.getElementById('tutorial_text_7').innerHTML = texty[6] + '    <a href="javascript:void(0);">Ďalšia úloha</a>';
					document.getElementById('tutorial_text_7').addEventListener("click",function() {pom_ob.dalsia_uloha(8)});
				}else{
					document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '';
				}
				break;	
			case 8:
				document.getElementById('tutorial_text_1').style.color = '#000000';
				document.getElementById('tutorial_text_2').style.color = '#000000';
				document.getElementById('tutorial_text_3').style.color = '#000000';
				document.getElementById('tutorial_text_4').style.color = '#000000';
				document.getElementById('tutorial_text_5').style.color = '#000000';
				document.getElementById('tutorial_text_6').style.color = '#000000';
				document.getElementById('tutorial_text_7').style.color = '#000000';
				document.getElementById('tutorial_progress_' + tutorial_cast).innerHTML = '<img src="images/ok.png" width="15" height="15">';
				document.getElementById('tutorial_text_' + tutorial_cast).innerHTML = 'Hotovo.';
				break;
			}
		}
	}
}








