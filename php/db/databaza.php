<?php
class spojenie{
	protected $spoj;
	protected $result;
	
	public function __construct(){
		$this->spoj = mysqli_connect('localhost', 'soa', '34455765473', 'bakdb');
		if(!$this->spoj){
			die(mysqli_connect_error());
		}
		mysqli_set_charset($this->spoj, "utf8");
		$this->osetriVstupy($_GET);
		$this->osetriVstupy($_POST);
	}
	
	public function odpoj(){
		mysqli_close($this->spoj);
	}
	
	public function __destruct(){
		$this->odpoj();
	}
	
	public function vratSpojenie(){
		return $this->spoj;
	}
	
	public function id(){
		return mysqli_insert_id($this->spoj);
	}
	
	public function query($sql){
		$this->result = mysqli_query($this->spoj, $sql);
		if($this->result === false){
			die(mysqli_error($this->spoj)." SQL: $sql");
		}
		return $this->result;
	}
	
	public function fetch_single($sql){
		$this->query($sql);
		return mysqli_fetch_assoc($this->result);
	}
	
	public function fetch_multi($sql){
		$this->query($sql);
		$rows = array();
		while($row = mysqli_fetch_assoc($this->result)){
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function osetriVstupy(&$co){
		foreach($co as $kluc => $hodnota){
			if (is_array($hodnota)){
				$this->osetriVstupy($hodnota);
			}else{
				if(isset($hodnota) && trim($hodnota) != ''){
					$co[$kluc] = mysqli_real_escape_string($this->spoj, $hodnota);
				}
			}	
		}
	}
	
}
?>