<?php
	class Main extends spojenie {
		function overUzivatelaUcitel($meno,$heslo,$idSkoly){
			$sql = "SELECT u.*, skola.nazov AS nazov_skoly, skola.id AS id_skoly 
					FROM users u
					INNER JOIN skola ON u.id_skoly = skola.id
					WHERE u.meno = '$meno' AND u.heslo = md5('$heslo') AND u.id_skoly = '$idSkoly' 
					LIMIT 1";	
			return $this->fetch_single($sql);
		}
		
		function overUzivatelaSkupiny($idSkoly,$idSkupiny,$heslo){
			$sql = "SELECT *
					FROM skupiny
					WHERE id_skoly = '$idSkoly' AND id = '$idSkupiny' AND heslo = md5('$heslo')
					";
			return $this->fetch_single($sql);		
		}
	
		function loadPocetBodov($id){
			$sql = "SELECT body FROM users WHERE id = '$id' LIMIT 1";	
			return $this->fetch_single($sql);
		}
		
		function pridajNovyTest($nazov,$test,$id_skoly){
			$sql="INSERT INTO testy (nazov,dataTestu,id_skoly) VALUES ('$nazov','$test','$id_skoly')";
			return $this->query($sql);
		}
		
		function pridajNovuSkupinu($nazov,$heslo,$idSkoly,$idUcitela){
			$sql="INSERT INTO skupiny (nazov,heslo,id_skoly,id_ucitela) VALUES ('$nazov',md5('$heslo'),'$idSkoly','$idUcitela')";
			return $this->query($sql);
		}
		
		function nacitajNazvyVsetkychTestovSkoly($idSkoly){
			$sql = "SELECT id,nazov,id_skoly 
					FROM testy
					WHERE (id_skoly = '$idSkoly' OR id_skoly = 0) AND  exist = 1
					ORDER BY id_skoly";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajNazvyVsetkychPristupnychTestovSkoly($idSkoly,$idSkupiny){
			$sql = "SELECT t.id, t.nazov, t.id_skoly , s.status 
					FROM testy t
					INNER JOIN testyskupin s ON (t.id = s.id_testu)
					WHERE (t.id_skoly = '$idSkoly' OR t.id_skoly = 0) AND s.id_skupiny = '$idSkupiny' AND s.status = 2
					ORDER BY id_skoly";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajUciteloveSkupinySkoly($idSkoly, $idUcitela){
			$sql = "SELECT id, nazov, id_skoly
					FROM skupiny
					WHERE id_skoly = '$idSkoly' AND id_ucitela = '$idUcitela'";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajPravaVsetkychSkupinPodlaSkoly($idSkoly){
			$sql = "SELECT *
					FROM testyskupin
					WHERE id_skoly = '$idSkoly'";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajTestySkupin($idTestu,$idSkupiny){
			$sql = "SELECT id 
					FROM testyskupin 
					WHERE id_testu = '$idTestu' AND id_skupiny = '$idSkupiny'";
			return $this->fetch_single($sql);		
		}
		
		function updateTestySkupin($idTestu,$idSkupiny,$status){
			$sql = "
				UPDATE testyskupin 
				SET status = '$status' 
				WHERE id_testu = '$idTestu' AND id_skupiny = '$idSkupiny';
			";
			return $this->query($sql);	
		}
		
		function insertTestySkupiny($idSkupiny,$idTestu,$pravoSkupiny,$id_skoly){
			$sql = "
				INSERT INTO testyskupin (id_testu,id_skupiny,status,id_skoly) 
				VALUES ('$idTestu', '$idSkupiny', '$pravoSkupiny', '$id_skoly')
			";
			return $this->query($sql);	
		}
		
		function nacitajLogStudenta($id_studenta){
			$sql = "SELECT l.meno_studenta, l.priezvisko_studenta, l.log_testu, l.datum, l.body, t.nazov AS nazov_testu, t.dataTestu
					FROM logtestov l			
					INNER JOIN testy t ON l.id_testu = t.id
					WHERE l.id = '$id_studenta'
			";
			return $this->fetch_single($sql);	
		}
		
		function nacitajLogSkupiny($idSkupiny){
			$sql = "SELECT l.id, l.id_testu, l.id_skoly, l.id_skupiny, l.meno_studenta, l.priezvisko_studenta, l.ip_studenta, l.datum, l.body, s.nazov AS nazov_skupiny, t.nazov AS nazov_testu
					FROM logtestov l				
					INNER JOIN skupiny s ON l.id_skupiny = s.id
					INNER JOIN testy t ON l.id_testu = t.id
					WHERE l.id_skupiny = '$idSkupiny'
					ORDER BY l.id_testu, l.body DESC
					";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajNazovTestuPodlaID($idTestu){
			$sql = "SELECT id,nazov,id_skoly FROM testy WHERE id = '$idTestu'";	
			return $this->fetch_single($sql);	
		}
		
		function vymazTest($idTestu){
			$sql="UPDATE testy SET exist = 0 WHERE id='$idTestu'";	
			return $this->query($sql);
		}
		
		function pridajZaznamStudenta($idTestu,$idSkoly,$idSkupiny,$meno,$priezvisko,$ip,$logTestu,$datum,$body){
			$sql="										
					INSERT INTO logtestov (id_testu, id_skoly, id_skupiny, meno_studenta, priezvisko_studenta, ip_studenta, log_testu, datum, body)
					VALUES ('$idTestu','$idSkoly','$idSkupiny','$meno','$priezvisko','$ip','$logTestu','$datum','$body')";			
			return $this->query($sql);
		}
		
		function nacitajZaznamStudenta($idTestu,$idSkoly,$idSkupiny,$meno,$priezvisko,$ip,$logTestu,$datum,$body){
			$sql ="	SELECT * 
					FROM logtestov
					WHERE 	id_testu = '$idTestu' 
						AND id_skoly = '$idSkoly' 
						AND id_skupiny = '$idSkupiny' 
						AND meno_studenta = '$meno' 
						AND priezvisko_studenta = '$priezvisko' 
						AND	ip_studenta = '$ip'	
			";
			return $this->fetch_single($sql);
		}
		
		function nacitajTestPodlaId($idTestu){
			$sql = "SELECT * FROM testy WHERE id = '$idTestu' AND exist = 1 LIMIT 1";	
			return $this->fetch_single($sql);
		}
		
		function upravTest($idTestu,$nazovTestu,$dataTestu){
			$sql="UPDATE testy SET nazov = '$nazovTestu', dataTestu = '$dataTestu'  WHERE id='$idTestu'";			
			return $this->query($sql);
		}
		
		function nacitajVsetkySkoly(){
			$sql = "SELECT * FROM skola";	
			return $this->fetch_multi($sql);
		}
		
		function nacitajSkupinyDanejSkoly($idSkoly){
			$sql = "SELECT * 
					FROM skupiny
					WHERE id_skoly = '$idSkoly'";	
			return $this->fetch_multi($sql);
		}
		
		function vytvorLogTestu($data){
			unset($data['idTestu']);
			unset($data['odoslatTest']);
			
			return serialize($data);	
		}
		
		function zistiCiUzAbsolvovalTest($idTestu,$pole){
			for ($j = 0;$j < count($pole);$j++){
				if ($idTestu == $pole[$j]['id']){
					return true;
				}
			}
			
			return false;
		}
		
		function separujNazvyTestov($data){
			$pom_pole[0]			 = $data[0]['id_testu'];
			$pole_testov[0]['id'] 	 = $data[0]['id_testu'];
			$pole_testov[0]['nazov'] = $data[0]['nazov_testu'];
			for ($i = 1; $i < count($data); $i++){
				if (!in_array($data[$i]['id_testu'], $pom_pole)){
					$pom_pole[] 	= $data[$i]['id_testu'];
					$pole_testov[] 	= array('id' => $data[$i]['id_testu'], 'nazov' => $data[$i]['nazov_testu']);
				}
			}
			return $pole_testov;
		}
		
		function zistiSelectedTabulkaAdministracia($test,$skupina,$data){
			for ($a = 0; $a < count($data); $a++){
				if (($data[$a]['id_testu'] == $test) && ($data[$a]['id_skupiny'] == $skupina)){
					return $data[$a]['status'];
				}
			}
			return 1;		
		}
		
		function arrayRemoveEmpty($pole){
			foreach ($pole as $key => $value) {
				if (is_array($value)) {
					$pole[$key] = $this->arrayRemoveEmpty($pole[$key]);
				}

				if (empty($pole[$key])) {
					unset($pole[$key]);
				}
			}

			return $pole;
		}
	}
?>