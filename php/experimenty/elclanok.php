﻿<div id="plocha">
	<?php
		if (!isset($_GET['op'])){
	?>
			<canvas id="myCanvas" width="780" height="500"></canvas>
			<script>
				var c=document.getElementById("myCanvas");
				var ctx=c.getContext("2d");
				
				var Elclanok = new Elektricky_clanok();
				Elclanok.Create();
			</script>	
			<img src="images/off_button.png" width="58" height="55" onclick="Elclanok.Run()" class="cell_cudlik_go" id="button_img_on_off">
			<input type="text" value="0" id="voltmeter_e_cell" class="display_voltmeter_e_cell" readonly>
	<?php
		}
	?>
</div>	
<div id="tutorial_panel">
	<?php
		include('php/ui/tutorial.php');
	?>
	<script>Elclanok.tutorial();</script>
</div>
<div id="control_panel">
	<?php
		if (!isset($_GET['op'])){
			include('php/ui/loginPanel.php');
			?>
				<center>
					<a href="index.php?experiment=elclanok&op="><button class="myButton">Informácie</button></a>
					<a href="" onclick="location.reload();"><button class="myButton">Reset</button></a>
					<br />
					<br />
					
					<script>
						$(function() {
							$( "#teplota" ).slider({
								value:298,
								min: 0,
								max: 1000,
								step: 5,
								slide: function( event, ui ) {
									Elclanok.zmen_zobrazovanu_teplotu(ui.value);
									Elclanok.zmen_teplotu(ui.value);
									Elclanok.prepocitaj_napatie_pri_zmene();
									Elclanok.tutorial();
								}
							});
						});
					</script>
					<img src="images/teplomer_ikona.png" width="45" height="80"><br />
					<span id="aktualna_teplota">298 K / 25°C</span><br /><br />
					<div id="teplota"></div>
					<br />
				</center>	
			<?php
		}else{
			echo "<script>changeBackground('#777777');</script>";
		}
	?>	
</div>	
<div id="odborny_popis" style="display:<?= $display_op[3];?>;" class="okraj_pre_menu">
	<h1>
		<div style="width: 920px; margin-left:-10px;">
			Informácie o elektro-chemickom článku
			<a href='index.php?experiment=<?=$_GET['experiment']?>' class='xko_do_prava'><img src='images/no.png' width='18' height='18' title='Zavrieť okno'>&nbsp;</a>
		</div>
	</h1>
	<?php
		include('php/odbornyPopis/op_elektrolyza.php');	
	?>
</div>