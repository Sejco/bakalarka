<div id="plocha">		
	<?php
		if (!isset($_GET['op'])){
	?>
			<canvas id="myCanvas" width="780" height="500"></canvas>
			
			<div id="obvod1" style="display: none;">
				<span class="kzSpanU1"><input type="text" value="0" class="kzInputText" id="U1O1">&nbsp;<b>V</b></span>
				<span class="kzSpanU2"><input type="text" value="0" class="kzInputText" id="U2O1">&nbsp;<b>V</b></span>
				<span class="kzSpanU3"><input type="text" value="0" class="kzInputText" id="U3O1">&nbsp;<b>V</b></span>
				<span class="kzSpanU4"><input type="text" value="0" class="kzInputText" id="U4O1">&nbsp;<b>V</b></span>
				
				<span class="kzSpanR1"><input type="text" value="0" class="kzInputText" id="R1O1">&nbsp;<b>&#8486;</b></span>
				<span class="kzSpanR2"><input type="text" value="0" class="kzInputText" id="R2O1">&nbsp;<b>&#8486;</b></span>
				<span class="kzSpanR3"><input type="text" value="0" class="kzInputText" id="R3O1">&nbsp;<b>&#8486;</b></span>
				<span class="kzSpanR4"><input type="text" value="0" class="kzInputText" id="R4O1">&nbsp;<b>&#8486;</b></span>
			</div>
			
			<div id="obvod2" style="display: none;" >
				<input type="text" value="0" id="A1O0" class="kzA1O0" readonly>
				<input type="text" value="0" id="A2O0" class="kzA2O0" readonly>
				<input type="text" value="0" id="A3O0" class="kzA3O0" readonly>
				<span class="kzSpanU1O0"><input type="text" value="1.4" class="kzInputText" id="U1O0" oninput="kontrola_vstupu('U1O0',0.1,10);Kz.ukazPruduObvodLvl0();Kz.tutorial();	">&nbsp;<b>V</b></span>
				<span class="kzSpanU2O0"><input type="text" value="1.8" class="kzInputText" id="U2O0" oninput="kontrola_vstupu('U2O0',0.1,10);Kz.ukazPruduObvodLvl0();Kz.tutorial();	">&nbsp;<b>V</b></span>
				
				<span class="kzSpanR1O0"><input type="text" value="20" class="kzInputText" id="R1O0" oninput="kontrola_vstupu('R1O0',0,100);Kz.ukazPruduObvodLvl0();Kz.tutorial();	">&nbsp;<b>&#8486;</b></span>
				<span class="kzSpanR2O0"><input type="text" value="30" class="kzInputText" id="R2O0" oninput="kontrola_vstupu('R2O0',0,100);Kz.ukazPruduObvodLvl0();Kz.tutorial();	">&nbsp;<b>&#8486;</b></span>
				<span class="kzSpanR3O0"><input type="text" value="40" class="kzInputText" id="R3O0" oninput="kontrola_vstupu('R3O0',0,100);Kz.ukazPruduObvodLvl0();Kz.tutorial();	">&nbsp;<b>&#8486;</b></span>
				
				<img src="images/off_button.png" width="58" height="55" onclick="Kz.Run()" class="kzCudlikGo" id="button_img_on_off">
			</div>
			
			<script>
				var c=document.getElementById("myCanvas");
				var ctx=c.getContext("2d");
				
				var Kz = new Kirchoff();
				Kz.Create();			
			</script>				
	<?php
		}else{
			echo "<script>changeBackground('#777777');</script>";
		}
	?>
</div>	
<div id="tutorial_panel">
	<?php
		include('php/ui/tutorial.php');
	?>
	<script>Kz.tutorial();</script>
</div>
<div id="control_panel">
	<?php
		if (!isset($_GET['op'])){
			include('php/ui/loginPanel.php');
	?>
			<center>
				<a href="index.php?experiment=kz&op="><button class="myButton">Informácie</button></a>
				<a href="" onclick="location.reload();"><button class="myButton">Reset</button></a>
				<br />
				<br />
				<script>
					$(function() {
						$( "#u1" ).slider({
							value:1.4,
							min: 0.1,
							max: 10,
							step: 0.1,
							slide: function( event, ui ) {
								Kz.zmenHodnotuSuciastky('U1O0',ui.value);
								Kz.zmenHodnotuNadSliderom('aktualU1O0',ui.value,'V');	
								Kz.ukazPruduObvodLvl0();
								Kz.tutorial();								
							}
						});
					});
				</script>
				<img src="images/u1.png" width="160" height="60"><br />
				<span id="aktualU1O0">1.4 V</span><br /><br />
				<div id="u1"></div>
				<br />
				
				<script>
					$(function() {
						$( "#u2" ).slider({
							value:1.8,
							min: 0.1,
							max: 10,
							step: 0.1,
							slide: function( event, ui ) {
								Kz.zmenHodnotuSuciastky('U2O0',ui.value);
								Kz.zmenHodnotuNadSliderom('aktualU2O0',ui.value,'V');	
								Kz.ukazPruduObvodLvl0();
								Kz.tutorial();		
							}
						});
					});
				</script>
				<img src="images/u2.png" width="160" height="60"><br />
				<span id="aktualU2O0">1.8 V</span><br /><br />
				<div id="u2"></div>
				<br /><br />
				
				<script>
					$(function() {
						$( "#r1" ).slider({
							value:20,
							min: 0,
							max: 100,
							step: 1,
							slide: function( event, ui ) {
								Kz.zmenHodnotuSuciastky('R1O0',ui.value);
								Kz.zmenHodnotuNadSliderom('aktualR1O0',ui.value,'&#8486;');
								Kz.ukazPruduObvodLvl0();
								Kz.tutorial();	
							}
						});
					});
				</script>
				<img src="images/r1.png" width="160" height="30"><br />
				<span id="aktualR1O0">20 &#8486;</span><br /><br />
				<div id="r1"></div>
				<br />
				
				<script>
					$(function() {
						$( "#r2" ).slider({
							value:30,
							min: 0,
							max: 100,
							step: 1,
							slide: function( event, ui ) {
								Kz.zmenHodnotuSuciastky('R2O0',ui.value);
								Kz.zmenHodnotuNadSliderom('aktualR2O0',ui.value,'&#8486;');
								Kz.ukazPruduObvodLvl0();
								Kz.tutorial();
							}
						});
					});
				</script>
				<img src="images/r2.png" width="160" height="30"><br />
				<span id="aktualR2O0">30 &#8486;</span><br /><br />
				<div id="r2"></div>
				<br />
				
				<script>
					$(function() {
						$( "#r3" ).slider({
							value:40,
							min: 0,
							max: 100,
							step: 1,
							slide: function( event, ui ) {
								Kz.zmenHodnotuSuciastky('R3O0',ui.value);
								Kz.zmenHodnotuNadSliderom('aktualR3O0',ui.value,'&#8486;');	
								Kz.ukazPruduObvodLvl0();	
								Kz.tutorial();	
							}
						});
					});
				</script>
				<img src="images/r3.png" width="160" height="30"><br />
				<span id="aktualR3O0">40 &#8486;</span><br /><br />
				<div id="r3"></div>
				<br />
				
		
				
			</center>
	<?php
		}	
	?>		
</div>	
<div id="odborny_popis" style="display:<?= $display_op[0];?>;" class="okraj_pre_menu">
	<h1>
		<div style="width: 920px; margin-left:-10px;">
			Informácie o Kirchoffových zákonoch
			<a href='index.php?experiment=kz' class='xko_do_prava'><img src='images/no.png' width='18' height='18' title='Zavrieť okno'>&nbsp;</a>
		</div>
	</h1>
	<?php
		include('php/odbornyPopis/op_kz.php');
	?>
</div>