﻿<div id="plocha">
	<?php
		if (!isset($_GET['op'])){
	?>
			<canvas id="myCanvas" width="780" height="500"></canvas>
			<script>
				var c=document.getElementById("myCanvas");
				var ctx=c.getContext("2d");
				
				var Polovodic = new Polovodice();
				Polovodic.Create();
				
				active_tab(function(){
					if (!active_tab()){
						Polovodic.Stop()
					}
				});
			</script>	
			<img src="images/off_button.png" width="58" height="55" onclick="Polovodic.Run()" class="polovodice_cudlik_go" id="button_img_on_off">
			<button onclick="Polovodic.Zmen_smer();Polovodic.tutorial();" class="polovodice_cudlik_zmen_smer">Zmeň smer</button>	
			<input type="text" value="0" id="display_ampermetra" class="display_ampermetra_input" readonly>
	<?php
		}
	?>
</div>	
<div id="tutorial_panel">
	<?php
		include('php/ui/tutorial.php');
	?>
	<script>Polovodic.tutorial();</script>
</div>
<div id="control_panel">
	<?php
		if (!isset($_GET['op'])){
			include('php/ui/loginPanel.php');
	?>
			<center>
				<a href="index.php?experiment=pn_prechod&op="><button class="myButton">Informácie</button></a>
				<a href="" onclick="location.reload();"><button class="myButton">Reset</button></a>
				<br />
				<br />
				
				<script>
					$(function() {
						$( "#teplota" ).slider({
							value:300,
							min: 0,
							max: 1000,
							step: 5,
							slide: function( event, ui ) {
								Polovodic.zmen_zobrazovanu_teplotu(ui.value);
								Polovodic.zmen_teplotu(ui.value);
								Polovodic.prepoctaj_prud_pri_zmene();
								Polovodic.tutorial();
							}
						});
					});
				</script>
				<img src="images/teplomer_ikona.png" width="45" height="80"><br />
				<span id="aktualna_teplota">300 K / 27°C</span><br /><br />
				<div id="teplota"></div>
				<br />
				
				<script>
					$(function() {
						$( "#napatie" ).slider({
							value:0.8,
							min: 0,
							max: 1,
							step: 0.02,
							slide: function( event, ui ) {
								Polovodic.zmen_zobrazovane_napatie(ui.value);
								Polovodic.zmen_napatie(ui.value);
								Polovodic.prepoctaj_prud_pri_zmene();
								Polovodic.tutorial();
							}
						});
					});
				</script>
				<img src="images/battery.png" width="75" height="80"><br />
				<span id="aktualne_napatie">0.80 V</span><br /><br />
				<div id="napatie"></div>
				<br />
				
				<img src="images/kremik.png" width="90" height="90" id="obrazok_prvku"><br />
				<span id="aktualny_prvok">Kremík</span><br /><br />
					<form>
					<input type="radio" id="si" name="prvok" onchange="		Polovodic.zmen_zobrazovane_prvku(1);
																			Polovodic.zmen_prvok(1);																		
																			Polovodic.prepoctaj_prud_pri_zmene();
																			Polovodic.tutorial();" checked >
					<label for="si">Si <sub>14</sub></label>
					<input type="radio" id="ge" name="prvok" onchange="		Polovodic.zmen_zobrazovane_prvku(2);
																			Polovodic.zmen_prvok(2);																		
																			Polovodic.prepoctaj_prud_pri_zmene();
																			Polovodic.tutorial();">
					<label for="ge">Ge <sub>32</sub></label>
				</form>
			</center>	
	<?php
		}else{
			echo "<script>changeBackground('#777777');</script>";
		}
	?>	
</div>	
<div id="odborny_popis" style="display:<?= $display_op[2];?>;" class="okraj_pre_menu">
	<h1>
		<div style="width: 920px; margin-left:-10px;">
			Informácie o PN prechode
			<a href='index.php?experiment=pn_prechod' class='xko_do_prava'><img src='images/no.png' width='18' height='18' title='Zavrieť okno'>&nbsp;</a>
		</div>
	</h1>
	<?php
		include('php/odbornyPopis/op_pn_prechod.php');	
	?>
</div>