﻿<div id="plocha">		
	<?php
		if (!isset($_GET['op'])){
	?>
			<canvas id="myCanvas" width="780" height="500"></canvas>
			<script>
				var c=document.getElementById("myCanvas");
				var ctx=c.getContext("2d");
				
				var Transformator = new Trafo();
				Transformator.Create();
				
				// active_tab(function(){
				//	if (!active_tab()){
				//		Polovodic.Stop()
				//	}
				//});
			</script>	
			<img src="images/off_button.png" width="58" height="55" onclick="Transformator.Run();" class="trafo_cudlik_go" id="button_img_on_off">
			<input type="text" value="0" id="display_voltmeter" class="trafo_display_voltmeter" readonly>	
	<?php
		}else{
			echo "<script>changeBackground('#777777');</script>";
		}
	?>
</div>	
<div id="tutorial_panel">
	<?php
		include('php/ui/tutorial.php');
	?>
	<script>Transformator.tutorial();</script>
</div>
<div id="control_panel">
	<?php
		if (!isset($_GET['op'])){
			include('php/ui/loginPanel.php');
	?>
			<center>
				<a href="index.php?experiment=tranformatorovy_jav&op="><button class="myButton">Informácie</button></a>
				<a href="" onclick="location.reload();"><button class="myButton">Reset</button></a>
				<br />
				<br />

				<script>
					$(function() {
						$( "#zavity_1" ).slider({
							value:800,
							min: 50,
							max: 1000,
							step: 5,
							slide: function( event, ui ) {
								Transformator.zmen_pocet_zavitov(ui.value,false);
								Transformator.ukaz_vystupne_napatie();
								Transformator.tutorial();
							}
						});
					});
				</script>
				<img src="images/cievka_1.jpg" width="60" height="55"><br />
				<span id="aktualne_zavity_1">800 závitov</span><br /><br />				
				<div id="zavity_1"></div>
				<br />
				
				<script>
					$(function() {
						$( "#zavity_2" ).slider({
							value:200,
							min: 50,
							max: 1000,
							step: 5,
							slide: function( event, ui ) {
								Transformator.zmen_pocet_zavitov(false,ui.value);
								Transformator.ukaz_vystupne_napatie();
								Transformator.tutorial();
							}
						});
					});
				</script>
				<img src="images/cievka_2.jpg" width="60" height="55"><br />
				<span id="aktualne_zavity_2">200 závitov</span><br /><br />
				<div id="zavity_2"></div>
				<br />
				
				<script>
					$(function() {
						$( "#zdroj" ).slider({
							value:5000,
							min: 50,
							max: 10000,
							step: 50,
							slide: function( event, ui ) {								
								Transformator.zmen_vstupne_napatie(ui.value);
								Transformator.zmen_zobrazovane_napatie();
								Transformator.ukaz_vystupne_napatie();
								Transformator.tutorial();
							}
						});
					});
				</script>
				<img src="images/battery.png" width="60" height="55"><br />
				<span id="aktualne_napatie">5 000 V</span><br /><br />
				<div id="zdroj"></div>
				<br />
				
				
				<img src="images/striedavy_prud.png" width="100" height="55" id="img_druh_prudu"><br />
				<span id="druh_prudu">Striedavý prúd</span><br /><br />
				<form>
					<input type="radio" id="ac_prud" name="typ_prudu" onchange="Transformator.zmen_zobrazovane_napatie(1);
																				Transformator.zmen_img_prudu(1);
																				Transformator.ukaz_vystupne_napatie();" checked >
					<label for="ac_prud">AC &#x2248;</label>
					<input type="radio" id="dc_prud" name="typ_prudu" onchange="Transformator.zmen_zobrazovane_napatie(2);
																				Transformator.zmen_img_prudu(2);
																				Transformator.ukaz_vystupne_napatie();">
					<label for="dc_prud">DC =</label>
				</form>
				<br /><br />
			</center>
	<?php
		}	
	?>		
</div>	
<div id="odborny_popis" style="display:<?= $display_op[1];?>;" class="okraj_pre_menu">
	<h1>
		<div style="width: 920px; margin-left:-10px;">
			Informácie o transformátorovom jave
			<a href='index.php?experiment=tranformatorovy_jav' class='xko_do_prava'><img src='images/no.png' width='18' height='18' title='Zavrieť okno'>&nbsp;</a>
		</div>
	</h1>
	<?php
		include('php/odbornyPopis/op_transformatorovy_jav.php');
	?>
</div>