<div id="plocha">
	<?php
		if (!isset($_GET['op'])){
	?>
			<canvas id="myCanvas" width="780" height="500"></canvas>
			<script>
			
				var c=document.getElementById("myCanvas");
				var ctx=c.getContext("2d");
				var Vodice = new Vodice_a_izolanty();
				var svetlo = new Image();
				svetlo.src = 'images/spherelight.png';
				svetlo.onload = function(){			
					Vodice.Create();	
				}
				
			</script>	
			<img src="images/off_button.png" width="58" height="55" onclick="Vodice.Run();Vodice.tutorial();" class="vodice_cudlik_go" id="button_img_on_off">
			<input type="text" value="0" id="display_ampermetra" class="display_ampermetra_vodice" readonly>
			<input type="text" value="0 &#937;" id="display_ohmmetra" class="display_ohmmetra_vodice" readonly>			
	<?php
		}
	?>
</div>	
<div id="tutorial_panel">
	<?php
		include('php/ui/tutorial.php');
	?>
	<script>Vodice.tutorial();</script>
</div>
<div id="control_panel">
	<?php
		if (!isset($_GET['op'])){
			include('php/ui/loginPanel.php');
	?>
			<center>
				<a href="index.php?experiment=vodice&op="><button class="myButton">Informácie</button></a>
				<a href="" onclick="location.reload();"><button class="myButton">Reset</button></a>
				<br />
				<br />
				
				<script>
					$(function() {
						$( "#teplota" ).slider({
							value:293,
							min: 0,
							max: 1000,
							step: 5,
							slide: function( event, ui ) {
								Vodice.zmen_zobrazovanu_teplotu(ui.value);	
								Vodice.zmen_teplotu(ui.value);
								Vodice.zmen_hodnotu_ohmmetra();
								Vodice.zmen_hodnotu_ampermetra();
								Vodice.tutorial();
							}
						});
					});
				</script>
				<img src="images/teplomer_ikona.png" width="45" height="80"><br />
				<span id="aktualna_teplota">293 K / 20°C</span><br /><br />
				<div id="teplota"></div>
				<br />
				
				<script>
					$(function() {
						$( "#napatie" ).slider({
							value:500,
							min: 0,
							max: 1000,
							step: 5,
							slide: function( event, ui ) {
								Vodice.zmen_zobrazovane_napatie(ui.value);
								Vodice.zmen_napatie(ui.value);
								Vodice.zmen_hodnotu_ampermetra();	
								Vodice.tutorial();
							}
						});
					});
				</script>
				<img src="images/battery.png" width="75" height="80"><br />
				<span id="aktualne_napatie">500 V</span><br /><br />
				<div id="napatie"></div>
				<br /><br />
				
				<img src="images/cu.png" width="100" height="100" id="obrazok_materialu"><br /><br /><br />
				<select id="id_materialu" onchange="Vodice.zmen_zobrazovany_material();Vodice.zmen_material();Vodice.zmen_hodnotu_ohmmetra();Vodice.zmen_hodnotu_ampermetra();Vodice.tutorial();" style="width:100px;">
					<option value="cu">Meď</option>
					<option value="ag">Striebro</option>
					<option value="au">Zlato</option>
					<option value="si">Kremík</option>
					<option value="c">Grafit</option>
					<option value="di">Diamant</option>					
				</select>
			</center>	
	<?php
		}else{
			echo "<script>changeBackground('#777777');</script>";
		}
	?>	
</div>	
<div id="odborny_popis" style="display:<?= $display_op[4];?>;" class="okraj_pre_menu">
	<h1>
		<div style="width: 920px; margin-left:-10px;">
			Informácie o vodičoch a izolantoch
			<a href='index.php?experiment=vodice' class='xko_do_prava'><img src='images/no.png' width='18' height='18' title='Zavrieť okno'>&nbsp;</a>
		</div>
	</h1>
	<?php
		include('php/odbornyPopis/op_vodice_a_izolanty.php');	
	?>
</div>