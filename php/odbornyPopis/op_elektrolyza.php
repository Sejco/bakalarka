<br />

<b>Čo to je elektrochemický článok?</b>
<p align="justify">
	Elektrochemický alebo inak povedaná galvanický článok je zdrojom elektrickej energie. Táto elektrická energia vzniká vďaka chemickým reakciám, ktoré tu prebiehajú. Existuje obrovské množstvo rôznych 
	druhov elektrochemických článkov. Tento konkrétny sa voá Daniellov článok a bol vynájdený v roku 1836 britským chemikom a meteorológom Johnom Fredericom Daniellom.
</p>

<br />

<b>Na čo slúži?</b>
<p align="justify">
	Elektrochemický článok je v podstate baterka, ktorá poháňa každé zariadenie, ktoré nosíme so sebou. Či už je to notebook, mobil, tablet alebo hodinky. Tento kontrétny elektrochemický článok je
	však už zastaralý, ale vďaka jeho pricípu mohli vzniknúť moderné batérie s dlhou životnosťou.
</p>

<br />

<b>Ako to funguje?</b>
<p align="justify">
	Elektrochemický článok je zložený z dvoch oddelených nádob pričom v prvej sa nachádza roztok síranu zinočnatého <b>ZnSO<sub>4</sub></b> a v druhej roztok síranu meďnatého <b>CuSO<sub>4</sub></b>.
	Obe tieto nádoby sú prepojené takzvaným mostíkom, ktorý zabezpečuje vyrovnávanie elektrických nábojov v nádobách tým, že umožňuje cestovať iónom medzi nádobami. Tento mostík obsahuje roztok nitrátu 
	draselného <b>KNO<sub>3</sub></b>. V každej z nádob je okrem roztoku umiestnená aj elektoróda v prvej nádobe je mzinková elektróda a v druhej je medenná. 
</p>

<p align="justify">
	Princíp je taký, že sa zo zinkovej elektródy do roztoku síranu zinočnatého odtrhne jeden atóm zinku. Ten však v zinkovej elektróde zanechá 2 elektróny. Túto chemickú reakciu môžeme zapísať nasledujúcou rovnicou:
</p>
<br />
<center><b>Zn<sub>(s)</sub> &rarr; Zn<sup>2+</sup><sub>(aq)</sub> + 2e<sup>-</sup></b></center>
<br />
<center><img src="images/op/nadoba1.png" width="250" height="300" title="Reakcia zinku"></center>
<p align="justify">
	Tieto 2 elektróny precestujú elektrickým obvodom až do medennej elektródy. Tam spôsobia záporný elektrický náboj a tým pádom sa na medennú elektródu nalepí jeden atóm medi z roztoku síranu meďnatého.
	Túto chemickú reakciu popisuje nasledujúca rovnica:
</p>
<br />
<center><b>Cu<sup>2+</sup><sub>(aq)</sub> + 2e<sup>-</sup> &rarr; Cu<sub>(s)</sub> </b></center>
<br />
<center><img src="images/op/nadoba2.png" width="250" height="300" title="Reakcia medi"></center>
<p align="justify">
	Obe tieto reakcie zmenili elektrocký náboj v oboch nádobách. Nádobe so síranom zinočnatým teraz chýbajú 2 elektróny, takže má kladný náboj. A naopak nádoba so síranom meďnatým má 2 elektróny navyše. 
	Na vyrovnanie týchto nábojov slúži práve soľný mostík. Do prvej nádoby s kladným nábojom sa začnú pohybovať 2 molekuly <b>NO<sub>3</sub><sup>-</sup></b> a do druhej nádoby zase 2 atómy draslíku 
	<b>K<sup>+</sup></b>, ktoré vzniknú roztrhnutím molekuly <b>KNO<sub>3</sub></b>.
</p>
<br /><br />

