<br />

<b>Čo sú to Kirchoffove zákony?</b>
<p align="justify">
	Kirchoffové zákony pozostávajú z dvoch pravidiel, ktoré v polovici 19. storočia matematicky vyjadril nemecký fyzik Gustav Robert Kirchoff(1824-1887). Tieto dve pravidlá patria k základom teórie elektrických obvodov.  Používajú sa najmä pre výpočet veľkosti a smeru elektrikých prúdov v jednotlivých vetvách  rozvetvených obvodov a na výpočet veľkosti elektrického napätia na jednotlivých komponentoch daného obvodu. Pri analýze elektrického obvodu sa dajú použiť obe tieto pravidlá. Pri výpočte je potrebné zostaviť a vyriešiť sustavu N rovníc o N neznámych. Počet rovníc a neznámych závisí od počtu uzlov a slučiek v danom obvode. Tieto zákony sa dajú použiť nielen v obvodoch s jednosmerným prúdom, ale aj v obvodoch so striedavým prúdom.
</p>

<br />

<b>Prvý Kirchoffov zákon:</b>
<p align="justify">
	Platí pre rozvetvený elektrický obvod. Rozvetvený elektrický obvod nazývame taký, ktorý obsahuje aspoň 2 uzly. Uzol je miesto, kde sa stretávajú aspoň 3 vodiče. Časť elektrického obvodu, ktorá sa nachádza medzi jednotlivými uzali sa nazýva vetva. Prvý kirchoffov zákon popisuje práve uzly v el. obvodoch. Vyjadruje, že algebraický súčet prúdov do uzla vtekajúcich a z uzla vytekajúcich sa rovná nule (viď obrázok). Pri aplikácií v el. obdovodoch sa za pomoci tohto pravidla zostavujú rovnice pre všetky nezávislé uzly.
</p>
<center>
	<img src="images/op/kzImg1.jpg" width="450" height="130" title="Prvý Kirchoffov zákon">
</center>
<br />
<center>
	<table class="tableRovnica">
		<tr>
			<td></td>
			<td></td>
			<td>I1</td>		
			<td>+</td>	
			<td>I2</td>
			<td>=</td>
			<td>I3</td>
		</tr>
		<tr>
			<td>I1</td>
			<td>+</td>
			<td>I2</td>
			<td>-</td>
			<td>I3</td>
			<td>=</td>
			<td>0</td>
		</tr>	
	</table>
</center>

<br />

<b>Druhý Kirchoffov zákon:</b>
<p align="justify">
	Kirchoffov zákon hovorí o napätí v slučkách. Slučka je uzavretá cesta v el. obvode. Dalo by sa to prirovnať k uzatvorenej ceste v grafe s tým, že každú cestu môžeme prejsť len raz a jedna cesta sa nemôže križovať s inou. Sú to takzvané oká elektrického obvodu. Pre lepšie vysvetlenie slučky viď obrázok na ktorom sú slučky označené číslicami 1 a 2. Pre každú takúto slučku v obvode platí, že súčet úbytkov napätí na jednotlivých prvkoch slučky sa rovná súčtu napätí vnútorných napäťových zdrojov. Celkový súčet napätí slučky by sa teda mal rovnať nule.
</p>
<center>
	<img src="images/op/kzImg2.jpg" width="550" height="350" title="Druhý Kirchoffov zákon">
</center>
<p align="justify">
	Pripomeňme si, že napätie <b>U</b> vypočítame ako súčin odporu <b>R</b> s prúdom <b>I</b>. Teda <b>U = R * I</b>. Pre prvú slučku určím jej rovnicu nasledovne. Najprv pozriem na orientáciu napäťového zdroja. Prúd zdrojom prechádza z + do -, naznačím si teda odahovaný smer prúdu prostredníctvom šípky pri danom zdroji. Rovnakým postupom prejdem celý obdov a naznačím si predpokladaný smer prúdu na všetkých komponentoch obvodu. Nesmieme pri tom však zabudnúť, že prúd tečie vždy z + do -.
</p>
<center>
	<img src="images/op/kzImg3.jpg" width="550" height="350" title="Smer prúdu">
</center>
<p align="justify">
	Teraz už môžeme zostaviť samotné rovnice pre tento obdov pomocou druhého Kirchoffovho zákona. Začneme od zdroja U1. Orientácia prúdu na tomto zdroji je opačná ako je smer, ktorý udáva slučka 1. takže do prvej rovnice pridám <b>-U1</b>. Pokračujem na rezistor R1. Vidím, že orientácia prúdu je rovnaká ako smer slučky takže pridám <b>+R1</b>, ale keďže druhý Kirchoffov zákon hovorí o napätiach a napätie vypočítam ako <b>R * I</b>, musím <b>R1</b> prenásobiť aj príslušným prúdom, čiže <b>*I1</b>. Postupujem ďalej k rezistoru R3. Tu aplikujem rovnaký postup ako pri R1, takže dostávam <b>R3 * I3</b>. Dostávam sa znovu k zdroju U1 a týmto pádom som dokončil rovnicu pre prvú slučku. Zostavá už len aplikovať druhý Kirchoffov zákon a celú rovnicu položiť rovnú 0. Rovnakým postupom zostavím rovnicu aj pre druhú slučku. Vo výsledku by mali rovnice vyzerať nasledovne:
</p>
<center>
	<table class="tableRovnica">
		<tr>
			<td>1.</td>
			<td>-</td>
			<td>U1</td>
			<td>+</td>
			<td>R1*I1</td>
			<td>+</td>
			<td>R3*I3</td>
			<td>= 0</td>
		</tr>
		<tr>
			<td>2.</td>
			<td></td>
			<td>U2</td> 
			<td>-</td> 
			<td>R3*I3</td> 
			<td>-</td> 
			<td>R2*I2</td> 
			<td>= 0</td>
		</tr>	
	</table>
</center>

<br />
<b>Ako to funguje?</b>
<p align="justify">
	V našom obvode sú umiestnené aj 3 ampérmetre(zelený, oranžový, fialový), ktoré nám ukážu prúd v jednotlivých vetvách obdodu. V každej vetve tečie rozdielne veľký prúd. Pre výpocet tohto prúdu služia práve Kirchoffove zákony. Spojím rovnivnice z oboch zákonov a dostanem sústavu troch rovníc o troch neznámych. Neznáme sú práve prúdy <b>I1 , I2</b> a <b>I3</b>. Rovnice budú vyzerať nasledovne:
</p>
<center>
	<table class="tableRovnica">
		<tr>
			<td>-</td>
			<td>U1</td>
			<td>+</td>
			<td>R1*I1</td>
			<td>+</td>
			<td>R3*I3</td>
			<td>=</td> 
			<td>0</td>
		</tr>
		<tr>
			<td></td>
			<td>U2</td> 
			<td>-</td> 
			<td>R3*I3</td> 
			<td>-</td> 
			<td>R2*I2</td> 
			<td>=</td> 
			<td>0</td>
		</tr>	
		<tr>
			<td></td>
			<td>I1</td>
			<td>+</td>
			<td>I2</td>
			<td>-</td>
			<td>I3</td>
			<td>=</td>
			<td>0</td>
		</tr>
	</table>
</center>
<p align="justify">
	Dosadím hodnoty <b>U1, U1, R1, R2</b> a <b>R3</b>. A dostanem hodnoty neznámych prúdov. Ak sa však stane, že vypočítaný prúd bude mať zápornú hodnotu, znamená to, že tečie presne opačným smerom ako som si ho zakreslil.
</p>
<center>
	<img src="images/op/kzImg4.jpg" width="550" height="350" title="Výpočet prúdov">
</center>
<p align="justify">
	V tomto prípade však všetky prúdy tečú tak, ako sme si ich pôvodne zakreslili. Výsledné hodnoty prúdov pre aktuálne zvolené parametre teda sú:
</p>
<center>
	<table class="tableRovnica">
		<tr>
			<td>I1</td>
			<td>=</td> 
			<td>0.1</td>
			<td>A</td>
		</tr>
		<tr>
			<td>I2</td>
			<td>=</td> 
			<td>0.2</td>
			<td>A</td>
		</tr>
		<tr>
			<td>I3</td>
			<td>=</td> 
			<td>0.3</td>
			<td>A</td>
		</tr>
	</table>
</center>

<br />
<br />