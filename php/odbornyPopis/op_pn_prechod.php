<br />
<b>Čo je to PN prechod?</b>
<p align="justify">
	PN prechod je rozhranie medzi dvoma rôznymi typmi polovodičov. Je zložený z polovodiča typu P a polovodiča typu N. Toto rozhranie dvoch typov polovodičov zabezpečuje tok elektrického prúdu iba jedným smerom. Je súčasťou každej polovodičovej súčiastky ako je napríklad dióda, tranzistor, fotodióda atď.
</p>
<center>
	<table>
		<tr>
			<td><img src="images/op/pnImg3.jpg" width="120" height="120" title="Dióda"></td>
			<td><img src="images/op/pnImg2.jpg" width="120" height="120" title="Tranzistor"></td>
			<td><img src="images/op/pnImg1.jpg" width="120" height="120" title="Fotodióda"></td>	
		</tr>
	</table>
</center>

<b>Polovodič typu P</b>
<p align="justify">
	Polovodiče sú tvorené takzvanými štvormocnými prvkami ako je napríklad <b>Kremík</b>, <b>Germánium</b>, <b>Selen</b> a mnoho iných zlúčením. Štvormocný prvok je prvok, ktorý má na vonkajšej vrstve elektrónové ho obalu práve 4 elektróny. Tie sa nazývajú aj valenčné elektróny. Najčastejšie sa však používa <b>Kremík</b> a <b>Germánium</b>. Tie tvoria dokonalú mriežku. Pre vytvorenie polovodiču typu P sa však do tejto kryštalickej štruktúry pridávajú trojmocné prvky ako napríklad <b>Indium</b>, <b>Bór</b>, <b>Gálium</b> alebo <b>Hliník</b>. Tieto prvky majú iba tri valenčné elektróny a tým pádom vzniznú v kryštalickej mriežke prázdne miesta. Tieto prázdne meista sa nazývajú <b>diery</b> a správajú sa ako keby mali kladný náboj.
</p>
<center>
	<img src="images/op/pnImg4.jpg" width="580" height="200" title="Polovodič typu P">
</center>
<br />

<b>Polovodič typu N</b>
<p align="justify">
	Rovnako ako polovodič typu P je aj polovodič typu N tvorený štvor mocnými prvkami. V tomto prípade sa však do kryštalickej mriežky pridáva až päť mocný prvok. Medzi päť mocné prvky patrí napríklad <b>Arzén</b> alebo <b>Fosfor</b>. Tie v kryštalickej mriežke nadviažu všetky 4 väzby s okolnými atómami štvormocných prvkov a navyše tam zanechájú aj jeden extra elektrón, ktorý má samozrejme záporný náboj.
</p>
<center>
	<img src="images/op/pnImg5.jpg" width="580" height="200" title="Polovodič typu N">
</center>
<br />

<b>Ako to funguje?</b>
<p align="justify">
	Spojením týchto dvoch typov polovodičov získame spomínaný PN prechod. Oblať N má navyše elektrón, ktorý v oblasti P chýba. V oblasti P sa nachádza diera, ktorá má kaldný náboj. Elektróny z okolných atómov môžu preskočiť do diery a zaplniť ju. Tým pádom vznikne nová diera, ale na inom mieste. Takýmto spôsobom môžu diery cestovať. Cestujú až do chvíle, kým sa nedostanú do blizkosti päť mocného prvku, ktorý má vo svojom okolí jeden elektrón navyše. Sila ktorov je elektrón priťahovaný k svojmu jadru je menšia ako náboj diery a preto ju tento elektrón zaplní. Elektrón, ktorý pricestoval k troj mocnému prvku cestuje ďalej a zanecháva po sebe znovu ďalšiu dieru. Rovnako je to aj s pät mocným prvkom na druhej strane polovodiča. K nemu zase pricestuje nový elektrón, ktorý sa pohybuje elektrickým obvodom.
	<br /><br />
	Ak však vymeníme polaritu zdroja, voľný elektrón z polovodiča typu N odcestuje ku kladnému pólu zdroja a zo záporného pólu zdroja vyletí elektrón, ktorý zaplní dieru v polvodiči typu P. Tým pádom sa už nič ďalšie nestane, pretože prúd nemá ako ďalej tiecť skrz PN prechod. To znamená, že PN prechod prepúšťa prúd iba jedným smerom.
</p>




<br />
<br />