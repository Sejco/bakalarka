﻿<br />
<b>Čo je to transformátor?</b>
<p align="justify">
Transformátor je elektircký sroj, slúžiaci na prenos a transformáciu striedavého elektirckého napätia medzi dvoma obvodmi. Je tvorený feromagnetickým jadrom a dvoma cievkami. Cievky 
sú navinuté na spoločnom jadre transformátora. Cievka, ktorá je pripojená na vstupné napätie sa nazýva primárna cievka a cievka, ktorá je pripojená na
výstupný obdov sa nazýva sekundárna.
</p>
<center><img src="images/op/trafo_obr_1.jpg" width="350" height="200"></center>
<b>Na čo slúži?</b>
<p align="justify">
Pri zmene elekrického napätia sa zmení aj elektircký prúd, ktorý tečie vodičom. Čím je napätie väčšie, tým je prúd menší. Čím je prúd menší, tým tenšie vodiče
sú potrebné na jeho distribúciu. Preto sa pri distribúcii elektrického prúdu na veľke vzdialenosti (priamo z elektrárne, medzi mestami, atď..) vyskytuje vo vodičoch tak 
vysoké napätie (napr. 23 000 V). Toto vysoké napätie sa potom za pomoci transformátora umiestneného v blízkosti vašeho domova transformuje na použitelnejšie napätie 
napríklad 230 V, na ktorých funguje väčšina domácich spotrebičov.  
</p>
<center><img src="images/op/trafo_obr_2.jpg" width="250" height="160"></center>
<b>Ako to funguje?</b>
<p align="justify">
Elektrický prúd, ktorý preteká vodičom vytvorí v okolí tohoto vodiča slabé elektromagnetické pole. Ak sa dva alebo viacero vodičov nachádza blízko seba a cez 
každý preteká prúd, ich magnetické polia sa zlúčia a vytvoria jedno veľké. Inými slovami, cievka pozostávajúca z 10 závitov vygeneruje o polovicu menšie 
elektromagnetické pole, ako cievka pozostávajúca z 20 závitov. Ak je cievka generujúca elektromagnetické pole navinutá na feromagnetickom jadre, jadro sa 
zmagnetizuje a vytvorí sa v ňom magnetický indukčný tok. To znamená, že pôvodné elektromagnetické pole, generované primárnou cievkou zasiahne aj cievku sekundárnu.
Ak na sekundárnu cievku bude pôsobiť toto elektromagnetické pole, podľa Faradayovho indukčného zákona sa v nej začne indukovať elektrické napätie. 
</p>
<center><b><i>u = -N * (d&#934; / dt)</i></b></center>
<p align="justify">
To však platí iba ak je do primárnej cievky privedené striedavý prúd. Zmena veľkosti napätia totiž spôsobí, že magentický indukčný tok vo feromagnetickom
jadre bude meniť svoju veľkosť a smer. Ak by sme do primárnej cievky pustili jednosmerný prúd, v jadre by sa vytvoril konštantný magnetický tok a ten by v sekundárnej
cievke neindukoval žiadne napätie, pretože derivácia konštanty je nulová. Výsledné napätie sa teda vypočíta na základe transformačného pomeru P, kde:
</p>
<center><b><i>P = N1 / N2 = U1 / U2</i></b></center>
<p align="justify">
N1 a N2 predstavujú počty závitov primárnej a sekundárnej cievky. U1 a U2 sú napätia na jednotlivých cievkach. Ak je transformačný pomer P vačší ako 1, jedná sa 
o transformáciu nahor, teda výstupnénapätie bude väčšie ako vstupné, v opačnom prípade sa bude jednať o transformáciu nadol. Tento vzorec pre výpočet
počíta s ideálnym transformátorom, to znamená, že transformácia prebieha bez strát. Skutočné transformátory však majú účinnosť približne 80% - 99%. Pri účinnosti 
<b>&#951; = 90% </b>by bolo potrebné výstupné napätie prenásobiť koeficientom 0.9, aby sme dostali presný výsledok.
</p>
<center>
	<img src="images/op/celkovy_popis_trafo.png" width="730" height="440" title="Vysvetlenie princípu transformácie"/>
</center>

<br />
<br />