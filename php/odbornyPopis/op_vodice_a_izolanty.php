<br />

<b>Čo sú to vodiče a izolanty??</b>
<p align="justify">
	Vodičmi sa označujú materiály, ktorými dokáže tiecť elektrický prúd. A izolanty sú naopak materiály, ktoré elektrický prúd izolujú. Okrem pevných látok dokážu elektrinu viesť aj kvapaliny či dokonca aj niektoré plyny.
</p>
<br />
<b>Na čo slúžia?</b>
<p align="justify">
	Vodiče slúžia na prenos elektrickej energie z jedného miesta na druhé. Sú to napríkald kovové jadrá všetkých káblov. Izolanty sa na druhej strane používajú na ochenu pred elektrickým prúdom. Je to napríkald
	gumenná bužirka, ktorou sú všetky káble obalené.
</p>
<br />
<b>Ako to funguje?</b>
<p align="justify">
	Vodivosť materiálu závisí od mnohých premenných. Základom je však počet voľných elektrónov, ktoré sa v danej látke nachádzajú. Najlepšími vodičmi sú práve kovy pretože obsahujú prevažne jeden alebo dva valenčné elektróny. Atómy kovov ako
	meď, zlato alebo striebro obsahujú práve jeden takýto elektrón. Tento elektrón sa nazýva voľným a teda sa môže ľubovoľne pohybovať skrz celú kryštalickú mriežku kovu. Všetky takéto elektróny tvoria obrovskú masu, ktorá sa môže vo vodiči
	veľmi ľahko pohybovať a tým umožňuje materiálu viesť elektrický prúd.
</p>
<center><img src="images/op/metal_struktura.jpg" width="540" height="280" title="Štruktúra kovu"></center>
<p align="justify">
	Kovy sa zdajú byť ideálnymi vodičmi elektrockého prúdu, ale aj tie tvoria určitý elektrický odpor. Ten sa dá vypočítať na základe teploty, dĺžky vodiča, priemere vodiča a merného odporu materiálu. Merný odpor materiálu
	je tabuľková hodnota <b>&rho;</b>, ktorá udáva odpor materiálu v ohmoch (<b>&#x2126;</b>) na dĺžku 1 meter pri teplote 20°C. V naseldujúcej tabuľke je okrem merného odporu <b>&rho;</b> uvedený aj teplotný súčiniteľ odporu <b>&alpha;</b>,
	ten sa používa pre výpočet odporu pre rôzne teploty od 20°C.
</p>
<br />
<center>
	<table>
		<tr>
			<td style="width:150px;"><b>Materiál</b></td>
			<td style="width:150px;"><b>&rho;</b></td>
			<td style="width:100px;"><b>&alpha;</b></td>
		</tr>
		<tr>
			<td>Meď</td>
			<td>0,0000000168</td>
			<td>0,00386</td>
		</tr>
		<tr>
			<td>Striebro</td>
			<td>0,0000000159</td>
			<td>0,0038</td>
		</tr>
		<tr>
			<td>Zlato</td>
			<td>0,0000000224</td>
			<td>0,003715</td>
		</tr>
		<tr>
			<td>Kremík</td>
			<td>640</td>
			<td>0,00159</td>
		</tr>
		<tr>
			<td>Grafit</td>
			<td>0,00020</td>
			<td>-0,0005</td>
		</tr>
		<tr>
			<td>Diamant</td>
			<td>100000000000</td>
			<td>0</td>
		</tr>
	</table>
</center>
<br /><br />
<p align="justify">
	Teraz keď už poznáme všetky premenné potrebné pre výpočet odporu matriálu, môžeme ich dosasiť do nasledujúceho vzorca. Pričom <b>R</b> je výsledný odpor. </b>&rho;</b> je hodnota merného odporu z tabuľky. <b>&alpha;</b> je teplotný
	súčiniteľ odporu taktiež z tabuľky. <b>l</b> je dĺžka vodiča v metroch a <b>S</b> je priemer vodiča v metroch štvorcových. V našom experimente sa počíta s <b>l</b> = 100 a <b>S</b> = 0,00001.
</p>

<center><b>R = (&rho; * (1 + &alpha; * &Delta;t) * l / S)</b></center>

<p align="justify">
	Ako sme si už všimli, kovy sú veľmi dobré vodiče vďaka svojím valenčným elektrónom. Ale čo ostatné látky? Atóm kremíku obsahuje 4 valenčné elektróny. V kryštalickej mriežke z čistého kremíku však vytvára 4 kovalentné väzby. To znamená,
	že na každý zo svojich valenčných elektrónov naviaže iný atóm kremíku. A tým pádom v mriežke nezostanú žiadne voľné elektróny, ktoré by sa mohli vodičom pohybovať, pretože všetky elektróny su pevne viazané väzbami.
</p>

<center><img src="images/op/kremik_struktura.jpg" width="520" height="280" title="Štruktúra kremíku"></center>
<br /><br />
<b>Uhlík - grafit a diamant</b>
<p align="justify">
	Grafit aj diamant sú tvorené uhlíkom s tým rozdielom, že uhlík je vodič a diamant je veľmi dobrý izolant. Prečo majú tak rozdielnu vodivosť? Tajomstvom je usporiadanie atómov. Uhlík má rovnako ako kremík 4 valenčné elektróny.
	Teda môže nadviazať 4 kovalentné väzby. 	
</p>

<center><img src="images/op/graph-diamond.jpg" width="520" height="300" title="Grafit a diamant"></center>

<p align="justify">
	Grafit však vytvára iba 3 väzby obr(b) a tým pádom mu zostane jeden elektrón voľný. Tieto voľné elektróny sa pohybujú voľne medzi jednotlivými šesťuholníkovými vrstvami. Niesú teda pevne viazané a schopnosť voľne sa pohybovať 
	dáva materiálu vodivosť. <br /><br />
	Diamant však vzniká pod obrovským tlakom a pri veľmi vysokej teplote. To zapríčiňuje, že jednotlivé atómy sa dostanú do tesnej blizkosti a nadziažu aj štvrtú kovalentnú väzbu. Tým pádom v štruktúre diamantu nezostane žiaden voľný 
	elektrón, ktorý sa mohol voľne pohybovať. Z toho dôvodu je diamant veľmi dobrým izolantom.<br /><br />
</p>

