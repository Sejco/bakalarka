<?php
	$farby = ['colorRed','colorBlue','colorGreen'];
?>
<div id="plocha">
	<?php
		if (isset($_SESSION['user'])){
			if ($_SESSION['user']['ucitel'] == 1){
					if (isset($_POST['pravoSkupiny'])){
						if ($main->nacitajTestySkupin($_POST['idTestu'],$_POST['idSkupiny'])){
							$main->updateTestySkupin($_POST['idTestu'],$_POST['idSkupiny'],$_POST['pravoSkupiny']);
						}else{
							$main->insertTestySkupiny($_POST['idSkupiny'],$_POST['idTestu'],$_POST['pravoSkupiny'],$_SESSION['user']['id_skoly']);
						}
						echo "<script>location.href='index.php?experiment=administracia'</script>";
					}				
					$testy 		= $main->nacitajNazvyVsetkychTestovSkoly($_SESSION['user']['id_skoly']);
					$skupiny	= $main->nacitajUciteloveSkupinySkoly($_SESSION['user']['id_skoly'], $_SESSION['user']['id']);
					$prava		= $main->nacitajPravaVsetkychSkupinPodlaSkoly($_SESSION['user']['id_skoly']);		
				?>
					<center><h2><?=$_SESSION['user']['nazov_skoly']?></h2></center>
					<table>
						<tr>
							<td style="width:230px;"><b>Testy / Skupiny</b></td>
							<?php
								for ($i = 0;$i < count($skupiny);$i++){
									?>
										<td style="width:100px;"><a href="index.php?experiment=prehladSkupin&skupina=<?=$skupiny[$i]['id']?>"><?=$skupiny[$i]['nazov']?></td>								
									<?php
								}
							?>
						</tr>
						<?php
							for ($i = 0;$i < count($testy);$i++){
								?>
									<tr>
										<td><a href="index.php?experiment=editorTestov&editorTestovPodstranka=zobrazTest&id=<?=$testy[$i]['id']?>"><?=$testy[$i]['nazov']?></a></td>
										<?php
											for ($j = 0; $j < count($skupiny); $j++){
												$slct = $main->zistiSelectedTabulkaAdministracia($testy[$i]['id'],$skupiny[$j]['id'],$prava);
												
												?>
													<td>
														<form method="post">
															<input type="hidden" value="<?=$skupiny[$j]['id']?>" name="idSkupiny">
															<input type="hidden" value="<?=$testy[$i]['id']?>" name="idTestu">
															<select name="pravoSkupiny" onchange="this.form.submit()" style="width:100px;" class="<?=$farby[$slct - 1]?>">
																<option value="1" <?=($slct == 1 ? 'selected' : '')?> class="colorRed">Neprístupné</option>
																<option value="2" <?=($slct == 2 ? 'selected' : '')?> class="colorBlue">Prístupné</option>
																<option value="3" <?=($slct == 3 ? 'selected' : '')?> class="colorGreen">Hotové</option>
															</select>
														</form>
													</td>
												<?php
											}
										?>
									</tr>
								<?php
							}
						?>
					</table>
				<?php
			}
		}else{
			?>
				<center><h2>Nepovolený prístup!</h2></center>
				<center>Najskôr sa musíte prihlásiť</center>
			<?php
		}
	?>

</div>
<div id="control_panel">
	<?php
		include('loginPanel.php');
	?>
</div>