﻿<div id="plocha">
<?php	
	if ($_SESSION['user']['ucitel']){
		if (isset($_POST['deleteTest'])){
			$main->vymazTest($_POST['idTestu']);
			echo "<script>location.href = 'index.php?experiment=editorTestov';</script>";
		}
		
		if (isset($_POST['editTest'])){
			$nazov = $_POST['nazov'];
			$id = $_POST['id'];
			unset($_POST['nazov']);
			unset($_POST['editTest']);
			unset($_POST['id']);
			$main->upravTest($id,$nazov,serialize($main->arrayRemoveEmpty($_POST)));
			echo "<script>location.href = 'index.php?experiment=editorTestov&editorTestovPodstranka=zobrazTest&id=".$id."';</script>";			
		}

		if (isset($_POST['saveTest'])){
			$nazov = $_POST['nazov'];
			unset($_POST['nazov']);
			unset($_POST['saveTest']);
			$main->pridajNovyTest($nazov,serialize($_POST), $_SESSION['user']['id_skoly']);
			echo "<script>location.href = 'index.php?experiment=editorTestov';</script>";			
		}

		if (!isset($_GET['editorTestovPodstranka'])){
			echo "<center><h2>Zoznam testov</h2></center>";
			$vsetkyTesty = $main->nacitajNazvyVsetkychTestovSkoly($_SESSION['user']['id_skoly']);
			echo '<table class="alignCenter">';
				echo "<tr>
						<td class='width300'><b>Názov testu</b></td>
						<td class='editorTestovPrvyStlpec'><b><center>Náhľad</center></b></td>
						<td class='editorTestovPrvyStlpec'><b><center>Upraviť</center></b></td>
						<td class='editorTestovPrvyStlpec'><b><center>Ostrániť</center></b></td>
					  </tr>";
			for ($i = 0;$i < count($vsetkyTesty); $i++){
				?>
					<tr>
						<td><?=$vsetkyTesty[$i]['nazov']?></td>
						<td>
							<a href="index.php?experiment=editorTestov&editorTestovPodstranka=zobrazTest&id=<?=$vsetkyTesty[$i]['id']?>">
								<center><img src="images/eyeicon.png" width="15" height="15" title="Otvoriť náhľad testu"></center>
							</a>
						</td>
						<td>
							<?php
								if ($vsetkyTesty[$i]['id_skoly'] == $_SESSION['user']['id_skoly']){
									?>
										<a href="index.php?experiment=editorTestov&editorTestovPodstranka=upravTest&id=<?=$vsetkyTesty[$i]['id']?>"><center>
											<img src="images/edit.png" width="15" height="15" title="Upraviť test"></center>
										</a>
									<?php
								}else{
									?>										
										<center><img src="images/lock.png" width="18" height="18" title="Nemáte povolenie na upravenie tohto testu."></center>									
									<?php
								}	
							?>
						</td>
						<td>
							<?php
								if ($vsetkyTesty[$i]['id_skoly'] == $_SESSION['user']['id_skoly']){
									?>
										<a href="index.php?experiment=editorTestov&editorTestovPodstranka=vymazTest&id=<?=$vsetkyTesty[$i]['id']?>">
											<center><img src="images/no.png" width="15" height="15" title="Vymazať test"></center>
										</a>
									<?php
								}else{
									?>										
										<center><img src="images/lock.png" width="18" height="18" title="Nemáte povolenie na vymazanie tohto testu."></center>									
									<?php
								}	
							?>
						</td>
					</tr>
				<?php
			}
				echo "<tr><td colspan='4'><center><br /><br /><a href='index.php?experiment=editorTestov&editorTestovPodstranka=vytvoritNovyTest' class='myButton'>Vytvoriť nový test</a></center></td></tr>";
			echo '</table>';
		}elseif($_GET['editorTestovPodstranka'] == 'vytvoritNovyTest'){
			?>
				<center><h2>Vytváranie nového testu</h2></center>
				<form method="post" id="editorTestovForm">
					<table class="alignCenter" id="editorTestovTable">
						<tr>
							<td class="editorTestovPrvyStlpec">Názov testu:</td>
							<td colspan="8"><input type="text" name="nazov" style="width:200px;"></td>
						</tr>
						<tr class="editorTestovMedzeraMedziRiadkami">
							<td>Otázka 1</td>
							<td colspan="8"><textarea class="textAreaEditorTestov" id="q1" name="q1"></textarea></td>
						</tr>
						<tr id="moznosti_1">
							<td>Možnosti</td>
							<td class="inputEditorTestov"><input type="text" id="m11" name="m1[]" class="inputEditorTestov spravnaMoznost"></td>
							<td><input type="button" class="myButton" onclick="pridajMoznost('moznosti_1');" value="Pridaj možnosť"></td>
						</tr>
						<tr>
							<td>Počet bodov</td>
							<td><input type="number" id="body_1" name="body_1" class="editorTestovPrvyStlpec" min="0" max="999999" value="0" oninput="kontrola_vstupu_bez_blank('body_1',0 , 999999);"></td>
						</tr>
					</table>
					<br /><br />
					<center>
						<input type="button" class="myButton" onclick="pridajOtazku('editorTestovTable');" value="Pridaj otázku"><br /><br /><br /><br />
						<input type="submit" value="Uložiť test" class="myButton" name="saveTest">
					</center>
				</form>
			<?php
		}elseif($_GET['editorTestovPodstranka'] == 'zobrazTest'){
			$test = $main->nacitajTestPodlaId($_GET['id']);
			$abecdneMoznosti = ['A: ','B: ','C: ','D: ','E: ','F: '];

			if($test){
				$dataTestu = unserialize($test['dataTestu']);
				?>
					<center><h2><?=$test['nazov']?></h2></center>
					<?php
						for ($i = 1; $i <= (count($dataTestu) / 3); $i++){
							?>
								<table class="alignCenter" style="width:750px;">
									<tr class="editorTestovMedzeraMedziRiadkami">
										<td class="editorTestovPrvyStlpec valignTop"><b>Otázka <?=$i?></b></td>
										<td colspan="8" class="alignJustify" style="width:600px;"><?=$dataTestu['q'.$i];?></td>
									</tr>
									<tr>
										<td>Možnosti</td>
										<?php
											shuffle($dataTestu['m'.$i]);									
											for ($j = 0; $j < count($dataTestu['m'.$i]); $j++){
												?>
													<td style="width:<?=(600/count($dataTestu['m'.$i]))?>px;"><b><?=$abecdneMoznosti[$j]?></b><i><?=$dataTestu['m'.$i][$j];?></i></td>
												<?php
											}
										?>
									</tr>
									<tr>
										<td>Body</td>
										<td><?=$dataTestu['body_'.$i]?></td>
									</tr>
								</table>
							<?php
						}
			}else{
				?>
					<center><h2>ID testu neexistuje!</h2></center>
					<br /><br />
				<?php
			}
		}elseif($_GET['editorTestovPodstranka'] == 'upravTest'){
			$test = $main->nacitajTestPodlaId($_GET['id']);
			if($test){
				if ($test['id_skoly'] == $_SESSION['user']['id_skoly']){
					$dataTestu = unserialize($test['dataTestu']);
					?>
						<form method="post" id="editorTestovForm">
							<input type="hidden" name="id" value="<?=$test['id']?>">
							<table class="alignCenter" id="editorTestovTable">
								<tr>
									<td class="editorTestovPrvyStlpec">Názov testu:</td>
									<td colspan="8"><input type="text" name="nazov" style="width:200px;" value="<?=$test['nazov']?>"></td>
								</tr>
								<script>var pocetOtazok = <?=(count($dataTestu) / 3)?>;</script>
								<?php
									for ($i = 1; $i <= (count($dataTestu) / 3); $i++){
										?>																	
											<tr id="otazka_<?=$i?>" class="editorTestovMedzeraMedziRiadkami">
												<td>Otázka <?=$i?></td>
												<td colspan="8"><textarea class="textAreaEditorTestov" id="q<?=$i?>" name="q<?=$i?>"><?=$dataTestu['q'.$i];?></textarea></td>
											</tr>
											<tr id="moznosti_<?=$i?>">
												<td>Možnosti</td>
												<?php
													for ($j = 0; $j < count($dataTestu['m'.$i]); $j++){
														?>
															<td class="inputEditorTestov"><input type="text" id="m<?=$i.''.$j?>" name="m<?=$i?>[]" class="inputEditorTestov <?=($j == 0 ? 'spravnaMoznost' : '')?>" value="<?=$dataTestu['m'.$i][$j]?>"></td>
														<?php	
													}	
												?>
												<td><input type="button" class="myButton" onclick="pridajMoznost('moznosti_<?=$i?>');" value="Pridaj možnosť"></td>
												<script>odstranCudlikPridaniaBunky('moznosti_<?=$i?>');</script>
											</tr>
											<tr id="bodyTr_<?=$i?>">
												<td>Počet bodov</td>
												<td><input type="number" min="0" max="999999" id="body_<?=$i?>" name="body_<?=$i?>" class="editorTestovPrvyStlpec" value="<?=$dataTestu['body_'.$i]?>"  oninput="kontrola_vstupu_bez_blank('body_<?=$i?>',0 , 999999);"></td>
												<td><input type="button" class="myButton" onclick="odoberOtazku('<?=$i?>');" value="Odstrániť otázku"></td>
											</tr>																			
										<?php
									}
								?>
							</table>	
							<br /><br />
							<center>
								<input type="button" class="myButton" onclick="pridajOtazku('editorTestovTable');" value="Pridaj otázku"><br /><br /><br /><br />
								<input type="submit" value="Uložiť test" class="myButton" name="editTest">
							</center>
						</form>			
					<?php	
				}else{
					?>
						<center><h2>Tento test nepatrí vašej škole!</h2></center>
						<br /><br />
					<?php
				}
			}else{
				?>
					<center><h2>ID testu neexistuje!</h2></center>
					<br /><br />
				<?php
			}
		}elseif($_GET['editorTestovPodstranka'] == 'vymazTest'){
			$test = $main->nacitajNazovTestuPodlaID($_GET['id']);
			
			if ($test){
				if ($test['id_skoly'] == $_SESSION['user']['id_skoly']){
					?>
						<center><h2>Vymazať test <?=$test['nazov']?> ?</h2></center>
						<center>
							<form method="post">
								<input type="hidden" value="<?=$test['id']?>" name="idTestu">
								<input type="submit" class="myButton" value="Vymazať test" name="deleteTest">
								<a href="index.php?experiment=editorTestov" class="myButton">Nie</a>
							</form>
						</center>	
					<?php
				}else{
					?>
						<center><h2>Tento test nepatrí vašej škole!</h2></center>
						<br /><br />
					<?php
				}
			}else{
				?>
					<center><h2>ID testu neexistuje!</h2></center>
					<br /><br />
				<?php
			}
		}
	}else{
		?>
			<center><h2>Nepovolený prístup!</h2></center>
			<br /><br />
		<?php
	}
?>
</div>
<div id="control_panel">
	<?php
		include('loginPanel.php');
	?>
</div>