<?php
	$skoly = $main->nacitajVsetkySkoly();
	
	if (isset($_POST['id_skoly'])){
		if ($_POST['id_skoly'] != 0){
			$skupiny = $main->nacitajSkupinyDanejSkoly($_POST['id_skoly']);
		}
	}
	
	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";
	
	if (isset($_SESSION['user'])){
		echo "<script>location.href='index.php?experiment=kz'</script>";
	}
?>
<div id="plocha">
	<center>
		<h2>Prihlásenie</h2>
	</center>
	<form method="post">
		<table class="alignCenter">
			<tr>
				<td style="width:270px;">Zvoľte vašu <b>školu</b></td>
				<td>
					<select name="id_skoly" onchange="this.form.submit()" style="width:250px;">
						<option value="0">-</option>
					<?php
						for ($i = 0; $i < count($skoly);$i++){
							?>
								<option value="<?=$skoly[$i]['id']?>" <?=($skoly[$i]['id'] == (isset($_POST['id_skoly']) ? $_POST['id_skoly'] : 0) ? 'selected' : '')?>>
									<?=$skoly[$i]['nazov']?>
								</option>
							<?php
						}
					?>
					</select>
				</td>
			</tr>
			<?php
				if (isset($_POST['id_skoly'])){
					if ($_POST['id_skoly'] != 0){
						?>	
							<tr>				
								<td>Zvoľte vašu <b>triedu</b></td>
								<td>
									<select name="id_skupiny" onchange="this.form.submit()" style="width:250px;">
										<option value="0">-</option>
										<option value="-1" <?=(-1 == (isset($_POST['id_skupiny']) ? $_POST['id_skupiny'] : 0) ? 'selected' : '')?>>Učiteľ</option>
									<?php
										for ($i = 0; $i < count($skupiny);$i++){
											?>
												<option value="<?=$skupiny[$i]['id']?>" <?=($skupiny[$i]['id'] == (isset($_POST['id_skupiny']) ? $_POST['id_skupiny'] : 0) ? 'selected' : '')?>>
													<?=$skupiny[$i]['nazov']?>
												</option>
											<?php
										}
									?>
									</select>
								</td>
							</tr>
						<?php
					}
				}
				if ((isset($_POST['id_skoly'])) && (isset($_POST['id_skupiny']))){
					if (($_POST['id_skoly'] != 0) && ($_POST['id_skupiny'] != 0)){
						if ($_POST['id_skupiny'] == -1){ 							//ak je to ucitel
							?>	
								<tr>
									<td>Zadajte vaše <b>prihlasovacie meno</b></td>
									<td>
										<input
											type="text"
											name="loginFormMeno"
											style="width:140px;"
											id="loginFormMeno"			
											placeholder="Meno"
										>
									</td>
								</tr>
								<tr>
									<td>Zadajte vaše <b>heslo</b></td>
									<td>
										<input
											type="password"						
											name="loginFormHeslo"
											id="loginFormHeslo"
											style="width:140px;"
											placeholder="Heslo"
										>
									</td>
								</tr>
								<td>
									<input type="submit" name="loginFormSubmitUcitel" value="Prihlásiť sa" class="myButton alignRight"></td>
								</tr>
							<?php
						}else{
							?>	
								<tr>
									<td>
										Zadaj <b>svoje meno</b>
									</td>
									<td>
										<input
											type="text"
											name="loginFormMeno"
											style="width:140px;"
											id="loginFormMeno"
											placeholder="Meno"

										>
									</td>
								</tr>
								<tr>
									<td>
										Zadaj <b>svoje priezvisko</b>
									</td>
									<td>
										<input
											type="text"
											name="loginFormPriezvisko"
											style="width:140px;"
											id="loginFormPriezvisko"
											placeholder="Priezvisko"

										>
									</td>
								</tr>
								<tr>
									<td>
										Zadaj <b>heslo skupiny</b>
									</td>
									<td>
										<input
											type="password"
											name="loginFormHeslo"
											id="loginFormHeslo"
											style="width:140px;"
											placeholder="Heslo skupiny"
										>
									</td>
								</tr>
								<tr>
									<td colspan="2">* Heslo skupiny sa dozviete od vyučujúceho.</td>
								</tr>
								<tr>
									<td colspan="2"><center><br /><br /><input type="submit" name="loginFormSubmitStudent" value="Prihlásiť sa" class="myButton"></center></td>
								</tr>
								
							<?php
						}
					}
				}
			?>
		</table>
	</form>
</div>
<div id="control_panel">
	<?php
		include('loginPanel.php');
	?>
</div>