<div id="loginPanel">
	<?php
		if (!isset($_SESSION['user'])){
			if ($experiment == 'login'){
				?>
					<input type="button" class="myButton alignRight" onclick="window.history.back()" value="Späť">
				<?php
			}else{
				?>
					<input type="button" class="myButton alignRight" onclick="location.href='index.php?experiment=login'" value="Prihlásenie">
				<?php
			}
		}else{
			echo '<center><b>'.$_SESSION['user']['meno'].' '.$_SESSION['user']['priezvisko'].'</b></center><br />';
			if ($_SESSION['user']['ucitel'] == 1){
				?>
					<a href='index.php?experiment=administracia' 	<?=($experiment == 'administracia' ? 'class="loginPanelAHover"' : '')?>>Administrácia</a>
					<a href='index.php?experiment=prehladSkupin'    <?=($experiment == 'prehladSkupin' ? 'class="loginPanelAHover"' : '')?>>Skupiny</a>
					<a href='index.php?experiment=editorTestov' 	<?=($experiment == 'editorTestov' ? 'class="loginPanelAHover"' : '')?>>Testy</a>
				<?php				
			}else{
				?>
					<a href='index.php?experiment=testPreStudenta' 	<?=($experiment == 'testPreStudenta' ? 'class="loginPanelAHover"' : '')?>>Otestuj sa</a>
				<?php
			}
	?>		
			<form method="post">
				<input type="submit" name="logoutFormSubmit" value="Odhlásiť" class="alignRight myButton">
			</form>
	<?php			
		}
	?>
</div>