<?php
	if (isset($_POST['createNewStudenGroup'])){
		$main->pridajNovuSkupinu($_POST['menoSkupiny'],$_POST['hesloSkupiny'],$_SESSION['user']['id_skoly'],$_SESSION['user']['id']);
		echo "<script>location.href='index.php?experiment=prehladSkupin';</script>";
	}
?>
<div id="plocha">
	<?php
		if (isset($_SESSION['user'])){
			if ($_SESSION['user']['ucitel'] == 1){
				if (!isset($_GET['skupina'])){
					$skupiny	= $main->nacitajUciteloveSkupinySkoly($_SESSION['user']['id_skoly'], $_SESSION['user']['id']);
					?>
						<center><h2>Prehľad skupín</h2></center>
						<table class='alignCenter'>
							<?php
								for ($i = 0; $i < count($skupiny); $i++){
									?>
										<tr>
											<td style="width:200px;"><center><a href="index.php?experiment=prehladSkupin&skupina=<?=$skupiny[$i]['id']?>"><?=$skupiny[$i]['nazov']?></a></center></td>
										</tr>
									<?php
								}
							?>
						</table>
						<br /><br /><br /><br /><br />
						<table>
							<form method="post">
								<tr>
									<td style="width:200px;"><b>Vytvorenie novej skupiny</b></td>
								</tr>
								<tr>
									<td style="width:130px;"><input type="text" name="menoSkupiny" placeholder="Meno skupiny" style="width:130px;"></td>
								</tr>
								<tr>
									<td style="width:130px;"><input type="password" name="hesloSkupiny" placeholder="Heslo skupiny" style="width:130px;"></td>
								</tr>
								<tr>
									<td style="width:200px;"><input type="submit" name="createNewStudenGroup" value="Vytvoriť novú skupinu" class="myButton"></td>
								</tr>
							</form>
						</table>
					<?php
				}else{
					if (!isset($_GET['idStudenta'])){
						$skupiny 		= $main->nacitajLogSkupiny($_GET['skupina']);
						if (!empty($skupiny)){
							$nazvyTestov	= $main->separujNazvyTestov($skupiny);
							?>
								<center><h2>Prehľad skupiny <?=$skupiny[0]['nazov_skupiny']?></h2></center>
								<table>
									<?php
										for ($i = 0; $i < count($nazvyTestov); $i++){
											?>
												<tr>
													<td colspan="6"><br /><b><?=$nazvyTestov[$i]['nazov']?></b></td>
												</tr>
												<tr>
													<td style="width:30px;"></td>
													<td style="width:200px;"><b>Meno</b></td>
													<td style="width:80px;"><center><b>Body</b></center></td>
													<td style="width:100px;"><center><b>Čas</b></center></td>
													<td style="width:200px;"><center><b>Dátum</b></center></td>
													<td><center><b>IP</b></center></td>
												</tr>
											<?php
											for ($j = 0; $j < count($skupiny); $j++){
												if ($nazvyTestov[$i]['id'] == $skupiny[$j]['id_testu']){
													$phpdate 	= strtotime($skupiny[$j]['datum']);
													$datum 		= date('d.m.Y', $phpdate);
													$cas		= date('H:i:s', $phpdate);
													?>
														<tr>
															<td style="width:30px;"></td>
															<td style="width:200px;">
																<a href="index.php?experiment=prehladSkupin&skupina=<?=$skupiny[$j]['id_skupiny']?>&idStudenta=<?=$skupiny[$j]['id']?>">
																	<?=$skupiny[$j]['meno_studenta'].' '.$skupiny[$j]['priezvisko_studenta']?>
																</a>
															</td>
															<td style="width:80px;"><center><?=$skupiny[$j]['body']?></center></td>
															<td style="width:100px;"><center><?=$cas?></center></td>
															<td style="width:200px;"><center><?=$datum?></center></td>
															<td><center><i><?=$skupiny[$j]['ip_studenta']?></i></center></td>
														</tr>
													<?php
												}
											}
										}
									?>
								</table>
							<?php

						}else{
							?>
								<center><h2>Táto skupina ešte neodovzdala žiaden test.</h2></center>
							<?php
						}
					}else{
						$logStudenta = $main->nacitajLogStudenta($_GET['idStudenta']);
						$phpdate 	= strtotime($logStudenta['datum']);
						$datum 		= date('d.m.Y', $phpdate);
						$cas		= date('H:i:s', $phpdate);
						?>
							<center><h2>Výsledky testu</h2></center>
							<table>
								<tr>
									<td style="width:150px;">Meno:</td>
									<td><b><?=$logStudenta['meno_studenta']?></b></td>
								</tr>
								<tr>
									<td>Priezvisko:</td>
									<td><b><?=$logStudenta['priezvisko_studenta']?></b></td>
								</tr>
								<tr>
									<td>Bodový zisk:</td>
									<td><b><?=$logStudenta['body']?></b></td>
								</tr>
								<tr>
									<td>Názov testu:</td>
									<td><?=$logStudenta['nazov_testu']?></td>
								</tr>
								<tr>
									<td>Dátum:</td>
									<td><?=$cas.'  '.$datum?></td>
								</tr>
							</table>
								<?php
									$dataTestu = unserialize($logStudenta['dataTestu']);
									$logTestu  = unserialize($logStudenta['log_testu']);

									for ($i = 1; $i <= (count($dataTestu) / 3); $i++){
										?>
											<table class="alignCenter" style="width:750px;">
												<tr class="editorTestovMedzeraMedziRiadkami">
													<td class="valignTop" style="width:150px;"><b>Otázka <?=$i?></b></td>
													<td colspan="8" class="alignJustify" style="width:600px;"><?=$dataTestu['q'.$i];?></td>
												</tr>
												<tr>
													<td>Odpoveď</td>
													<td <?=((isset($logTestu['m'.$i]) ? ($dataTestu['m'.$i][0] == $logTestu['m'.$i]) : false) ? 'style="background-color:#00DD00;"' : 'style="background-color:red;"')?>> <?=(isset($logTestu['m'.$i]) ? $logTestu['m'.$i] : '')?></td>
												</tr>
												<tr>
													<td>Správna odpoveď</td>
													<td><?=$dataTestu['m'.$i][0]?></td>
												</tr>
												<tr>
													<td>Body</td>
													<td><?=$dataTestu['body_'.$i]?></td>
												</tr>
											</table>
										<?php
									}
								?>
							<br /><br /><br /><br />
						<?php
					}
				}
			}else{
				?>
					<center><h2>Nepovolený prístup!</h2></center>
					<center>Táto funkcia je dostupná iba pre učiteľov.</center>
				<?php
			}
		}else{
			?>
				<center><h2>Nepovolený prístup!</h2></center>
				<center>Najskôr sa musíte prihlásiť</center>
			<?php
		}
	?>
</div>
<div id="control_panel">
	<?php
		include('loginPanel.php');
	?>
</div>