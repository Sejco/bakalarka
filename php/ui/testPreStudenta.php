<script>
	var zavret = true;
	
	function cl(){
		zavret = false;
	}
</script>
<div id="plocha">
	<?php
		if (isset($_SESSION['user'])){
			if (!isset($_GET['idTestu'])){		
				$vsetkyTesty = $main->nacitajNazvyVsetkychPristupnychTestovSkoly($_SESSION['user']['id_skoly'],$_SESSION['user']['id']);
					?>
						<center><h2>Zoznam testov</h2></center>				
						<table class="alignCenter">
						<tr>
							<td class='width300'><b>Názov testu</b></td>
							<td class='width300'></td>
						</tr>
				<?php	  
					for ($i = 0;$i < count($vsetkyTesty); $i++){										
						?>
							<tr>
								<td><?=$vsetkyTesty[$i]['nazov']?></td>
								<td>
									<button onclick='window.open("index.php?experiment=testPreStudenta&idTestu=<?=$vsetkyTesty[$i]['id']?>", "<?=$vsetkyTesty[$i]['nazov']?>", "height=500,width=820")' class="myButton">
										<center>Začať test</center>
									</button>
								</td>
							</tr>
						<?php
					}				
				echo '</table>';
			}else{
				if (isset($_POST['odoslatTest'])){
					$test = $main->nacitajTestPodlaId($_POST['idTestu']);
					$dataTestu = unserialize($test['dataTestu']);
					$ziskaneBody = 0;
					$maxBody 	 = 0;
					?>
						<center><h2>Výsledky pre: <?=$test['nazov']?></h2></center>
						<?php
							for ($i = 1; $i <= (count($dataTestu) / 3); $i++){							
								if (isset($_POST['m'.$i])){
									if ($dataTestu['m'.$i][0] == $_POST['m'.$i]){
										$ziskaneBody += $dataTestu['body_'.$i];	
									}
								}	
								$maxBody += $dataTestu['body_'.$i];	
								?>
									<table class="alignCenter" style="width:750px;">
										<tr class="editorTestovMedzeraMedziRiadkami">
											<td class="editorTestovPrvyStlpec valignTop"><b>Otázka <?=$i?></b></td>
											<td colspan="8" class="alignJustify" style="width:600px;"><?=$dataTestu['q'.$i];?></td>
										</tr>
										<tr>
											<td>Odpoveď</td>																																						
											<td <?=((isset($_POST['m'.$i]) ? ($dataTestu['m'.$i][0] == $_POST['m'.$i]) : false) ? 'style="background-color:#00DD00;"' : 'style="background-color:red;"')?>><i><?=(isset($_POST['m'.$i]) ? $_POST['m'.$i] : '');?></i></td>
										</tr>
										<tr>
											<td>Body</td>
											<td><?=$dataTestu['body_'.$i]?></td>
										</tr>
									</table>
								<?php
							}
						?>
						<br /><br />
						<center><b>Bodový zisk: <?=$ziskaneBody.'/'.$maxBody?></b></center><br /><br />	
						<center><button class="myButton" onclick="window.close();">Zavrieť</button></center>
					<?php	
						$ip			= $_SERVER['REMOTE_ADDR'];
						$datum  	= date("Y-m-d H:i:s"); 
						$logTestu	= $main->vytvorLogTestu($_POST);
						if (!$main->nacitajZaznamStudenta($_POST['idTestu'],$_SESSION['user']['id_skoly'],$_SESSION['user']['id'],$_SESSION['user']['meno'],$_SESSION['user']['priezvisko'],$ip,$logTestu,$datum,$ziskaneBody)){
							$main->pridajZaznamStudenta($_POST['idTestu'],$_SESSION['user']['id_skoly'],$_SESSION['user']['id'],$_SESSION['user']['meno'],$_SESSION['user']['priezvisko'],$ip,$logTestu,$datum,$ziskaneBody);			
						}
				}else{
					$test = $main->nacitajTestPodlaId($_GET['idTestu']);
					if ($test){
						$dataTestu = unserialize($test['dataTestu']);
						$abecdneMoznosti = ['A: ','B: ','C: ','D: ','E: '];
						?>
							<script>
								window.onbeforeunload = function() {
									if (zavret){
										return "Práve prebieha Váš test. \n\nAk odídete z tejto stránky, Vaše odpovede budú vymazané.";
									}
								};
							</script>
							<center><h2>Test: <?=$test['nazov']?></h2></center>
							<form method="post">
								<input type="hidden" name="idTestu" value="<?=$test['id']?>">
								<?php
									for ($i = 1; $i <= (count($dataTestu) / 3); $i++){
										?>
											<table class="alignCenter" style="width:750px;">
												<tr class="editorTestovMedzeraMedziRiadkami">
													<td class="editorTestovPrvyStlpec valignTop"><b>Otázka <?=$i?></b></td>
													<td colspan="8" class="alignJustify" style="width:600px;"><?=$dataTestu['q'.$i];?></td>
												</tr>
												<tr>
													<td>Možnosti</td>
													<?php
														shuffle($dataTestu['m'.$i]);									
														for ($j = 0; $j < count($dataTestu['m'.$i]); $j++){
															?>															
																<td style="width:<?=(600/count($dataTestu['m'.$i]))?>px;">
																	<label for="<?='m'.$i.'l'.$j?>">
																		<b><?=$abecdneMoznosti[$j]?></b><i><?=$dataTestu['m'.$i][$j];?></i>
																	</label>
																	<input type="radio" name="<?='m'.$i?>" id="<?='m'.$i.'l'.$j?>" value="<?=$dataTestu['m'.$i][$j];?>">
																</td>															
															<?php
														}
													?>
												</tr>
												<tr>
													<td>Body</td>
													<td><?=$dataTestu['body_'.$i]?></td>
												</tr>
											</table>
										<?php
									}
								?><br /><br />
								<center><input type="submit" value="Odoslať test" class="myButton" name="odoslatTest" onclick="cl();"></center>	
							</form><br /><br /><br />
						<?php						
					}else{
						?>
							<center><h2>ID testu neexistuje!</h2></center>
						<?php
					}
				}
			}
		}else{
			?>
				<center><h2>Nepovolený prístup!</h2></center>
				<center>Najskôr sa musíte prihlásiť</center>
			<?php
		}
	?>		
	
</div>
<?php
	if (!isset($_GET['idTestu'])){
		?>
			<div id="control_panel">
				<?php
					include('loginPanel.php');
				?>
			</div>
		<?php
	}
?>